﻿var common = require('kioow.common');

module.exports = function (app) { 
    
    app.all('*', function (req, res, next) {
        res.header("Content-Type", "application/json; charset=utf-8");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers",
            "OMTToken, X-File-Size, Origin, X-Requested-With, Content-Type, " + 
            "Accept, Methods, Access-Control-Allow-Origin, Access-Control-Allow-Methods");
        //bypass the options requests...
        if (req.method == 'OPTIONS') {
            res.sendStatus(200);
        }
        else {
            //lock the GET method requests...
            if (req.method == 'GET') {
                res.status(500).send('You must call API with POST method!');
            }
            else {
                //to the next middleware...
                next();
            }
        }
    });

    app.post('/login', function (req, res) { 
        
    });

    app.post('/signup', function (req, res) { 
        
    });

    app.post('/get', function (req, res) { 
        
    });

    app.post('/save', function (req, res) { 
        
    });

    app.post('/list', function (req, res) { 
        
    });
    
    //Upload files management..
    var multer = require('multer');
    var utils = common.utils;

    var storageimages = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, app.restconfiguration.resourcesdirectory + '/upload')
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });

    var uploadimages = multer({ storage: storageimages });
  
    app.post('/upload/image', uploadimages.single('openmarketimages'), function (req, res) {
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;
        var _fs = require('../tools/filemgr');
        var cloudfs = new _fs.filemgr();
        cloudfs.cloudinaryUpload(target_path, function (image) {
            console.log(image);
            res.send(image);
            //omtcore.Hermes.notify('file.upload', {
            //    file: image,
            //});
            fs.delete(target_path, function (err) {
                if (err) {
                    console.log(err);
                }
            });
        });
    });

    app.post('/search', function (req, res) { 
    
    });
}