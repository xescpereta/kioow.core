﻿module.exports = function (core, socket) { 
    
    socket.on('findone', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'findone.done';
        var erroreventkey = conf.onerroreventkey || 'findone.error';
        var rq = {
            request: conf,
            method: 'findone'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('find', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'find.done';
        var erroreventkey = conf.onerroreventkey || 'find.error';
        var rq = {
            request: conf,
            method: 'find'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('list', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'list.done';
        var erroreventkey = conf.onerroreventkey || 'list.error';
        var rq = {
            request: conf,
            method: 'list'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('update', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'update.done';
        var erroreventkey = conf.onerroreventkey || 'update.error';
        var rq = {
            request: conf,
            method: 'update'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('save', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'save.done';
        var erroreventkey = conf.onerroreventkey || 'save.error';
        var rq = {
            request: conf,
            method: 'save'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('search', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'search.done';
        var erroreventkey = conf.onerroreventkey || 'search.error';
        var rq = {
            request: conf,
            method: 'search'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });
    
    socket.on('oldsearch', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'oldsearch.done';
        var erroreventkey = conf.onerroreventkey || 'oldsearch.error';
        var rq = {
            request: conf,
            method: 'oldsearch'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });
    
    socket.on('feed', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'feed.done';
        var erroreventkey = conf.onerroreventkey || 'feed.error';
        var rq = {
            request: conf,
            method: 'feed'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('inspiration', function (conf) { 
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'inspiration.done';
        var erroreventkey = conf.onerroreventkey || 'inspiration.error';
        var rq = {
            request: conf,
            method: 'inspiration'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('translate', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'translate.done';
        var erroreventkey = conf.onerroreventkey || 'translate.error';
        var rq = {
            request: conf,
            method: 'translate'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });
    
    socket.on('book', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'book.done';
        var erroreventkey = conf.onerroreventkey || 'book.error';
        var rq = {
            request: conf,
            method: 'book'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });
    
    socket.on('getdata', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'getdata.done';
        var erroreventkey = conf.onerroreventkey || 'getdata.error';
        var rq = {
            request: conf,
            method: 'getdata'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });
    
    socket.on('email', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'email.done';
        var erroreventkey = conf.onerroreventkey || 'email.error';
        var rq = {
            request: conf,
            method: 'email'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('distinct', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'distinct.done';
        var erroreventkey = conf.onerroreventkey || 'distinct.error';
        var rq = {
            request: conf,
            method: 'distinct'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('test', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'test.done';
        var erroreventkey = conf.onerroreventkey || 'test.error';
        var rq = {
            request: conf,
            method: 'test'
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('throw', function (conf) {
        conf.core = core;
        
        var eventkey = conf.oncompleteeventkey || 'throw.done';
        var erroreventkey = conf.onerroreventkey || 'throw.error';
        var rq = {
            request: conf,
            method: 'throw',
            socket: socket
        };
        var rt = core.processrequest(rq);
        rt.on(eventkey, function (rs) { socket.emit(eventkey, rs); });
        rt.on(erroreventkey, function (rs) { socket.emit(erroreventkey, rs); });
    });

    socket.on('error', function (err) {
        console.log('error en socket [Request failed]...');
        console.error(err.message);
        console.error(err.stack);
        socket.emit('uncaughtException', { error : err.message, stack: err.stack });
    });

}