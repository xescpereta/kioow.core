﻿module.exports.refreshsession = function (url, req) {
    req.oncompleteeventkey = 'refreshsession.done';
    req.onerroreventkey = 'refreshsession.error';
    return {
        command: 'refreshsession',
        request: req,
        url: url
    }
}