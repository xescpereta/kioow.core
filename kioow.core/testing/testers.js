﻿//set the Tests to Execute : true: Enabled, false: Disabled
var testlist = {
    test1 : false,
    test2: false, 
    test3: false,
    test4: false,
    testcore: true,
    testhiperion: false,
    testmembership: false,
    testhermes: false,
    test5: false,
    testmail: false,
    testsignup: false,
    testlists: false,
    testselfinvoke: false,
    testbackbone: false,
    testsyncronize: false,
    testworker: false,
    testauth: false
};
//set the pointer to the function...
var ct = {
    test1: test1,
    test2: test2,
    test3: test3,
    test4: test4,
    testcore: testcore,
    testhiperion: testhiperion,
    testmembership: testmembership,
    testhermes: testhermes,
    test5: test5,
    testmail: testmail,
    testsignup: testsignup,
    testlists: testlists,
    testselfinvoke: testselfinvoke,
    testbackbone: testbackbone,
    testsyncronize: testsyncronize,
    testworker: testworker,
    testauth: testauth
};

var _ = require('underscore');
var common = require('kioow.common');

function test1() {
    var mng = require('../classes/mongoiso');
    var mongo = new mng.MongoIso({ dbname: 'mongodb', forkdblayer: false });
    
    var request = {
        query: {
            slug_es: 'la-confederacion'
        },
        populate : [{
                path: 'dmc',
                select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership.b2bchanel'
            }],
        collectionname: 'DMCProducts'
    };
    
    //var request = {
    //    query: {
    //        code: 'OMT246'
    //    },
    //    populate : [{
    //            path: 'user'
    //                //select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership.b2bchanel'
    //        }, { path: '' }],
    //    collectionname: 'DMCs'
    //};

    setTimeout(function () {
        mongo.findone(request, function (rs) {
            console.log(rs.Data.dmc);
        }, function (err) {
            console.log(err);
        });
    }, 4000);
    
}

function test2() {
    var url = 'https://localhost:6033';
    var endopointinterface = 'socket';
    var connection = require('kioow.connector')({
        url: url,   //url to the endpoint (could be any url...)
        endpointinterface: endopointinterface      //endpointinterface: 'http' or 'socket'
    });
    var key = '440cc0ab-b235-496c-82c2-c8a87a2b0664';
    //var request = {
    //    command: 'findone',
    //    service: 'core',
    //    request: {
    //        query: { slug_es: 'la-confederacion' },
    //        oncompleteeventkey: 'findone.done',
    //        onerroreventkey: 'findone.error',
    //        populate : [{
    //                path: 'dmc', 
    //                select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership.b2bchanel'
    //            }],
    //        collectionname: 'DMCProducts'
    //    },
    //};
    
    var request = {
        command: 'find',
        service: 'core',
        request: {
            query: { _id: { $ne: null } },
            oncompleteeventkey: 'find.done',
            onerroreventkey: 'find.error',
            collectionname: 'Countries'
        },
    };

    var rq = connection.send(request);
    rq.on(request.request.oncompleteeventkey, function (result) {
        console.log('TEST2 () -> RESULTS: ###############################');
        console.log(result);
        console.log('END TEST2 ()         ###############################');
    });
    rq.on(request.request.onerroreventkey, function (err) {
        console.error('TEST2 () -> ERROR: ###############################');
        console.error(err);
        console.error('END TEST2 ()         ###############################');
    });

}

function test3() { 
    //TODO
    var url = 'https://localhost:6033';
    var endopointinterface = 'socket';
    var connection = require('kioow.connector')({
        url: url,   //url to the endpoint (could be any url...)
        endpointinterface: endopointinterface      //endpointinterface: 'http' or 'socket'
    });
    
    var request = {
        command: 'throw',
        service: 'membership',
        request: {
            errormessage: 'error from tester 3',
            oncompleteeventkey: 'throw.done',
            onerroreventkey: 'throw.error',
        },
    };
    
    var rq = connection.send(request);
    rq.on(request.request.oncompleteeventkey, function (result) {
        console.log('TEST3 () -> RESULTS: ###############################');
        console.log(result.dmc);
        console.log('END TEST3 ()         ###############################');
    });
    rq.on(request.request.onerroreventkey, function (err) {
        console.error('TEST3 () -> ERROR: ###############################');
        console.error(err);
        console.error('END TEST3 ()         ###############################');
    });
}

function test4() { 
    //TODO

    var rq = {
        data: {
            "_id": "56406abb66f56b88387498b6", 
            "slug": "afi1778", "code": "AFI1778", 
            "name": "pablo12", "__v": 0, "user": { "_id": "56406abb66f56b88387498b5", "email": "pablo@yourttoo.com", "code": "AFI1778" }, "updatedOn": "2015-11-10T10:42:19.650Z", "createdOn": "2015-11-09T09:43:23.428Z", "fees": { "unique": 10, "groups": 10, "tailormade": 10, "flights": 10 }, "currency": { "label": "Euro", "symbol": "€", "value": "EUR" }, 
            "images": { "splash": { "public_id": "", "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" }, "logo": { "public_id": "jvikoejxnhcui8oso7xg", "url": "http://res.cloudinary.com/openmarket-travel/image/upload/v1447062389/jvikoejxnhcui8oso7xg.jpg", "format": "jpg", "height": 600, "resource_type": "image", "secure_url": "https://res.cloudinary.com/openmarket-travel/image/upload/v1447062389/jvikoejxnhcui8oso7xg.jpg", "signature": "a7f540032c69a136b8801695a23726ca0cba275c", "version": 1447060000, "width": 600 }, "photo": { "public_id": "", "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" } }, 
            "membership": { "acceptterms": false, "registervalid": true }, 
            "contact": {
                "firstname": "pablo1", "lastname": "grillo", "email": "pablo@yourttoo.com", "position": "test", "skype": "lkjlj", 
                "paymentContact": { "email": "pablo@yourttoo.com" }, "bookingContact": { "email": "pablo@yourttoo.com" }, "marketingContact": { "email": "pablo@yourttoo.com" }
            }, 
            "company": { "name": "Agencia de viajes", "legalname": "pablo test sl", "constitutionyear": 1999, "phone": "465456464", "website": "http://www.pablogrillo.com", "taxid": "J8898898", "location": { "fulladdress": "Rúa de Gómez Ulla, 19, 15702 Santiago de Compostela, A Coruña, Spain", "cp": "15702", "city": "Santiago de Compostela", "country": "Spain", "countrycode": "ES", "stateorprovince": "Galicia", "latitude": 42.8756878, "longitude": -8.543839899999966, "route": "Rúa de Gómez Ulla" } }
        }, 
        query: { "code": "AFI1778" }, 
        collectionname: "Affiliate",
        oncompleteeventkey: "save.done", 
        onerroreventkey: "save.error"
    };

    var url = 'https://yourttootest.cloudapp.net:6033';
    var endopointinterface = 'socket';
    var connection = require('kioow.connector')({
        url: url,   //url to the endpoint (could be any url...)
        endpointinterface: endopointinterface      //endpointinterface: 'http' or 'socket'
    });
    
    var request = {
        command: 'save',
        service: 'core',
        request: rq
    };
    var rq = connection.send(request);
    rq.on(request.request.oncompleteeventkey, function (result) {
        console.log('TEST4 () -> RESULTS: ###############################');
        console.log(result);
        console.log('END TEST4 ()         ###############################');
    });
    rq.on(request.request.onerroreventkey, function (err) {
        console.error('TEST4 () -> ERROR: ###############################');
        console.error(err);
        console.error('END TEST4 ()         ###############################');
    });
}

function testcore() {
    
    var core = require('../interface/core');
    core.configuration.port = 7777;
    var url = 'http://localhost:' + core.configuration.port;
    var user = {
        "_id" : "569923c3e07e363c1922028f",
        "apikey" : "1e02b37e64ba6c3add3459dfb73d46fb32800ae3",
        "username" : "pablo",
        "email" : "pablo@yourttoo.com",
        "code" : "AV001939",
        "phone" : "",
        "password" : "$2a$10$0Pxu/NFYAwJdyNhMiz9UfOYAtOBKqrhPVHvWYbqzjLj75pOFnWLXK",
        "isDMC" : false,
        "isTraveler" : false,
        "isAdmin" : false,
        "isAffiliate" : true,
        "active" : true,
        "isLocal" : true,
        "isFacebookLinked" : false,
        "isTwitterLinked" : false,
        "isGoogleLinked" : false,
        "roles" : [],
        "photo" : {
            "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
        },
        "__v" : 0
    };
    var member = {
        "_id" : "569923c3e07e363c19220290",
        "slug" : "av001939",
        "code" : "AV001939",
        "name" : "pablo",
        "fees" : {
            "unique" : 10,
            "groups" : 12,
            "tailormade" : 11,
            "flights" : 8
        },
        "currency" : {
            "label" : "Euro",
            "symbol" : "€",
            "value" : "EUR"
        },
        "images" : {
            "splash" : {
                "public_id" : "",
                "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
            },
            "logo" : {
                "public_id" : "d7kimj5yiccemvfywns9",
                "url" : "http://res.cloudinary.com/openmarket-travel/image/upload/v1452880627/d7kimj5yiccemvfywns9.jpg",
                "format" : "jpg",
                "height" : 225,
                "resource_type" : "image",
                "secure_url" : "https://res.cloudinary.com/openmarket-travel/image/upload/v1452880627/d7kimj5yiccemvfywns9.jpg",
                "signature" : "c9b0747f1412e0738bed39a1af1321494f32822a",
                "version" : 1452880627,
                "width" : 225
            },
            "photo" : {
                "public_id" : "",
                "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
            }
        },
        "membership" : {
            "acceptterms" : true,
            "colaborationagree" : false,
            "omtmargin" : 3,
            "registervalid" : true
        },
        "contact" : {
            "firstname" : "Pablo",
            "lastname" : "Grillo",
            "email" : "pablo@yourttoo.com",
            "position" : "cargo",
            "skype" : "",
            "paymentContact" : {
                "firstname" : "Pablo",
                "lastname" : "Grillo",
                "phone" : "46545645",
                "email" : "pablo@yourttoo.com",
                "location" : {
                    "fulladdress" : "Carrer Dr. Gómez Ullà, 19, 07420 Sa Pobla, Illes Balears, España",
                    "cp" : "07420",
                    "city" : "Sa Pobla",
                    "country" : "España",
                    "countrycode" : "ES",
                    "stateorprovince" : "Illes Balears",
                    "latitude" : 39.7694891,
                    "longitude" : 3.028840199999991,
                    "route" : "Carrer Doctor Gómez Ullà"
                }
            },
            "bookingContact" : {
                "email" : "pablo@yourttoo.com"
            },
            "marketingContact" : {
                "email" : "pablo@yourttoo.com"
            }
        },
        "company" : {
            "name" : "Agencia de Test",
            "legalname" : "Viajes Prueba SL",
            "constitutionyear" : 1999,
            "phone" : "46545645",
            "agencylic" : "LIC4554",
            "group" : "AMsa",
            "website" : "google.com",
            "taxid" : "F46546465",
            "location" : {
                "fulladdress" : "Carrer Dr. Gómez Ullà, 19, 07420 Sa Pobla, Illes Balears, España",
                "cp" : "07420",
                "city" : "Sa Pobla",
                "country" : "España",
                "countrycode" : "ES",
                "stateorprovince" : "Illes Balears",
                "latitude" : 39.7694891,
                "longitude" : 3.028840199999991,
                "route" : "Carrer Doctor Gómez Ullà"
            }
        },
        "__v" : 0,
        "user" : user,
        "omtcomment" : "Este es un comentario"
    };
    //var url = 'http://yourttootest.cloudapp.net:4000';
    //var request = {
    //    command: 'search',
    //    request: {
    //        country: 'TH',
    //        type: 'asc',
    //        currency: 'EUR',
    //        oncompleteeventkey: 'search.done',
    //        onerroreventkey: 'search.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'findone',
    //    request: {
    //        query: { slug_es: 'una-experiencia-en-tailandia' },
    //        oncompleteeventkey: 'findone.done',
    //        onerroreventkey: 'findone.error',
    //        populate : [{
    //                path: 'dmc', 
    //                select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership'
    //            }],
    //        collectionname: 'DMCProducts'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'save',
    //    request: {
    //        data: require('./affiliatedummy'),
    //        oncompleteeventkey: 'save.done',
    //        onerroreventkey: 'save.error',
    //        collectionname: 'Affiliate',
    //        query: { code: "AFI528" }
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'save',
    //    request: {
    //        data: require('./bookingdummy3'),
    //        query: {
    //            idBooking: '00001247',
    //        },
    //        oncompleteeventkey: 'save.done',
    //        onerroreventkey: 'save.error',
    //        collectionname: 'Bookings'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'book',
    //    request: {
    //        booking: require('./bookingdummy4'),
    //        oncompleteeventkey: 'book.done',
    //        onerroreventkey: 'book.error',
    //        auth: member
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'book',
    //    request: {
    //        booking: require('./bookingdummy4'),
    //        oncompleteeventkey: 'book.done',
    //        onerroreventkey: 'book.error',
    //        auth: member
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'throw',
    //    request: {
    //        errormessage: 'error from tester 3',
    //        oncompleteeventkey: 'throw.done',
    //        onerroreventkey: 'throw.error',
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'distinct',
    //    request: {
    //        fields: ['slug'],
    //        oncompleteeventkey: 'distinct.done',
    //        onerroreventkey: 'distinct.error',
    //        collectionname: 'DestinationCities'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'email',
    //    request: {
    //        oncompleteeventkey: 'email.done',
    //        onerroreventkey: 'email.error',
    //        to: ['emai@loques'],
    //        subject: '',
    //        mailtemplate: '',
    //        mailparameter: {},
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'getdata',
    //    request: {
    //        type: 'static',
    //        oncompleteeventkey: 'getdata.done',
    //        onerroreventkey: 'getdata.error',
    //        name: 'months_es'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'find',
    //    request: {
    //        query: { code: 'ES' },
    //        oncompleteeventkey: 'find.done',
    //        onerroreventkey: 'find.error',
    //        populate : [{
    //                path: 'dmc', 
    //                select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership.b2bchanel'
    //            }],
    //        collectionname: 'DMCProducts',
    //        //visitor: 'like'
    //    },
    //    url: url
    //};

    //var request = {
    //    command: 'find',
    //    request: {
    //        query: { slug: { $ne: null } },
    //        oncompleteeventkey: 'find.done',
    //        onerroreventkey: 'find.error',
    //        collectionname: 'DestinationCities',
    //        fields: '_id label_en label_es slug countrycode'
    //    },
    //    url: url
    //};

    //"slug_es":"chiang-mai-esencial"
    //var request = {
    //    command: 'find',
    //    request: {
    //        query: { slug_es: 'chiang-mai-esencial', publishState: 'published' },
    //        populate: [{ path: 'dmc' }],
    //        oncompleteeventkey: 'findone.done',
    //        onerroreventkey: 'findone.error',
    //        collectionname: 'DMCProducts',
    //        auth: member
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'find',
    //    request: {
    //        query: { userqueryId: '55bf7cd87eb6f93c023cbfbd' },
    //        oncompleteeventkey: 'find.done',
    //        onerroreventkey: 'find.error', 
    //        collectionname: 'Quotes'
    //    },
    //    url: url
    //};
    //var dataURQ = {
    //    "data": {
    //        "code": "NOCODE", 
    //        "publishedDate": "2015-11-25T09:10:32.189Z", 
    //        "isnew": true, 
    //        "description": "New Query...", 
    //        "additionalinfo": {
    //            "description": "", 
    //            "trip": "grouptrip", 
    //            "regimen": "ol", 
    //            "needs": "", 
    //            "guide": {
    //                "included": false, 
    //                "language": {
    //                    "spanish": false, 
    //                    "english": false, 
    //                    "french": false, "german": false, "italian": false, "portuguese": false
    //                }
    //            }
    //        }, "dates": {
    //            "knowingdates": false, 
    //            "arrivaldate": null, 
    //            "arrival": {
    //                "year": 2016, "month": 1, "day": 21, "monthname_en": "February", "monthname_es": "Febrero"
    //            }, 
    //            "month": { "monthnumber": 2, "monthname": "Febrero", "monthyear": 2016 }, 
    //            "week": "3", 
    //            "alreadygotflights": false, 
    //            "dataflightsIn": "", "dataflightsOut": "", 
    //            "duration": 4, "flexibility": { "number": 0, "range": "days" }
    //        }, "hosting": {
    //            "hostingKindNotes": "", "lowcosthotels": true, 
    //            "standarhotels": false, "superiorhotels": false, "charmhotels": false, "luxuryhotels": false
    //        }, "budget": {
    //            "cost": 555, 
    //            "currency": { "label": "Euro", "symbol": "€", "value": "EUR" }
    //        }, "travelercode": null, 
    //        "traveler": null, 
    //        "destinations": [
    //            { "fulladdress": "España", "cp": "", "city": "", "country": "España", "countrycode": "ES", "stateorprovince": "", "latitude": 40.46366700000001, "longitude": -3.7492200000000366 }], 
    //        "whattodo": [{ "_id": "543e9b2eee63d0382c52def1", "slug": "water-sports", "label": "Deportes Acuaticos", "label_en": "Water Sports" }], 
    //        "roomDistribution": [{ "numRoom": 0, "roomType": { "roomCode": "double", "label": "Doble", "pax": 2 }, "paxList": [{ "typePax": "adult", "age": 30, "holder": true }, { "typePax": "adult", "age": 30 }], "slug": "double0" }], "group": null, "passengers": [{ "acomodattion": { "howmany": 2 } }], 
    //        "state": "requested", 
    //        "affiliate": { "_id": "56406cfa66f56b88387498b8", "slug": "afi1779", "code": "AFI1779", "name": "antosango", "__v": 0, "user": { "_id": "56406cfa66f56b88387498b7", "apikey": "f5c39210ba44343966bacb8a1546836f3f7899e7", "username": "antosango", "email": "antosango@gmail.com", "code": "AFI1779", "phone": "", "password": null, "isDMC": false, "isTraveler": false, "isAdmin": false, "isAffiliate": true, "active": true, "isLocal": true, "isFacebookLinked": false, "isTwitterLinked": false, "isGoogleLinked": false, "__v": 0, "updatedOn": "2015-11-09T09:52:58.726Z", "createdOn": "2015-11-09T09:52:58.366Z", "roles": ["55e82574da8c472e7e8202ef"], "photo": { "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" } }, "updatedOn": "2015-11-19T17:26:07.901Z", "createdOn": "2015-11-09T09:52:58.369Z", "fees": { "unique": 17, "groups": 10, "tailormade": 12, "flights": 6 }, "currency": { "label": "Euro", "symbol": "€", "value": "EUR" }, "images": { "splash": { "public_id": "", "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" }, "logo": { "public_id": "", "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" }, "photo": { "public_id": "", "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg" } }, "membership": { "acceptterms": false, "registervalid": true }, "contact": { "firstname": "antonio", "lastname": "sanchez gomez", "email": "antosango@gmail.com", "position": "aaa", "skype": "", "paymentContact": { "email": "antosango@gmail.com" }, "bookingContact": { "email": "antosango@gmail.com" }, "marketingContact": { "email": "antosango@gmail.com" } }, "company": { "name": "Antoniuos The Travel Band", "legalname": "Antoniuos The Travel Band", "constitutionyear": 1983, "phone": "600685003", "website": "a", "taxid": "53712905q", "location": { "fulladdress": "Calle Llucmajor", "city": "Palma de Mallorca", "stateorprovince": "Islas Baleares", "cp": "07006", "country": "España", "countrycode": "", "latitude": 0, "longitude": 0 } } }
    //    }, 
    //    "query": { "code": "NOCODE" }, 
    //    "collectionname": "UserQueries", 
    //    "oncompleteeventkey": "save.done", 
    //    "onerroreventkey": "save.error"
    //};
    //var request = {
    //    command: 'save',
    //    request: dataURQ,
    //    url: url
    //};
    //var sRQ = {
    //    currency: 'EUR',
    //    maxresults: 12,
    //    orderby: 'priceindexing',
    //    ordinal: 'asc',
    //    page: 'next',
    //    oncompleteeventkey: 'searchII.done',
    //    onerroreventkey: 'searchII.error',
    //    lastcode: null,
    //    prevcode: null
    //};
    var sRQ = {
        currency: "EUR",
        maxresults: 12,
        orderby: "priceindexing",
        ordinal: "asc",
        page: "next",
        countries: [
            "56bcf2d398a07004105a0464"
        ],
        cities: [
            "56bcf2d798a07004105a0dc2"
        ],
        maxresults: 12,
        b2bchannel: true
    };
    //var sRQ = {
    //    currency: "EUR",
    //    maxresults: 12,
    //    orderby: "priceindexing",
    //    ordinal: "asc",
    //    page: "next",
    //    lastcode: null,
    //    prevcode: null,
    //    //countries: [
    //    //    "569e26f903d217ac04e5953f" // 5699090952c4bf98051d93ad MX ObjectId("569e26f903d217ac04e5953f") TH 
    //    //],
    //    b2bchannel: true,
    //    auth: member
    //}
    //var sRQ = {
    //    currency: "EUR",
    //    maxresults: 12,
    //    orderby: "priceindexing",
    //    ordinal: "asc",
    //    page: "next",
    //    countries: [
    //        "56979b1272f4abc42cae8e18"
    //    ], cities: [
    //        "56979b1572f4abc42cae96c9", "56979b1472f4abc42cae9155", "56979b1672f4abc42cae9910"
    //    ],
    //    maxdays: 50,
    //    b2bchannel: true
    //}
    var request = {
        command: 'search',
        request: sRQ,
        url: url
    };
    //request.request.countries = ['5692726a352368a02187ef0e'];
    //request.request.cities = [];
    //request.request.b2bchannel =  true;
    //request.request.b2cchannel =  null;
    //request.request.tags =  [];
    //request.request.categories =  [];
    ////request.request.mindays =  1;
    ////request.request.maxdays =  50;
    ////request.request.pricemin =  0;
    ////request.request.pricemax =  0;
    //request.request.providers =  [];
    //request.request.exclusions =  [];
    //request.request.departuredate = "";
    
    var ytoclient = require('kioow.connector').connector;
    //start interface...
    core.start(function (rs) {
        console.log('Core is Ready for this test...');
        console.log(rs);
    });
    //warming time...
    setTimeout(function () {
        //Test Request:
        var startedtime = new Date();
        var rq = ytoclient.send(request);
        rq.on(request.request.oncompleteeventkey, function (result) { 
            console.log('TESTCORE () -> RESULTS: ###############################');
            console.log('finished...');
            console.log('started: ' + startedtime);
            console.log('ended: ' + new Date());
            console.log(result);
            //Object.prototype.toString.call(result) == '[object Array]' ?  console.log(result[0].minprice) : console.log(result.minprice);
            //Object.prototype.toString.call(result) == '[object Array]' ?  console.log(result[0].prices) : console.log(result.prices);
            //var ct = [];
            //var rp = {};
            //ct = _.map(result, function (city) {
            //    var sln = city.label_es + ' # ' + city.label_en + ' # ' + city.slug + ' # ' + city._id;
            //    var label = common.utils.slug(city.label_es);
            //    rp[label] != null ? rp[label].count++ : rp[label] = { label: city.label_es, count : 1 , slugs: [], countrycode: [] };
            //    rp[label] != null ? rp[label].slugs.push(city.slug) : null;
            //    rp[label] != null ? rp[label].countrycode.indexOf(city.countrycode) < 0 ? 
            //        rp[label].countrycode.push(city.countrycode) : null 
            //    : null;
            //    return sln;
            //});
            //ct.sort();
            //var rpu = _.filter(rp, function (rpt) { return rpt.count > 1 });
            //rpu = _.sortBy(rpu, function (rpt) { return rpt.label });
            //var fs = require('fs');
            ////fs.writeFile('c:/temp/testcore.track.txt', JSON.stringify(result, null, '\n'));
            //fs.writeFile('c:/temp/testcore.track.txt', JSON.stringify(ct, null, '\n'));
            //fs.writeFile('c:/temp/testcore.track2.txt', JSON.stringify(rp, null, '\n'));
            //fs.writeFile('c:/temp/testcore.track3.txt', JSON.stringify(rpu, null, '\n'));

            console.log('END TESTCORE ()         ###############################');
        });
        rq.on(request.request.onerroreventkey, function (err) {
            console.error('TESTCORE () -> ERROR: ###############################');
            console.error(err);
            console.error('END TESTCORE ERROR () ###############################');
        });
    }, 4000);
}

function testhermes() {
    
    var hermes = require('../interface/hermes');
    
    var url = 'http://localhost:7000';// + core.configuration.port;
    //var request = {
    //    command: 'notify.suscribers',
    //    request: {
    //        subject: 'product',
    //        action: 'update',
    //        data: { code: 'AFI00893' },
    //        oncompleteeventkey: 'notify.suscribers',
    //        onerroreventkey: 'notify.suscribers'
    //    },
    //    url: url
    //};
    
    var ytoclient = require('kioow.connector').connector;
    //start interface...
    hermes.start();
    //warming time...
    //setTimeout(function () {
    //    //Test Request:
    //    var rq = ytoclient.send(request);
    //    rq.on(request.request.oncompleteeventkey, function (result) {
    //        console.log('TESTHERMES () -> RESULTS: ###############################');
    //        console.log(results);
    //        console.log('END TESTHERMES ()         ###############################');
    //    });
    //    rq.on(request.request.onerroreventkey, function (err) {
    //        console.error('TESTHERMES () -> ERROR: ###############################');
    //        console.error(err);
    //        console.error('END TESTHERMES () ###############################');
    //    });
    //}, 4000);
}

function testhiperion() {
    
    var hiperion = require('../interface/hiperion');
    //hiperion.configuration.port = '1200';
    
    //var rq = {
    //    action: 'update',
    //    subject: 'affiliate',
    //    data: { code: 'AFI00833' }
    //};
    //var request = {
    //    command: 'processrequest',
    //    request: {
    //        "oncompleteeventkey": "processrequest.done",
    //        "onerroreventkey": "processrequest.error",
    //        "subject": "user",
    //        "action": "forgotpassword",
    //        "data": {
    //            "user": {
    //                "_id": "547efe4ad1265 4100bf3c230",
    //                "username": "xisco",
    //                "email": "xisco@openmarket.travel",
    //                "code": "OMTADM 01",
    //                "phone": "",
    //                "password": "$2a$10$quMGehclvK3ameFp.4JpbeQVogSin4V/ZlEfxGW3y05oFe bx6zBIO",
    //                "isDMC": false,
    //                "isTraveler": false,
    //                "isAdmin": true,
    //                "active": true,
    //                "isLocal": true,
    //                "isFacebookLinked": false,
    //                "isTwitterLinked": false,
    //                "isGoogleLinked": false,
    //                "_ _v": 0,
    //                "updatedOn": "2014-12-03T12:12:59.380Z",
    //                "createdOn": "2014-12-03T12:12:58.75 5Z",
    //                "roles": [
    //                    "53d11933159709402588a154"
    //                ],
    //                "photo": {
    //                    "url": "http://res.cloudinary.c om/open-market-travel/image/upload/v1412587999/avatar.jpg"
    //                }
    //            },
    //            "member": {
    //                "_id": "54 7efe4bd12654100bf3c231",
    //                "slug": "omtadm01",
    //                "code": "OMTADM01",
    //                "name": "xisco",
    //                "emai l": "xisco@openmarket.travel",
    //                "skype": "",
    //                "description": "",
    //                "membershipDate": "2014- 12-03T12:12:59.380Z",
    //                "user": {
    //                    "_id": "547efe4ad12654100bf3c230",
    //                    "username": "xisco",
    //                    "email": "xisco@openmarket.travel",
    //                    "code": "OMTADM01",
    //                    "phone": "",
    //                    "password": "$2a$ 10$quMGehclvK3ameFp.4JpbeQVogSin4V/ZlEfxGW3y05oFebx6zBIO",
    //                    "isDMC": false,
    //                    "isTrave ler": false,
    //                    "isAdmin": true,
    //                    "active": true,
    //                    "isLocal": true,
    //                    "isFacebookLinked": false,
    //                    "isTwitterLinked": false,
    //                    "isGoogleLinked": false,
    //                    "__v": 0,
    //                    "updatedOn": "2014-12-03T1 2:12:59.380Z",
    //                    "createdOn": "2014-12-03T12:12:58.755Z",
    //                    "roles": [
    //                        "53d11933159709402 588a154"
    //                    ],
    //                    "photo": {
    //                        "url": "http://res.cloudinary.com/open-market-travel/image/upl oad/v1412587999/avatar.jpg"
    //                    }
    //                },
    //                "__v": 0,
    //                "updatedOn": "2014-12-03T12:12:59.427Z",
    //                "cr eatedOn": "2014-12-03T12:12:59.380Z",
    //                "images": {
    //                    "splash": {
    //                        "url": "http://res.cloudi nary.com/open-market-travel/image/upload/v1412587999/avatar.jpg"
    //                    },
    //                    "logo": {
    //                        "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1412587999/avatar.jp g"
    //                    },
    //                    "photo": {
    //                        "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1 412587999/avatar.jpg"
    //                    }
    //                },
    //                "location": {
    //                    "fulladdress": "",
    //                    "city": "",
    //                    "stateorprovince": "",
    //                    "cp": "",
    //                    "country": "",
    //                    "countrycode": "",
    //                    "continent": "",
    //                    "latitude": 0,
    //                    "longitude ": 0
    //                }
    //            },
    //            "template": "ytoaffiliateforgotpassword",
    //            "data": {
    //                "key": "e71d2a27-2aa4-484d- f070-632d109c0e49",
    //                "link": "http://localhost:3010/nueva-contrasena?key=e71d2a27-2 aa4-484d-f070-632d109c0e49",
    //                "affiliate": "xisco"
    //            }
    //        },
    //    }
    //};
    
    var url = 'http://localhost:' + hiperion.configuration.port;
    //request.url = url;
    console.log(url);
    var ytoclient = require('kioow.connector').connector;
    //start interface...
    hiperion.start();
    //warming time...
    //setTimeout(function () {
    //    //Test Request:
    //    console.log('url: ' + request.url);
    //    var rq = ytoclient.send(request);
    //    rq.on(request.request.oncompleteeventkey, function (result) {
    //        console.log('TESTHIPERION () -> RESULTS: ###############################');
    //        console.log(result);
    //        console.log('END TESTHIPERION ()         ###############################');
    //    });
    //    rq.on(request.request.onerroreventkey, function (err) {
    //        console.error('TESTHIPERION () -> ERROR: ###############################');
    //        console.error(err);
    //        console.error('END TESTHIPERION ()       ###############################');
    //    });
    //}, 12000);
}

function testmembership() { 
    var child = require('child_process');
    var memberhisp = require('../interface/membership');
    memberhisp.configuration.port = 3434;
    var userdummy = require('./userdummy');
    var affidummy = require('./affiliatedummy');
    affidummy.user = userdummy;
    //var hermes = require('../interface/hermes');
    var url = 'http://localhost:' + memberhisp.configuration.port;
    //var request = {
    //    command: 'login',
    //    request: {
    //        email: 'xisco@openmarket.travel',
    //        password: 'password',
    //        oncompleteeventkey: 'login.done',
    //        onerroreventkey: 'login.error'
    //    },
    //    url: url
    //};
    var request = {
        command: 'validatetoken',
        request: {
            userid: '5603ed5c14ccf60023133b04',
            accesstoken: '$2a$10$3XUp3YNgzUbMSGkOdtNES.kBzFTeW3zHFnvGf/QVRrfFjDaXu7hme',
            auth: affidummy,
            oncompleteeventkey: 'validatetoken.done',
            onerroreventkey: 'validatetoken.error'  
        },
        url: url
    }
    //var request = {
    //    command: 'refreshsession',
    //    request: {
    //        userid: '547efe4ad12654100bf3c230',
    //        oncompleteeventkey: 'refreshsession.done',
    //        onerroreventkey: 'refreshsession.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'credentials',
    //    request: {
    //        userid: '547efe4ad12654100bf3c230',
    //        oncompleteeventkey: 'credentials.done',
    //        onerroreventkey: 'credentials.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'restorepassword',
    //    request: {
    //        email: 'xisco@openmarket.travel',
    //        key: 'password2',
    //        oncompleteeventkey: 'restorepassword.done',
    //        onerroreventkey: 'restorepassword.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'changepassword',
    //    request: {
    //        email: 'xisco@openmarket.travel',
    //        oldpassword: 'password2',
    //        newpassword: 'password',
    //        oncompleteeventkey: 'changepassword.done',
    //        onerroreventkey: 'changepassword.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'changeemail',
    //    request: {
    //        oldemail: 'xisco@openmarket.travel',
    //        newemail: 'xisco22222@openmarket.travel',
    //        oncompleteeventkey: 'changeemail.done',
    //        onerroreventkey: 'changeemail.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'recoverpassword',
    //    request: {
    //        //email: 'xesc17@openmarket.travel',
    //        email: 'xisco@openmarket.travel',
    //        oncompleteeventkey: 'recoverpassword.done',
    //        onerroreventkey: 'recoverpassword.error'
    //    },
    //    url: url
    //};
    //var request = {
    //    command: 'removeaccount',
    //    request: {
    //        email: 'xesc17@openmarket.travel',
    //        oncompleteeventkey: 'removeaccount.done',
    //        onerroreventkey: 'removeaccount.error'
    //    },
    //    url: url
    //};

    var ytoclient = require('kioow.connector').connector;
    //start interface
    memberhisp.start();
    //hermes.start();
    //warming time...
    setTimeout(function () {
        //Test Request:
        var rq = ytoclient.send(request);
        rq.on(request.request.oncompleteeventkey, function (result) {
            console.log('TESTMEMBERSHIP () -> RESULTS: ###############################');
            console.log(result);
            console.log('END TESTMEMBERSHIP ()         ###############################');
        });
        rq.on(request.request.onerroreventkey, function (err) {
            console.error('TESTMEMBERSHIP () -> ERROR: ###############################');
            console.error(err);
            console.error('END TESTMEMBERSHIP ()       ###############################');
        });
    }, 7000);


}

function test5() {
    
    var url = 'https://localhost:6033';
    var endopointinterface = 'socket';
    var connection = require('kioow.connector')({
        url: url,   //url to the endpoint (could be any url...)
        endpointinterface: endopointinterface      //endpointinterface: 'http' or 'socket'
    });
    
    var request = {
        command: 'login',
        service: 'membership',
        request: {
            email: 'pablo@openmarket.travel',
            password: 'ecotango',
            oncompleteeventkey: 'login.done',
            onerroreventkey: 'login.error'
        },
    };
    
    
    var rq = connection.send(request);
    rq.on(request.request.oncompleteeventkey, function (result) {
        console.log('TESTMEMBERSHIP 5 () -> RESULTS: ###############################');
        console.log(result.dmc);
        console.log('END TESTMEMBERSHIP 5 ()         ###############################');
    });
    rq.on(request.request.onerroreventkey, function (err) {
        console.error('TESTMEMBERSHIP 5 () -> ERROR: ###############################');
        console.error(err);
        console.error('END TESTMEMBERSHIP 5 ()         ###############################');
    });


}

function testmail() { 
    var nodemailer = require('nodemailer');
    var mandrillTransport = require('nodemailer-mandrill-transport');
    var conf = {
        smtp: {
            service: 'Mandrill SMTP',
            host: 'smtp.mandrillapp.com',
            port: 587,
            auth: {
                user: 'pablo@openmarket.travel',
                pass: 'rGCc6bMbFKJrgj5y-XfRYQ'
            },
            apikey: 'rGCc6bMbFKJrgj5y-XfRYQ'
        }, 
        port: 8000
    };
    var mailmessage = {
        from: 'xisco@openmarket.travel',
        to: 'pablo@openmarket.travel',
        subject: 'Mail de test',
        html: '<h2>Esto es un mail de test</h2>'
    };

    var smtp = nodemailer.createTransport(mandrillTransport({
        auth: {
            apiKey: conf.smtp.apikey
        }
    }));

    smtp.sendMail(mailmessage, function (err, response) {
        if (err) {
            console.log('[SendingMail] : Error occured');
            console.log('[SendingMail] : ' + err.message);
            
        } else {
            console.log('[SendingMail] : Message sent successfully! to ' + mailmessage.to + ' - ' + mailmessage.subject);
            console.log(response);
            
        }
    });
}

function testsignup() { 
    var afi = {
        "_id" : "563a497e33ccb8a41b3694a5",
        "slug" : "afi536",
        "code" : "AFI536",
        "name" : "xesc17",
        "updatedOn" : "2015-11-04T18:07:58.794Z",
        "createdOn" : "2015-11-04T18:07:58.489Z",
        "fees" : {
            "unique" : 0,
            "groups" : 0,
            "tailormade" : 0,
            "flights" : 0
        },
        "images" : {
            "splash" : {
                "public_id" : "",
                "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
            },
            "logo" : {
                "public_id" : "",
                "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
            },
            "photo" : {
                "public_id" : "",
                "url" : "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
            }
        },
        "membership" : {
            "acceptterms" : false
        },
        "contact" : {
            "firstname" : "xescpereta",
            "lastname" : "xescpereta",
            "email" : "xesc17@openmarket.travel",
            "position" : "xescpereta",
            "skype" : ""
        },
        "company" : {
            "name" : "test",
            "legalname" : "test",
            "constitutionyear" : 1234,
            "phone" : "43534543543",
            "website" : "http://www.test.com",
            "taxid" : "4354543543",
            "location" : {
                "fulladdress" : "Parc BIT, 07120, Islas Baleares, España",
                "cp" : "07120",
                "city" : "Palma de Mallorca",
                "country" : "España",
                "countrycode" : "ES",
                "stateorprovince" : "Islas Baleares",
                "latitude" : 39.63628310000001,
                "longitude" : 2.632574699999964
            }
        },
        "__v" : 0,
        "user" : { email: 'xisco@openmarket.travel'}
    };
    var m = require('../mediator/hermes.mediator');
    var commandkey = 'notify.suscribers';

    var hermes = new m.HermesMediator();
    var rq = {
        subject: 'affiliate',
        action: 'new',
        data: afi
    };
    
    hermes.send(commandkey, rq);

}

function testlists() { 
    var core = require('../interface/core');
    core.configuration.port = 1999;
    var url = 'http://localhost:' + core.configuration.port;
    
    var request = {
        command: 'list',
        request: {
            query: { code: { $ne: null } },
            //lastcode: '560564fb6cd75cbc0ebe2604',
            //lastcode: 'AL3231417',
            maxresults: 3,
            orderby: 'code',
            type: 'desc',
            page: 'next',
            oncompleteeventkey: 'list.done',
            onerroreventkey: 'list.error',
            populate : [{
                    path: 'dmc', 
                    select : 'code name images company.name additionalinfo.description additionalinfo.description_es membership.b2bchanel'
                }],
            collectionname: 'DMCProducts',
            filterfields: null
        },
        url: url
    };
    
    var request = {
        command: 'list',
        request: {
            query: {
                $and: [{ idBooking: { $ne: null } }, 
                       { status: { $in: ["regularok", "regular1-2", "transfer1-2", "transferok2-2"] } }]
            },
            maxresults: 3,
            orderby: 'idBooking',
            //populate: [{ path: 'affiliate' }],
            type: 'desc',
            page: 'next',
            oncompleteeventkey: 'list.done',
            onerroreventkey: 'list.error',
            collectionname: 'Bookings',
            filterfields: null
        },
        url: url
    };

    //var request = {
    //    command: 'list',
    //    request: {
    //        query: { 'quotes.0': { $exists: true } },
    //        maxresults: 33,
    //        orderby: 'code',
    //        type: 'asc',
    //        page: 'next',
    //        oncompleteeventkey: 'list.done',
    //        onerroreventkey: 'list.error',
    //        populate: [{ path: 'affiliate', select : 'code company name slug' }, { path: 'quotes' }],
    //        collectionname: 'UserQueries',
    //        filterfields: null
    //    },
    //    url: url
    //}
    
    
    var ytoclient = require('kioow.connector').connector;
    //start interface...
    core.start();
    //warming time...
    setTimeout(function () {
        console.log('lets connect....');
        //Test Request:
        var rq = ytoclient.send(request);
        rq.on(request.request.oncompleteeventkey, function (result) {
            console.log('TESTLIST () -> RESULTS: ###############################');
            console.log(result);
            if (result.pager.items != null) {
                for (var i = 0, len = result.pager.items.length; i < len; i++) {
                    console.log(result.pager.items[i].code);
                    //console.log(result.pager.items[i].affiliate);
                    //console.log(result.pager.items[i].quotes);
                }
            }
            console.log('END TESTLIST ()         ###############################');
        });
        rq.on(request.request.onerroreventkey, function (err) {
            console.error('TESTLIST () -> ERROR: ###############################');
            console.error(err);
            console.error('END TESTLIST ()         ###############################');
        });
    }, 4000);
}

function testselfinvoke() {
    var common = require('kioow.common');
    var t = 0;
    var rt = (function (t) {
        return function (t) {
            console.log(t);
            var token = common.utils.getToken();
            var ct = common.eventtrigger.eventcarrier(token);
            setTimeout(function () {
                ct.emit('testevent', token + ' ------ ' + t);
            }, 3000);
            return ct;
        }
    })(t);
    t = 1;
    var ct1 = rt(t);
    t = 2;
    var ct2 = rt(t);
    console.log(ct1);
    console.log(ct2);
    ct1.on('testevent', function (token) { 
        console.log('CT1 : ' + token);
    });

    ct2.on('testevent', function (token) {
        console.log('CT2 : ' + token);
    });
    t = 3;
    var ct3 = rt(t);
    console.log(ct3);
    ct3.on('testevent', function (token) {
        console.log('CT3 : ' + token);
    });
    t = 4;
}

function testbackbone() {
    var common = require('kioow.common');
    var ytoclient = require('kioow.connector').BackboneCommander( {
            url: 'https://yourttootest.cloudapp.net:6033',   //url to the endpoint (could be any url...)
            endpointinterface: 'socket'      //endpointinterface: 'http' or 'socket'
        });
    //var ytoclient = require('kioow.connector')({
    //    url: 'https://yourttootest.cloudapp.net:6033',   //url to the endpoint (could be any url...)
    //    endpointinterface: 'socket'      //endpointinterface: 'http' or 'socket'
    //});
    
    var request = {
        command: 'tracking',
        service: 'backbone',
        request: {
            clear: false,
            oncompleteeventkey: 'tracking.done',
            onerroreventkey: 'tracking.error',
        }
    };
    
    //var request = {
    //    command: 'info',
    //    service: 'backbone',
    //    request: {
    //        keys: ['configuration', 'tracking'],
    //        oncompleteeventkey: 'info.done',
    //        onerroreventkey: 'info.error',
    //    }
    //};
    
    //var request = {
    //    command: 'reboot',
    //    service: 'backbone',
    //    request: {
    //        services: ['core'],
    //        oncompleteeventkey: 'reboot.done',
    //        onerroreventkey: 'reboot.error',
    //    }
    //};

    setTimeout(function () {
        console.log('lets connect....');
        //Test Request:
        var rq = ytoclient.send(request);
        rq.on(request.request.oncompleteeventkey, function (result) {
            console.log('TESTBACKBONE () -> RESULTS: ###############################');
            //console.log(result);
            var cmds = {};
            for (var prop in result) {
                var cmd = result[prop];
                cmds[cmd.command] = (cmds[cmd.command] != null) ? cmds[cmd.command] : { done: 0, error: 0, notserved: 0 };
                (cmd.done == null && cmd.errors == null) ? cmds[cmd.command].notserved++: null;
                (cmd.done != null && cmd.errors == null) ? cmds[cmd.command].done++: null;
                (cmd.errors != null && cmd.done == null) ? cmds[cmd.command].error++: null;
                
            }
            console.log(cmds);
            var fs = require('fs');
            fs.writeFile('c:/temp/backbone.track.txt', JSON.stringify(result));
            console.log('END TESTBACKBONE ()         ###############################');
        });
        rq.on(request.request.onerroreventkey, function (err) {
            console.error('TESTBACKBONE () -> ERROR: ###############################');
            console.error(err);
            console.error('END TESTBACKBONE ()         ###############################');
        });
        rq.on('backbone.error', function (err) { 
            console.error('TESTBACKBONE () -> CONNECTION ERROR: ###############################');
            console.error(err);
            console.error('END TESTBACKBONE ()         ###############################');
        });
    }, 4000);
}

function testsyncronize() {
    var ytobase = require('../base');
    var core = this.core = new ytobase.YourTTOOCore();
    var common = require('kioow.common');
    core.start();
    setTimeout(function () {
        core.list('Bookings').model.find({ idBooking: '00001247' }).exec(function (err, doc) {
            var editedbooking = require('./bookingdummy3');
            var originalbooking = doc[0];
            console.log(originalbooking.comments);
            oringinalbooking = common.utils.synchronyzeProperties(editedbooking, originalbooking);
            console.log(originalbooking.comments);
            originalbooking.save(function (err, doc) {
                err != null ? console.log('Error: %s', err): console.log('doc saved!');
            });
            //console.log(Object.prototype.toString.call(originalbooking));
        });
    },4000);
}

function testworker() {
    var worker = require('../interface/scheduler');
    //worker.configuration.port = 1998;
    worker.start();
    //setTimeout(function () {
    //    var common = require('kioow.common');
    //    var m = require('../mediator/scheduler.mediator');
    //    var handler = new m.SchedulerMediator(1998);
    //    var command = 'schedule';
    //    var data = {
    //        interval: 5,
    //        eventKeys: ['DESTINATIONS.FILES']
    //    };
    //    handler.send(command, data, function () { 
    //        console.log('sended and received...');
    //    });
    //    console.log('sended request for scheduling task...');
    //}, 4000);
}

function testauth() {
    var m = require('../mediator/auth.mediator');
    var handler = new m.AuthMediator();
    handler.start();
    var ct = 0;
    setInterval(function () {
        //var rq = { //invalid
        //    userid: '5603ed5c14ccf60023133b04',
        //    accesstoken: '$2a$10$3XUp3YNgzUbMSGkOdtNES.kBzFTeW3zHFnvGf/QVRrfFjDaXu7hme'
        //};
        ct++;
        console.log('launch test %s', ct);
        var rq = { //valid
            userid: 'dsd', //'5603ed5c14ccf60023133b04',
            accesstoken: '$2a$10$yezwl/y2cqA.G8oHohKPA.kcSmdtAduOatroVCLFpgxUhBv7UwnwG'
        }
        var cq = handler.validate(rq);
        cq.on('auth.done', function (cred) {
            console.log('TESTAUTH (%s) -> RESULTS: ###############################', ct);
            console.log(cred);
            console.log('END TESTAUTH (%s)         ###############################', ct);
        });
        cq.on('auth.error', function (err) {
            console.error('TESTAUTH (%s) -> CONNECTION ERROR: ###############################', ct);
            console.error(err);
            console.error('END TESTAUTH (%s)         ###############################', ct);
        });
    }, 4000);
}

for (var prop in testlist) {
    if (testlist[prop]) { 
        ct[prop]();
    }
}