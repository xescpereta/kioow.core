module.exports = {
  "_id": "5603ed5c14ccf60023133b04",
  "apikey": "627bc713c9c47120e5b12e863b8a8f06eb097952",
  "username": "xesc",
  "email": "xesc@openmarket.travel",
  "code": "AFI528",
  "phone": "",
  "password": "$2a$10$1cMIF5MwL1Z7YGBh9rn9teUJdzxr61lipyM19UwkekOquZnPXMa/.",
  "isDMC": false,
  "isTraveler": false,
  "isAdmin": false,
  "isAffiliate": true,
  "active": true,
  "isLocal": true,
  "isFacebookLinked": false,
  "isTwitterLinked": false,
  "isGoogleLinked": false,
  "updatedOn": "2015-09-24T12:32:29.063Z",
  "createdOn": "2015-09-24T12:32:28.802Z",
  "roles": [
    "55e82574da8c472e7e8202ef"
  ],
  "photo": {
    "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
  },
  "__v": 0
}