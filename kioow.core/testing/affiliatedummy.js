﻿module.exports = {
    "_id": "5603ed5c14ccf60023133b05",
    "slug": "afi528",
    "code": "AFI528",
    "name": "xesc#1",
    "__v": 0,
    "user": {
        "_id": "5603ed5c14ccf60023133b04",
        "email": "xesc@openmarket.travel",
        "code": "AFI528"
    },
    "updatedOn": "2015-12-10T16:07:47.294Z",
    "createdOn": "2015-09-24T12:32:28.810Z",
    "fees": {
        "flights": 2,
        "groups": 34,
        "tailormade": 34,
        "unique": 6
    },
    "images": {
        "splash": {
            "public_id": "",
            "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
        },
        "logo": {
            "public_id": "lcb4hu8zbzv6uquqclrk",
            "url": "http://res.cloudinary.com/openmarket-travel/image/upload/v1443719803/lcb4hu8zbzv6uquqclrk.jpg",
            "format": "jpg",
            "height": 2048,
            "resource_type": "image",
            "secure_url": "https://res.cloudinary.com/openmarket-travel/image/upload/v1443719803/lcb4hu8zbzv6uquqclrk.jpg",
            "signature": "b001b6d7b4fe729f0c41f0d17cae0c45074d96f6",
            "version": 1443719803,
            "width": 1536
        },
        "photo": {
            "public_id": "",
            "url": "http://res.cloudinary.com/open-market-travel/image/upload/v1426853495/assets/avatar.jpg"
        }
    },
    "membership": {
        "omtmargin": 3,
        "acceptterms": true,
        "membershipDate": "2015-10-01T15:53:02.454Z",
        "registervalid": true,
        "requestdelete": false
    },
    "company": {
        "name": "xisco.travel",
        "legalname": "xisco.travel",
        "constitutionyear": 1989,
        "phone": "64904541E",
        "website": "",
        "taxid": "X99889889s",
        "location": {
            "city": "Palma de Mallorca",
            "country": "Spain",
            "countrycode": "ES",
            "cp": "07120",
            "fulladdress": "Parc BIT, 07120, Islas Baleares, Spain",
            "latitude": 39.63628310000001,
            "longitude": 2.632574699999964,
            "stateorprovince": "Islas Baleares"
        }
    }
}