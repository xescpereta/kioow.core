﻿module.exports = function (kioowcore) {
    var ctypes = {
        Text : require('./text'),
        Textarea : require('./textarea'),
        Email : require('./email'),
        Url : require('./url'),
        Number : require('./number'),
        Money : require('./money'),
        Boolean : require('./boolean'),
        Date : require('./date'),
        Datetime : require('./datetime'),
        Html : require('./html'),
        Name : require('./name'),
        Password : require('./password'),
        Location : require('./location'),
        CloudinaryImage : require('./cloudinaryimage'),
        Relationship : require('./relationship')(kioowcore)
    }
    return ctypes;
}

//exports.Text = require('./text');
//exports.Textarea = require('./textarea');
////this.Key = require('./key');
//exports.Email = require('./email');
//exports.Url = require('./url');
//exports.Number = require('./number');
//exports.Money = require('./money');
//exports.Boolean = require('./boolean');
//exports.Date = require('./date');
//exports.Datetime = require('./datetime');
////this.Select = require('./select');
//exports.Html = require('./html');
////this.Markdown = require('./markdown');
//exports.Name = require('./name');
//exports.Password = require('./password');
//exports.Location = require('./location');
//exports.CloudinaryImage = require('./cloudinaryimage');
//exports.CloudinaryImages = require('./cloudinaryimages');
////this.S3File = require('./s3file');
////this.Embedly = require('./embedly');
//exports.Relationship = require('./relationship');