﻿var common = require('kioow.common');

var MailStack = function () {
    //the mail stack
    this.mailqueue = new common.queue.Queue();
    //reference to the smpt sender
    var nodemailer = require('nodemailer');
    var mandrillTransport = require('nodemailer-mandrill-transport');

    var configfile = '../configurations/mailstack.config';
    var conf = require(configfile);
    
    this.configuration = conf.configuration;
    
    this.smtp = nodemailer.createTransport(mandrillTransport({
        auth: {
            apiKey: this.configuration.smtp.apikey
        }
    }));
    this.checkinterval = null;

    this.icplistener = new common.icp.ICPlistener([{
            messagekey: 'command', 
            commandpropertyname: 'command', 
            oncompletepropertyname: 'oncompleteeventkey', 
            onerrorpropertyname: 'onerroreventkey'
        }]);
    this.errorhandler = { notassigned: true };
}

//inherits
var eventThis = common.eventtrigger;
eventThis.eventtrigger(MailStack);

MailStack.prototype.send = function (mailmessage, callback) {
    var rs = { ResultOK: false, Message: '' };
    if (mailmessage != null) {
        mailstack.smtp.sendMail(mailmessage, function (err, response) {
            if (err) {
                console.log('[SendingMail] : Error occured');
                console.log('[SendingMail] : ' + err.message);
                rs.ResultOK = false;
                rs.Message = err.message;
                if (callback) {
                    callback(rs);
                }
            } else {
                console.log('[SendingMail] : Message sent successfully! to ' + mailmessage.to + ' - ' + mailmessage.subject);
                console.log(response);
                rs.ResultOK = true
                rs.Message = 'Message sent successfully!';
                if (callback) {
                    callback(rs);
                }
            }
        });
    } else {
        rs.Message = 'El email es nulo';
        if (callback) {
            callback(rs);
        }
    }
}

MailStack.prototype.start = function () {
    //start listening...

    var remoteThis = common.remoteendpoint;
    remoteThis.RemoteServerEndPoint('Mailstack server Remote End Point', mailstack.configuration.port, mailstack);

    mailstack.remoteserver.listen();
    mailstack.remoteserver.io.on('connection', function (socket) { 
        console.log('new connection to mailstack...');
        var router = require('../socketroutes/mailing.actions')(mailstack, socket);
    });
    mailstack.remoteserver.io.on('disconnect', function (socket) {
        console.log('a client has disconnected');
    });
    //start the queue polling...
    mailstack.checkinterval = setInterval(function () {
        var mail = mailstack.mailqueue.dequeue();
        if (mail != null) {
            console.log('let\'s send mail...');
            try {
                
                mailstack.send(mail, function (result) {
                    if (result.ResultOK) {
                        console.log('Mail sended [' + mail.to + '] -> Subject: ' + mail.subject);
                    }
                    else {
                        console.log(result.Message);
                        mailstack.mailqueue.enqueue(mail);
                    }
                    
                });
            }
            catch (err) {
                console.log(err);
                mailstack.mailqueue.enqueue(mail);
            }
        }
    }, 10000);

    mailstack.icplistener.on('command', function (socket) {
        var icprouter = require('../socketroutes/mailing.actions')(mailstack, socket);
    });

    //mailstack.errorhandler = new require('../middleware/unhandlederror').ExceptionHandler(mailstack);
}


var mailstack = module.exports = exports = new MailStack;

