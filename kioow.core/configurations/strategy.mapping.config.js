﻿var configuration = {
    strategiespath: 'd:/node/kioow/core/kioow.core/strategy/',
    visitorspath: 'd:/node/kioow/core/kioow.core/visitors/',
    core: {
        strategiesfolder: 'core',
        find: 'find',
        read: 'read',
        search: 'search',
        inspiration: 'inspiration',
        cloudinaryupload: 'cloudinaryupload',
        upload: 'upload', 
        findone: 'findone',
        list: 'list',
        translate: 'translate',
        product: 'product',
        subscribers: 'subscribers'
    }, 
    scheduled: {
        strategiesfolder: 'scheduled',
        sitemap: 'sitemap.generation',
        dummy: 'dummy'
    },
    scheduler: {
        strategiesfolder: 'scheduler',
        schedule: 'schedule',
        dummy: 'dummy'
    },
    cms: {
        strategiesfolder: 'cms'
    },
    membership: {
        strategiesfolder: 'membership',
        signup: 'signup',
        login: 'login'
    },
    memento: {
        strategiesfolder: 'memento',
        push: 'push',
        pull: 'pull'
    },
    backbone: {
        strategiesfolder: 'backbone',
        reboot: 'reboot',
        test: 'test'
    },
    hermes: {
        strategiesfolder: 'hermes'
    }
};

module.exports.configuration = configuration;