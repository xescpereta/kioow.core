﻿//TASKS CONFIGURATION...
var yourTTOOProcesses = [
    //{
    //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
    //    interval: 0, 
    //    eventKey: 'HERMES.PROCESS.STARTED', 
    //    name: 'Hermes process instance',
    //    runInmediately : true,
    //    args: [ 
    //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
    //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/hermes.js', 
    //        '-port=7000',
    //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/hermes.config'
    //    ],
    //    command: 'c:/nodejs/node.exe',
    //    outputFile: 'c:/temp/trace/hermesserver.log',
    //    errorFile: 'c:/temp/error/hermesserver.error.log',
    //    url: { host: 'http://localhost', port: 7000 },
    //    webgardenCount: {
    //        count: 1,
    //        memory: null
    //    },
    //    dependencies: []
    //},
    //{
    //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
    //    interval: 0, 
    //    eventKey: 'HIPERION.PROCESS.STARTED', 
    //    name: 'Hiperion process instance',
    //    runInmediately : true,
    //    args: [ 
    //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
    //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/hiperion.js', 
    //        '-port=2000',
    //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/hiperion.config'
    //    ],
    //    command: 'c:/nodejs/node.exe',
    //    outputFile: 'c:/temp/trace/hiperionserver.log',
    //    errorFile: 'c:/temp/error/hiperionserver.error.log',
    //    url: { host: 'http://localhost', port: 2000 },
    //    webgardenCount: {
    //        count: 1,
    //        memory: null
    //    },
    //    dependencies: []
    //},
    {
        cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
        interval: 0, 
        eventKey: 'BACKBONE.PROCESS.STARTED', 
        name: 'Backbone process instance',
        runInmediately : true,
        args: [ 
            'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
            '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/backbone.js', 
            '-port=6033',
            '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/backbone.config'
        ],
        command: 'c:/nodejs/node.exe',
        outputFile: 'c:/temp/trace/backbone.log',
        errorFile: 'c:/temp/error/backbone.error.log',
        url: { host: 'http://localhost', port: 6033 },
        webgardenCount: {
            count: 1,
            memory: null
        },
        dependencies: []
    },
    //{
    //    cwd: 'C:/development/node/yourttoo.system/yourttoo.webserver',
    //    interval: 0, 
    //    eventKey: 'WEBSERVER.PROCESS.STARTED', 
    //    name: 'Webserver process instance',
    //    runInmediately : true,
    //    args: [ 
    //        'C:/development/node/yourttoo.system/yourttoo.webserver/ytowebserver.js'
    //    ],
    //    command: 'c:/nodejs/node.exe',
    //    outputFile: 'c:/temp/trace/webserver.yttoo.log',
    //    errorFile: 'c:/temp/error/webserver.yttoo.error.log',
    //    url: { host: 'http://localhost', port: 80 },
    //    webgardenCount: {
    //        count: 1,
    //        memory: null
    //    },
    //    dependencies: []
    //},
    //{
    //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
    //    interval: 0, 
    //    eventKey: 'CORE.REST.OMT.PROCESS.STARTED', 
    //    name: 'CORE REST OMT process instance',
    //    runInmediately : true,
    //    args: ['C:/development/node/yourttoo/core/yourttoo.core/servers/corerestfulserver.js'],
    //    command: 'c:/nodejs/node.exe',
    //    outputFile: 'c:/temp/trace/corerestserver.yttoo.log',
    //    errorFile: 'c:/temp/error/corerestserver.yttoo.error.log',
    //    url: { host: 'http://localhost', port: 81 },
    //    webgardenCount: {
    //        count: 1,
    //        memory: null
    //    },
    //    dependencies: []
    //},
    //{
    //    cwd: 'C:/development/node/yourttoo.thearchitect',
    //    interval: 0, 
    //    eventKey: 'THEORACLE.PROCESS.STARTED', 
    //    name: 'THEORACLE process instance',
    //    runInmediately : true,
    //    args: ['C:/development/node/yourttoo/core/yourttoo.thearchitect/alm/theoracle.js'],
    //    command: 'c:/nodejs/node.exe',
    //    outputFile: 'c:/temp/trace/theoracle.log',
    //    errorFile: 'c:/temp/error/theoracle.error.log',
    //    webgardenCount: {
    //        count: 1,
    //        memory: null
    //    },
    //    dependencies: []
    //}
];

module.exports.processes = yourTTOOProcesses;