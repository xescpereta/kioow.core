﻿//taskpath: name of the file with the implementation
//interval: time to repeat execution, period in minutes...
//eventKey: an unique name string(slug) for the task
//name: descriptive name
//runinmediately: set it at true to run the process as soon as the scheduler is arranged

var scheduledConfigurations = [
    //{
    //    TaskPath: 'dummy', 
    //    Interval: 1, 
    //    EventKey: 'DUMMY.TASK', 
    //    Name: 'DUMMY.TASK',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'users.keys', 
    //    Interval: 0, 
    //    EventKey: 'USERS.APIKEYS', 
    //    Name: 'USER API KEYS GENERATION',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'destinationsrepository.cms', 
    //    Interval: 350, 
    //    EventKey: 'CMS.DESTINATIONS.UPDATE', 
    //    Name: 'CMS DESTINATION UPDATE PROCESS',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'destinationsproduct.filler', 
    //    Interval: 350, 
    //    EventKey: 'PRODUCT.DESTINATIONS.UPDATE', 
    //    Name: 'PRODUCT DESTINATION UPDATE PROCESS',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'destinationfiles', 
    //    Interval: 0, 
    //    EventKey: 'DESTINATIONS.FILES', 
    //    Name: 'DESTINATION FILES GENERATION PROCESS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'destinationsrepository.cms.latlon.fixer', 
    //    Interval: 350, 
    //    EventKey: 'DESTINATIONS.REPEATED', 
    //    Name: 'DESTINATION REPEATED FIXER PROCESS',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'b2bchannel.activator', 
    //    Interval: 350, 
    //    EventKey: 'DMC.B2B.CHANNEL.ACTIVATION', 
    //    Name: 'DMC B2B CHANNEL ACTIVATION',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'destinationsrepeat.fixes', 
    //    Interval: 350, 
    //    EventKey: 'DESTINATIONS.REPEATED', 
    //    Name: 'DESTINATION REPEATED FIXER PROCESS',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'product.fitur.updater', 
    //    Interval: 350, 
    //    EventKey: 'DESTINATIONS.REPEATED', 
    //    Name: 'DESTINATION REPEATED FIXER PROCESS',
    //    RunInmediately : true
    //},
    //{
    //    TaskPath: 'cloudinary.check', 
    //    Interval: 15, 
    //    EventKey: 'CLOUDINARY.RESOURCES.CHECK', 
    //    Name: 'CLOUDINARY RESOURCES CHECK PROCESS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'sitemap.generation', 
    //    Interval: 140, 
    //    EventKey: 'SITEMAP.UPDATE', 
    //    Name: 'SITEMAP UPDATE PROCESS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'dmcproducts.landing', 
    //    Interval: 15, 
    //    EventKey: 'CACHE.BUILDING.LANDING', 
    //    Name: 'CACHE BUILDING FOR LANDING CONTENT',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.dmcproducts', 
    //    Interval: 15, 
    //    EventKey: 'CACHE.BUILDING.PRODUCTS', 
    //    Name: 'CACHE BUILDING FOR PRODUCTS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.promotions', 
    //    Interval: 10, 
    //    EventKey: 'CACHE.BUILDING.PROMOTIONS', 
    //    Name: 'CACHE BUILDING FOR PROMOTIONS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.products.countries', 
    //    Interval: 5, 
    //    EventKey: 'CACHE.BUILDING.PRODUCTS.COUNTRIES', 
    //    Name: 'CACHE BUILDING FOR PRODUCT COUNTRIES AND LOCATIONS',
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.dmcs', 
    //    Interval: 10,
    //    EventKey: 'CACHE.BUILDING.DMC',
    //    Name: 'CACHE BUILDING FOR DMCs' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.cms', 
    //    Interval: 10,
    //    EventKey: 'CACHE.BUILDING.CMS',
    //    Name: 'CACHE BUILDING FOR CMS Data' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'fullproduct.update', 
    //    Interval: 600,
    //    EventKey: 'SCHEDULED.PRODUCT.UPDATE',
    //    Name: 'PRODUCT UPDATING Data' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'remember60.booking', 
    //    Interval: 720, //Cada 12 horas
    //    EventKey: 'SCHEDULED.BOOKING.UPDATE',
    //    Name: 'BOOKING REMEMBER 60' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'finishedBookings.update', 
    //    Interval: 720, //Cada 12 horas
    //    EventKey: 'SCHEDULED.FINISHEDBOOKINGS.UPDATE',
    //    Name: 'BOOKING FINISHED UPDATING' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'cache.popupcontinents', 
    //    Interval: 720, //Cada 12 horas
    //    EventKey: 'SCHEDULED.POPUPCONTINENT.CONTENT',
    //    Name: 'POPUP CONTINENT CACHE' ,
    //    RunInmediately : false
    //},
    //{
    //    TaskPath: 'updater.booking', 
    //    Interval: 720, //Cada 12 horas
    //    EventKey: 'SCHEDULED.BOOKING.UPDATER',
    //    Name: 'BOOKING MODEL UPDATER' ,
    //    RunInmediately : true
    //}
];
var configuration = {
    port: 9000,
    scheduledConfigurations : scheduledConfigurations
}
module.exports.scheduledConfigurations = scheduledConfigurations;
module.exports.configuration = configuration;