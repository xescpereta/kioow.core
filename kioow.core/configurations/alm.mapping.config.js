﻿
var DeployMappings = [
    {
        slug: 'core',
        name: 'Core Path',
        source: 'c:/omt/alm/core/',
        target: 'c:/openmarket/core/',
        backup: 'c:/omt.backup/',
        zippedbackup: true,
        exclude: [],
        specified: [
            
        ]
    },
    {
        slug: 'webserver',
        name: 'WebServer Path',
        source: 'c:/omt/alm/server/',
        target: 'c:/openmarket/server/',
        backup: 'c:/omt.backup/',
        zippedbackup: true,
        exclude: [],
        specified: [
            
        ]
    },
    {
        slug: 'public',
        name: 'Public Resources Path',
        source: 'c:/omt/alm/public/',
        target: 'c:/openmarket/public/',
        backup: 'c:/omt.backup/',
        zippedbackup: true,
        exclude: [],
        specified: [
            
        ]
    },
    {
        slug: 'cms',
        name: 'CMS Path',
        source: 'c:/omt/alm/cms/',
        target: 'c:/openmarket/cms/',
        backup: 'c:/omt.backup/',
        zippedbackup: true,
        exclude: [],
        specified: [
            
        ]
    },
    {
        slug: 'annatar',
        name: 'Annatar Path',
        source: 'c:/omt/alm/annatar/',
        target: 'c:/openmarket/annatar/',
        backup: 'c:/omt.backup/',
        zippedbackup: true,
        exclude: [],
        specified: [
            
        ]
    },
    {
        slug: 'mongo',
        name: 'Mongo BBDD Path',
        source: 'c:/openmarket/dbdata/',
        target: 'c:/openmarket/dbdata/',
        backup: 'c:/omt.backup/dbdata/',
        zippedbackup: false,
        exclude: [],
        specified: [
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'cms-openmarket.0', targetname: 'cms-openmarket.0' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'cms-openmarket.ns', targetname: 'cms-openmarket.ns' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'openmarkettravel.0', targetname: 'openmarkettravel.0' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'openmarkettravel.1', targetname: 'openmarkettravel.1' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'openmarkettravel.2', targetname: 'openmarkettravel.2' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'openmarkettravel.ns', targetname: 'openmarkettravel.ns' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'operations-openmarket.0', targetname: 'operations-openmarket.0' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'operations-openmarket.1', targetname: 'operations-openmarket.1' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'operations-openmarket.2', targetname: 'operations-openmarket.2' },
            { type: 'file', directory: 'c:/omt.backup/dbdata/', sourcename: 'operations-openmarket.ns', targetname: 'operations-openmarket.ns' },
            { type: 'directory', directory: 'c:/omt.backup/dbdata/', sourcename: 'journal', targetname: 'journal' },
        ]
    },
];

var GITMappings = 
    {
        name: 'Release Open Market Travel',
        url: 'https://xiscorossello@bitbucket.org/xiscorossello/omt-release.git',
        relurl: 'xiscorossello@bitbucket.org/xiscorossello/omt-release.git',
        source: 'd:/alm/omt/.git',
        target: 'd:/alm/omt/',
        user: 'xiscorossello',
        password: 'kukusumuxu',
        slug: 'omt-release',
        owner: 'xiscorossello'
        //target: 'd:/alm/omt',
    }
;

module.exports.DeployMappings = DeployMappings;
module.exports.GITMappings = GITMappings;