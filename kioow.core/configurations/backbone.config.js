﻿var configuration = {
    port: 6033,
    ssl: {
        enabled: true,
        keyfile: 'C:/development/node/yourttoo/resources/ssl/ytokey.pem',
        certfile: 'C:/development/node/yourttoo/resources/ssl/ytocert.pem'
    },
    clusters: [
        {
            name: 'mailstack',
            balancestrategy: 'round-robin',
            comm: 'socket',
            processes: [
                {
                    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                    interval: 0, 
                    eventKey: 'MAILSTACK.PROCESS.STARTED', 
                    name: 'MAILSTACK process instance',
                    runInmediately : true,
                    args: [ 
                        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
                        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/mailstack.js', 
                        '-port=8000',
                        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/mailstack.config'
                    ],
                    command: 'c:/nodejs/node.exe',
                    outputFile: 'c:/temp/trace/mailstackserver.log',
                    errorFile: 'c:/temp/error/mailstackserver.error.log',
                    url: { host: 'http://localhost', port: 8000 },
                    webgardenCount: {
                        count: 1,
                        memory: null
                    },
                    dependencies: []
                }
            ]
        },
        {
            name: 'memento',
            balancestrategy: 'round-robin',
            comm: 'socket',
            processes: [
                {
                    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                    interval: 0, 
                    eventKey: 'MEMENTO.PROCESS.STARTED', 
                    name: 'MEMENTO process instance 1',
                    runInmediately : true,
                    args: [ 
                        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
                        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/memento.js', 
                        '-port=5000',
                        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/memento.config'
                    ],
                    command: 'c:/nodejs/node.exe',
                    outputFile: 'c:/temp/trace/mementoserver.1.log',
                    errorFile: 'c:/temp/error/mementoserver.1.error.log',
                    url: { host: 'http://localhost', port: 5000 },
                    webgardenCount: {
                        count: 1,
                        memory: null
                    },
                    dependencies: []
                },
                {
                    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                    interval: 0, 
                    eventKey: 'MEMENTO.PROCESS.STARTED', 
                    name: 'MEMENTO process instance 2',
                    runInmediately : true,
                    args: [
                        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle', 
                        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/memento.js', 
                        '-port=5001',
                        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/memento.config'
                    ],
                    command: 'c:/nodejs/node.exe',
                    outputFile: 'c:/temp/trace/mementoserver.2.log',
                    errorFile: 'c:/temp/error/mementoserver.2.error.log',
                    url: { host: 'http://localhost', port: 5001 },
                    webgardenCount: {
                        count: 1,
                        memory: null
                    },
                    dependencies: []
                }
            ]
        },
        {
            name: 'membership',
            balancestrategy: 'round-robin',
            comm: 'socket',
            processes: [
                {
                    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                    interval: 0, 
                    eventKey: 'MEMBERSHIP.PROCESS.STARTED', 
                    name: 'MEMBERSHIP process instance 1',
                    runInmediately : true,
                    args: [
                        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/membership.js',
                        '-port=6000',
                        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/membership.config'
                    ],
                    command: 'c:/nodejs/node.exe',
                    outputFile: 'c:/temp/trace/membershipserver.1.log',
                    errorFile: 'c:/temp/error/membershipserver.1.error.log',
                    url: { host: 'http://localhost', port: 6000 },
                    webgardenCount: {
                        count: 1,
                        memory: null
                    },
                    dependencies: []
                },
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'MEMBERSHIP.PROCESS.STARTED', 
                //    name: 'MEMBERSHIP process instance 2',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/membership.js',
                //        '-port=6001',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/membership.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/membershipserver.2.log',
                //    errorFile: 'c:/temp/error/membershipserver.2.error.log',
                //    url: { host: 'http://localhost', port: 6001 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //}
            ]
        },
        //{
        //    name: 'worker',
        //    balancestrategy: 'round-robin',
        //    comm: 'socket',
        //    processes: [
        //        {
        //            cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
        //            interval: 0, 
        //            eventKey: 'WORKER.PROCESS.STARTED', 
        //            name: 'WORKER process instance',
        //            runInmediately : true,
        //            args: [
        //                'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
        //                '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/scheduler.js', 
        //                '-port=9000',
        //                '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/scheduler.config'
        //            ],
        //            command: 'c:/nodejs/node.exe',
        //            outputFile: 'c:/temp/trace/workerserver.log',
        //            errorFile: 'c:/temp/error/workerserver.error.log',
        //            url: { host: 'http://localhost', port: 9000 },
        //            webgardenCount: {
        //                count: 1,
        //                memory: null
        //            },
        //            dependencies: []
        //        },
        //    ]
        //},
        {
            name: 'core',
            balancestrategy: 'round-robin',
            comm: 'socket',
            processes: [
                {
                    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                    interval: 0, 
                    eventKey: 'CORE.1.OMT.PROCESS.STARTED', 
                    name: 'CORE 1 OMT process instance',
                    runInmediately : true,
                    args: [
                        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                        '-port=4000',
                        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                    ],
                    command: 'c:/nodejs/node.exe',
                    outputFile: 'c:/temp/trace/coreserver.OMT.1.log',
                    errorFile: 'c:/temp/error/coreserver.OMT.1.error.log',
                    url: { host: 'http://localhost', port: 4000 },
                    webgardenCount: {
                        count: 1,
                        memory: null
                    },
                    dependencies: []
                },
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'CORE.2.OMT.PROCESS.STARTED', 
                //    name: 'CORE 2 OMT process instance',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                //        '-port=4001',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/coreserver.OMT.2.log',
                //    errorFile: 'c:/temp/error/coreserver.OMT.2.error.log',
                //    url: { host: 'http://localhost', port: 4001 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //},
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'CORE.3.OMT.PROCESS.STARTED', 
                //    name: 'CORE 3 OMT process instance',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                //        '-port=4002',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/coreserver.OMT.3.log',
                //    errorFile: 'c:/temp/error/coreserver.OMT.3.error.log',
                //    url: { host: 'http://localhost', port: 4002 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //},
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'CORE.1.OMT.PROCESS.STARTED', 
                //    name: 'CORE 4 OMT process instance',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                //        '-port=4003',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/coreserver.OMT.4.log',
                //    errorFile: 'c:/temp/error/coreserver.OMT.4.error.log',
                //    url: { host: 'http://localhost', port: 4003 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //},
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'CORE.5.OMT.PROCESS.STARTED', 
                //    name: 'CORE 5 OMT process instance',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                //        '-port=4004',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/coreserver.OMT.5.log',
                //    errorFile: 'c:/temp/error/coreserver.OMT.5.error.log',
                //    url: { host: 'http://localhost', port: 4004 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //},
                //{
                //    cwd: 'C:/development/node/yourttoo/core/yourttoo.core',
                //    interval: 0, 
                //    eventKey: 'CORE.6.OMT.PROCESS.STARTED', 
                //    name: 'CORE 6 OMT process instance',
                //    runInmediately : true,
                //    args: [
                //        'C:/development/node/yourttoo/core/yourttoo.core/servers/backboneprocess.shuttle',
                //        '-script=C:/development/node/yourttoo/core/yourttoo.core/interface/core.js', 
                //        '-port=4005',
                //        '-config=C:/development/node/yourttoo/core/yourttoo.core/configurations/core.config'
                //    ],
                //    command: 'c:/nodejs/node.exe',
                //    outputFile: 'c:/temp/trace/coreserver.OMT.6.log',
                //    errorFile: 'c:/temp/error/coreserver.OMT.6.error.log',
                //    url: { host: 'http://localhost', port: 4005 },
                //    webgardenCount: {
                //        count: 1,
                //        memory: null
                //    },
                //    dependencies: []
                //}
            ]
        }
    ]
}

module.exports.configuration = configuration;