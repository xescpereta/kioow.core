﻿var restconfig = {
    port: 3000,
    publicdirectory: 'C:/development/node/yourttoo.travel/public',
    resourcesdirectory: 'C:/development/node/yourttoo.travel/yourttoo.travel.core/resources',
    routespaths: [
        'C:/development/node/yourttoo/core/yourttoo.core/httproutes/api.js',
        'C:/development/node/yourttoo/core/yourttoo.core/httproutes/api.test.js'
    ],
    ssl: {
        enabled: false,
        keyfile: 'C:/development/node/yourttoo/resources/ssl/ytokey.pem',
        certfile: 'C:/development/node/yourttoo/resources/ssl/ytocert.pem'
    },
    api: {
        url: 'https://localhost:6033',
        endpointinterface: 'socket'
    }
}

module.exports.configuration = restconfig;