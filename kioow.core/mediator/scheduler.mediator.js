﻿var SchedulerMediator = function (schedulerport) {
    this.port = schedulerport || 9000;
    this.url = 'http://localhost:' + this.port;
}

SchedulerMediator.prototype.send = function (key, data, callback) {
    var rqcn = require('kioow.connector').connector;
    var rq = {
        command: key,
        request: data,
        url: this.url
    };
    rq.request.oncompleteeventkey = key + '.done';
    rq.request.onerroreventkey = key + '.error';

    var rqcommand = rqcn.send(rq, function (results) {
        console.log('Hermes item sent succesfully...');
        console.log(results);
        if (callback) { 
            callback(results);
        }
    }, function (err) {
        console.log(err);
        if (callback) {
            callback(err);
        }
    });
}

SchedulerMediator.prototype.getsubject = function (collectionname) {
    var common = require('kioow.common');
    var _ = require('underscore');
    var subject = null;
    var related = _.filter(common.staticdata.hermessuscriptions, function (subscription) {
        return (subscription.relatedcollections.indexOf(collectionname) >= 0);
    });

    subject = (related != null && related.length > 0) ? related[0].subject : 'allactions';
    return subject;
}

module.exports.SchedulerMediator = SchedulerMediator;