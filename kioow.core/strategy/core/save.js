﻿var common = require('kioow.common');

module.exports = function (options, callback, errorcallback) {
    var core = options.core;
    
    var mongo = core.mongo;
    var coreobj = core.corebase;
    
    var query = null;
    if (options.query != null) {
        query = options.query;
    }
    var filter = { query: query, collectionname: options.collectionname };
    var cev = common.eventtrigger.eventcarrier(common.utils.getToken());
    var originaldata = null;

    cev.on('saved.ok', function (hdata) {
        var data = hdata;
        var collection = options.collectionname;
        
        var mmed = require('../../mediator/hermes.mediator');
        var mediator = new mmed.HermesMediator();

        var action = (originaldata != null) ? 'update' : 'new';
        originaldata = originaldata || data;
        
        var hresult = null;
        switch (action) {
            case 'update':
                hresult = { current: data, original: originaldata };
                break;
            case 'new':
                hresult = data;
                break;
        }
        var subject = mediator.getsubject(collection);
        console.log('notify hermes subject: ' + subject);
        var commandkey = 'notify.suscribers';
        var hrq = {
            subject: subject,
            action: action,
            data: hresult,
            oncompleteeventkey: 'notify.suscribers.done',
            onerroreventkey: 'notify.suscribers.error'
        };
        
        mediator.send(commandkey, hrq, function (result) {
            console.log('Hermes ' + subject + ' notified event: ' + hrq.action);
        });

    });
    
    cev.on('finded.ok', function (bbddobj) {
        var source = options.data;
        if (bbddobj != null && source != null) {
            //the object exists in BBDD
            console.log('target: get! %s', bbddobj._id);
            console.log('source: its! %s', source._id);
            originaldata = (bbddobj != null) ? bbddobj.toObject() : null;
            bbddobj = common.utils.synchronyzeProperties(source, bbddobj);
            console.log('after synchro...');
            cev.emit('model.ready', bbddobj);
        } else {
            //create new...
            console.log('a new model object to create...');
            require('../../factory/codesgenerator')(mongo, options.collectionname, function (cbcode) {
                //reformat the code 
                cbcode = require('../../factory/codesformatter')({
                    code: cbcode,
                    collectionname: options.collectionname,
                    document: source
                });
                //very special case....
                if (options.collectionname == 'Bookings') {
                    source.idBooking = cbcode;
                } else {
                    source.code = cbcode; 
                }
                bbddobj = mongo.getmodel({ collectionname: options.collectionname, modelobject: source });
                cev.emit('model.ready', bbddobj);
            });
        }
        
    });
    
    cev.on('model.ready', function (document) { 
        
        mongo.save(document, function (results) {
            if (results.ResultOK == true) {
                console.log('Command Save SUCCESSFUL!!');
                callback(results.Data);

                options.populate == null ? cev.emit('saved.ok', results.Data.toObject()) : 
                coreobj.list(options.collectionname).model.populate(results.Data, options.populate, function (err, populatedoc) {
                    cev.emit('saved.ok', populatedoc.toObject());
                });
            } else {
                console.log('Command Save FAILED!!');
                errorcallback(results);
            }

        });
    });

    cev.on('finded.error', function (rs) {
        console.log('error finding model...');
        errorcallback(rs);
    });
    
    //check if it's new
    if (filter.query == null) {
        cev.emit('finded.ok', null);
    } else {
        mongo.findone(filter, function (result) {
            if (result.ResultOK == true) {
                options.populate == null ? cev.emit('finded.ok', result.Data) : 
                coreobj.list(options.collectionname).model.populate(result.Data, options.populate, function (err, populatedoc) { 
                    if (err) {
                        cev.emit('finded.error', err);
                    } else {
                        cev.emit('finded.ok', populatedoc)
                    }
                });
            }
            else {
                cev.emit('finded.error', result);
            }
        });
    }
}
