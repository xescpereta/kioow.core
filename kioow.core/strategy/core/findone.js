﻿module.exports = function (options, callback, errorcallback) {
    var core = options.core;

    var mongo = core.mongo;
    var coreobj = core.corebase;
    var _ = require('underscore');
    var helper = require('./helpers');
    var exchanges = null;
    var common = require('kioow.common');
    var currencys = common.staticdata.currencys;
    var m_currentcurrency = options.currency || 'EUR';
    var currentcurrency = null;
    var cev = common.eventtrigger.eventcarrier(common.utils.getToken());
    
    function getCurrencyExchanges() {
        helper.getExchangeCurrency(core, function (data) {
            exchanges = data;
            var fcr = _.filter(currencys, function (currency) {
                return currency.value == m_currentcurrency;
            });
            if (fcr != null && fcr.length > 0) {
                currentcurrency = fcr[0];
            }
            cev.emit('exchanges.builded', exchanges);
        });
    }
    
    cev.on('exchanges.builded', function (exchanges) { 
        mongo.findone(options, function (results) {
            if (results.ResultOK == true) {
                if (options.auth != null && options.auth.user != null) {
                    options.auth.user.isAffiliate ? process.nextTick(function () {
                        
                        results.Data = require('../../decorator/priceexchangesync')({
                            document: results.Data, currency: m_currentcurrency, 
                            exchanges: exchanges, currentcurrency: currentcurrency
                        });
                        require('../../decorator/product.affiliate.price')({
                            core: core, document: results.Data, loggeduser: options.auth
                        }, 
                        function (document) { callback(document); }, 
                        function (err) { errorcallback(err); });
                    }) : callback(results.Data);
                } else {
                    callback(results.Data);
                }
            } else {
                errorcallback(results);
            }
        });
    });

    getCurrencyExchanges();
}
