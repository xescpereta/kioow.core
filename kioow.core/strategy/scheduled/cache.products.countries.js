﻿
var _ = require('underscore');
var utils = require('../../tools');

function getDMCsList(core, filter, callback) {
    //cases true-false true-true false-true
    var dmccase = 'all';

    var condition = null;
    if ((filter.b2bchannel != null && filter.b2bchannel == true) &&
        (filter.b2cchannel == null || filter.b2cchannel == false)) {
        dmccase = 'onlyb2b';
    }
    if ((filter.b2cchannel != null && filter.b2cchannel == true) &&
        (filter.b2bchannel == null || filter.b2bchannel == false)) {
        dmccase = 'onlyb2c';
    }
    switch (dmccase) {
        case 'all':
            condition = null;
            break;
        case 'onlyb2b':
            condition =
                    { 'membership.b2bchannel': true }
            break;
        case 'onlyb2c':
            condition =
                    { 'membership.b2cchannel': true }
            break;
    }

    if (condition != null) {
        core.list('DMCs').model.find(condition).select('id').exec(function (err, docs) {
            var ids = [];
            if (err) {
                console.log(err);
            }

            if (docs != null && docs.length > 0) {
                for (var i = 0, len = docs.length; i < len; i++) {
                    ids.push(docs[i].id);
                }
            }
            callback(ids);
        });

    } else {
        callback(null);
    }

}
//Deprecated...
function _OLD_getCitiesForCountry(countrycode, core, query, callback) {
    var depcities = [];
    var sleepcities = [];
    var stopcities = [];
    var totalcities = [];
    var citycount = 3;
    
    core.list('DMCProducts').model.find(query)
    .distinct('itinerary.departurecity.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        depcities = _.filter(docs, function (city) { return city.countrycode == countrycode; });
        depcities = _.pluck(depcities, 'city');
        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })
    .distinct('itinerary.sleepcity.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        sleepcities = _.filter(docs, function (city) { return city.countrycode == countrycode; });
        sleepcities = _.pluck(sleepcities, 'city');
        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })
    .distinct('itinerary.stopcities.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        stopcities = _.filter(docs, function (city) { return city.countrycode == countrycode; });
        stopcities = _.pluck(stopcities, 'city');
        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })

}
//New feature
function getCitiesForCountry(countrycode, core, query, callback) {
    var depcities = [];
    var sleepcities = [];
    var stopcities = [];
    var totalcities = [];
    var slugs = [];
    var citycount = 3;

    core.list('DMCProducts').model.find(query)
    .distinct('itinerary.departurecity', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        //filtering cities...
        depcities = _.filter(docs, function (city) {
            if (city.city != null && city.city != '' && city.location != null) {
                return city.location.countrycode == countrycode;
            } else { return false; }
            
        });
        depcities = _.map(depcities, function (city) {
            var cachecity = {
                city_en: city.city,
                city_es: city.city_es,
                slug: city.slug
            };
            var slug = utils.slug(city.city.toUpperCase() + ' ' + city.location.countrycode.toUpperCase());
            if (slugs.indexOf(slug) < 0) {
                slugs.push(slug);
                return cachecity;
            }
            else { return null; }
        });
        depcities = _.filter(depcities, function (city) {
            return city != null;
        });

        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })
    .distinct('itinerary.sleepcity', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        //filtering cities...
        sleepcities = _.filter(docs, function (city) {
            if (city.city != null && city.city != '' && city.location != null) {
                return city.location.countrycode == countrycode;
            } else { return false; }

        });
        sleepcities = _.map(sleepcities, function (city) {
            var cachecity = {
                city_en: city.city,
                city_es: city.city_es,
                slug: city.slug
            };
            var slug = utils.slug(city.city.toUpperCase() + ' ' + city.location.countrycode.toUpperCase());
            if (slugs.indexOf(slug) < 0) {
                slugs.push(slug);
                return cachecity;
            }
            else { return null; }
        });
        sleepcities = _.filter(sleepcities, function (city) {
            return city != null;
        });

        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })
    .distinct('itinerary.stopcities', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        //filtering cities...
        stopcities = _.filter(docs, function (city) {
            if (city.city != null && city.city != '' && city.location != null) {
                return city.location.countrycode == countrycode;
            } else { return false; }

        });
        stopcities = _.map(stopcities, function (city) {
            var cachecity = {
                city_en: city.city,
                city_es: city.city_es,
                slug: city.slug
            };
            var slug = utils.slug(city.city.toUpperCase() + ' ' + city.location.countrycode.toUpperCase());
            if (slugs.indexOf(slug) < 0) {
                slugs.push(slug);
                return cachecity;
            }
            else { return null; }
        });
        stopcities = _.filter(stopcities, function (city) {
            return city != null;
        });

        citycount--;
        if (citycount == 0) {
            totalcities = _.union(depcities, sleepcities, stopcities);
            callback(totalcities);
        }
    })

}
function getLocationsForCountry(core, query, callback) {
    var cities = [];
    var citycount = 3;
    core.list('DMCProducts').model.find(query)
    .distinct('itinerary.departurecity.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        cities.concat(docs);
        citycount--;
        if (citycount == 0) {
            callback(cities);
        }
    })
    .distinct('itinerary.sleepcity.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        cities.concat(docs);
        citycount--;
        if (citycount == 0) {
            callback(cities);
        }
    })
    .distinct('itinerary.stopcities.location', function (err, docs) {
        if (err != null) {
            console.log(err);
        }
        cities.concat(docs);
        citycount--;
        if (citycount == 0) {
            callback(cities);
        }
    })

}
//city builders
function buildAllCities(core, country, callback) {
    var rs = {
        name: country.label_en,
        name_es: country.label_es,
        countrycode: country.slug.toUpperCase(),
        cities: []
    };
    var query = {
        $and: [{
            publishState: 'published',
        },
        {
            $or: [
                    { 'itinerary.departurecity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                    { 'itinerary.sleepcity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                    { 'itinerary.stopcities.location.countrycode': { $in: [country.slug.toUpperCase()] } }
            ]
        }]
    };
    //console.log('pasamos por aqui...');
    getCitiesForCountry(country.slug.toUpperCase(), core, query, function (cities) {
        //console.log(cities);
        rs.cities = _.filter(cities, function (city) { return (city != null && city != '') });;
        rs.cities.sort();
        callback(rs);
    });
}
function buildB2BCities(core, country, callback) {
    var filt = {
        b2bchannel: true,
        b2cchannel: false
    };
    getDMCsList(core, filt, function (dmcs) {

        var rs = {
            name: country.label_en,
            name_es: country.label_es,
            countrycode: country.slug.toUpperCase(),
            cities: []
        };
        var query = {
            $and: [{
                publishState: 'published',
            },
            {
                $or: [
                        { 'itinerary.departurecity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                        { 'itinerary.sleepcity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                        { 'itinerary.stopcities.location.countrycode': { $in: [country.slug.toUpperCase()] } }
                ]
            }]
        };
        if (dmcs != null && dmcs.length > 0) {
            query.$and.push({ dmc: { $in: dmcs }})
        }

        getCitiesForCountry(country.slug.toUpperCase(), core, query, function (cities) {
            rs.cities = _.filter(cities, function (city) { return (city != null && city != '') });;
            rs.cities.sort();
            callback(rs);
        });
    });
    
}
function buildB2CCities(core, country, callback) {
    var filt = {
        b2bchannel: false,
        b2cchannel: true
    };
    getDMCsList(core, filt, function (dmcs) {

        var rs = {
            name: country.label_en,
            name_es: country.label_es,
            countrycode: country.slug.toUpperCase(),
            cities: []
        };
        var query = {
            $and: [{
                publishState: 'published',
            },
            {
                $or: [
                        { 'itinerary.departurecity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                        { 'itinerary.sleepcity.location.countrycode': { $in: [country.slug.toUpperCase()] } },
                        { 'itinerary.stopcities.location.countrycode': { $in: [country.slug.toUpperCase()] } }
                ]
            }]
        };
        if (dmcs != null && dmcs.length > 0) {
            query.$and.push({ dmc: { $in: dmcs } })
        }

        getCitiesForCountry(country.slug.toUpperCase(), core, query, function (cities) {
            rs.cities = _.filter(cities, function (city) { return (city != null && city != '') });;
            rs.cities.sort();
            callback(rs);
        });
    });

}
//end city builders

var buildCacheForProducts = function (callback) {
    console.log(callback);
	var hash = require('../../tools/hashtable');

	//api de cmsfetch
	var _cmsdb = require('../../core/cmsfetch');
	var cmsfetch = new _cmsdb.CmsFetch();
	//api de core
	var omtcore = require('../../core');

	if (cmsfetch == null) {
	    cmsfetch = new _cmsdb.CmsFetch();
	}

	function checkAll(finalResults, cb) {
	    if (finalResults.totalpending == 0) {
	        var cachetodo = 3;
	        var cCache = require('../../omtcache/cachelocalaccess');
	        var rqAll = {
	            Key: 'ProductCountriesCACHE',
	            Item: finalResults.ProductCountriesCACHE
	        }
	        var rqB2B = {
	            Key: 'ProductCountriesB2BCACHE',
	            Item: finalResults.ProductCountriesB2BCACHE
	        }
	        var rqB2C = {
	            Key: 'ProductCountriesB2CCACHE',
	            Item: finalResults.ProductCountriesB2CCACHE
	        }
	        cCache.pushStore(rqAll, function (rs) {
	            cachetodo--;
	            if (cachetodo == 0) {
	                cb({
	                    ResultOK: true,
	                    Message: 'All caches for Countries and Cities finished and loaded.',
	                    data: {
	                        ProductCountriesCACHE: rqAll.Item.length + ' items loaded',
	                        ProductCountriesB2BCACHE: rqB2B.Item.length + ' items loaded',
	                        ProductCountriesB2CCACHE: rqB2C.Item.length + ' items loaded',
	                    }
	                });
	            }
	        });
	        cCache.pushStore(rqB2B, function (rs) {
	            cachetodo--;
	            if (cachetodo == 0) {
	                cb({
	                    ResultOK: true,
	                    Message: 'All caches for Countries and Cities finished and loaded.',
	                    data: {
	                        ProductCountriesCACHE: rqAll.Item.length + ' items loaded',
	                        ProductCountriesB2BCACHE: rqB2B.Item.length + ' items loaded',
	                        ProductCountriesB2CCACHE: rqB2C.Item.length + ' items loaded',
	                    }
	                });
	            }
	        });
	        cCache.pushStore(rqB2C, function (rs) {
	            cachetodo--;
	            if (cachetodo == 0) {
	                cb({
	                    ResultOK: true,
	                    Message: 'All caches for Countries and Cities finished and loaded.',
	                    data: {
	                        ProductCountriesCACHE: rqAll.Item.length + ' items loaded',
	                        ProductCountriesB2BCACHE: rqB2B.Item.length + ' items loaded',
	                        ProductCountriesB2CCACHE: rqB2C.Item.length + ' items loaded',
	                    }
	                });
	            }
	        });
	    }
	}

	cmsfetch.GetAllCountries(false, function (countries) {

	    if (countries != null && countries.length > 0) {
	        var results = {
	            ProductCountriesCACHE: [],
	            ProductLocationsCACHE: [],
	            ProductCountriesB2BCACHE: [],
	            ProductCountriesB2CCACHE: [],
                totalpending: countries.length
	        };
	        function _check_cities(pendingcities) {
	            if (pendingcities == 0) {
	                results.totalpending--;
	                if (results.totalpending == 0) {
	                    console.log(callback);
	                    checkAll(results, callback)
	                }
	            }
	        }
	        _.each(countries, function (country) {
	            console.log(country.label_es);
	            if (country != null) {
	                var pendingcities = 3;
	                buildAllCities(omtcore, country, function (result) {
	                    if (result.cities != null && result.cities.length > 0) {
	                        results.ProductCountriesCACHE.push(result);
	                    } else {
	                        console.log(result.name + ' has no cities [ProductCountriesCACHE]. Not included');
	                    }
	                    pendingcities--;
	                    _check_cities(pendingcities);
	                });
	                buildB2BCities(omtcore, country, function (result) {
	                    if (result.cities != null && result.cities.length > 0) {
	                        results.ProductCountriesB2BCACHE.push(result);
	                    } else {
	                        console.log(result.name + ' has no cities [ProductCountriesB2BCACHE]. Not included');
	                    }
	                    pendingcities--;
	                    _check_cities(pendingcities);
	                });
	                buildB2CCities(omtcore, country, function (result) {
	                    if (result.cities != null && result.cities.length > 0) {
	                        results.ProductCountriesB2CCACHE.push(result);
	                    } else {
	                        console.log(result.name + ' has no cities [ProductCountriesB2CCACHE]. Not included');
	                    }
	                    pendingcities--;
	                    _check_cities(pendingcities);
	                });
	            }
	            else {
	                results.totalpending--;
	                if (results.totalpending == 0) {
	                    checkAll(results, callback)
	                }
	            }
	        });
	    }
	    else {
	        //Something wrong...
	        callback({
	            ResultOK: false,
	            Message: 'No countries found on CMS...'
	        });
	    }
	});

}

var start = exports.start = buildCacheForProducts;
var empty = exports.empty = function () {
	buildCacheForProducts = null;
}