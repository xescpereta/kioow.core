﻿
function compare(a, b) {
	if (a.sortOrder < b.sortOrder)
		return -1;
	if (a.sortOrder > b.sortOrder)
		return 1;
	return 0;
}

function _arrayUnique(a) {
	return a.reduce(function (p, c) {
		if (p.indexOf(c) < 0) p.push(c);
		return p;
	}, []);
};

function _getMonthNameEnglish(monthindex) {
	if (monthindex == 0) { return 'January'; }
	if (monthindex == 1) { return 'February'; }
	if (monthindex == 2) { return 'March'; }
	if (monthindex == 3) { return 'April'; }
	if (monthindex == 4) { return 'May'; }
	if (monthindex == 5) { return 'June'; }
	if (monthindex == 6) { return 'July'; }
	if (monthindex == 7) { return 'August'; }
	if (monthindex == 8) { return 'September'; }
	if (monthindex == 9) { return 'October'; }
	if (monthindex == 10) { return 'November'; }
	if (monthindex == 11) { return 'December'; }
}
function _indexOfYear(year, availability) {
	var index = -1;
	if (availability != null && availability.length > 0) {
		for (var i = 0; i < availability.length; i++) {
			if (availability[i].year == year) {
				index = i;
				break;
			}
		}
	}
	return index;
}
function _showCities(itinerary) {
	var cities = [];
	var day = "";
	if (itinerary) {

		for (day in itinerary) {

			if (itinerary[day].departurecity != null && itinerary[day].departurecity.city != '') {
				var c = itinerary[day].departurecity.city;
				cities.push(c);
			}

			if (itinerary[day].stopcities != null && itinerary[day].stopcities.length > 0) {
				for (var j = 0; j < itinerary[day].stopcities.length; j++) {
					var cit = itinerary[day].stopcities[j];
					var c = cit.city;

					if (cities.indexOf(c) == -1) {
						cities.push(c);
					}
				}
			}

			if (itinerary[day].sleepcity != null && itinerary[day].sleepcity.city != '') {
				var c = itinerary[day].sleepcity.city;

				if (cities.indexOf(c) == -1) {
					cities.push(c);
				}
			}
		}
		return _arrayUnique(cities);
	}
}
function _cityReducerHome(cities, number) {
	reducedCities = {
		cities: [],
		numberRest: 0
	};
	if (cities != null && cities.length > number) {
		for (i = 0; i <= number; i++) {
			reducedCities.cities.push(cities[i]);
		};
		reducedCities.numberRest = cities.length - number;
		return reducedCities;
	}
	else {
		reducedCities = {
			cities: cities,
			numberRest: 0
		};
		return reducedCities;
	}
}
function _getMarkers(itinerary) {
	var markers = [];
	var cities = [];
	if (itinerary) {
		for (day in itinerary) {
			// debo obtener la ciudad donde se duerme o en el ultimo dia la departure

			if (itinerary[day].sleepcity != null && itinerary[day].sleepcity.city != '') {
				var c = itinerary[day].sleepcity.city;

				if (cities.indexOf(c) == -1) {
					cities.push(c);
					if (itinerary[day].sleepcity.location.latitude) {
						var markerdummy = {
							city: c,
							nights: 1,
							country: itinerary[day].sleepcity.location.country,
							position: {
								lat: itinerary[day].sleepcity.location.latitude,
								lng: itinerary[day].sleepcity.location.longitude
							}
						};
						markers.push(markerdummy);
					}
				} else {
					if (itinerary[day].sleepcity.location.latitude) {
						var position = cities.indexOf(c)
						markers[position].nights = markers[position].nights + 1;
					}
				}
			}
		}
	};
	return markers
}
function _calculatePriceMinimumDates(from, to, availability) {
	var pricemin = {
		value: 0,
		currency: {}
	}

	if (availability == null || availability.length == 0) {
		return pricemin;
	}

	var monthstart = from.getMonth();
	var monthend = to.getMonth();
	var iterate = new Date(from.getFullYear(), from.getMonth(), 1);

	while (iterate <= to) {

		var indexyear = _indexOfYear(iterate.getFullYear(), availability);

		if (indexyear > -1) {
			var month = _getMonthNameEnglish(iterate.getMonth());
			if (availability[indexyear] != null) {
				var avails = availability[indexyear][month].availability;
				if (avails != null && avails.length > 0) {
					for (var jj = 0; jj < avails.length; jj++) {

						if (avails[jj].rooms.double.price > 0) {
							if (pricemin.value == 0) {
								pricemin.value = avails[jj].rooms.double.price;
								pricemin.currency = avails[jj].rooms.currency;
							}
							else {
								if (avails[jj].rooms.double.price < pricemin) {
									pricemin.value = avails[jj].rooms.double.price;
									pricemin.currency = avails[jj].rooms.currency;
								}
							}
						}
					}
				}
			}
		}
		iterate.setMonth(iterate.getMonth() + 1);
	}
	return pricemin;
}
function _saveCache(products, callback) {
    var cCache = require('../../omtcache/cachelocalaccess');
    var rqlocs = {
        Key: 'productshowhome',
        Item: products
    };
    cCache.pushStore(rqlocs, function (rs) {
        callback({
            key: 'PROMOTIONS BUILDING CACHE',
            message: 'Process finished'
        });
    });
}


var buildCachePromotions = exports.buildCachePromotions = function(callback){
	//api de cmsfetch
	var _cmsdb = require('../../core/cmsfetch');
	var cmsfetch = new _cmsdb.CmsFetch();
	var utils = require('../../tools');

	cmsfetch.GetPromotionsCodes(function(promos){
		var productCodes = [];
		var productsHome = [];
		console.log(promos);
		var dateActual = new Date();
		console.log(dateActual);
		if (promos != null && promos.length > 0) {
			for (var i = promos.length - 1; i >= 0; i--) {
			    if (promos[i] != null) {
			        console.log(promos[i].code + ' : ' + new Date(promos[i].finishAt));
					if (promos[i].state == 'published') {
					    if (new Date(promos[i].finishAt) >= dateActual) {
					        productCodes.push(promos[i]);
					        console.log('Product added ' + promos[i]);
					    } else {
					        console.log('Product discard ' + promos[i]);
					    }
					}
				}
			}
			productCodes.sort(compare);

			var codeCount = productCodes.length;

			if (productCodes != null && productCodes.length > 0) {
				for (var i = 0; i < productCodes.length; i++) {
					//Products...
					var _mongodb = require('../../core/dbfetch');
					var dbfetch = new _mongodb.MongoFetch();

					var code = productCodes[i].code;
					var slug = null
					var currency = 'EUR';
					var debug = null;
					console.log('Processing code: ' + code);
					dbfetch.GetProduct(code, slug, currency, debug, function (resp) {
						if (resp != null && resp.publishState == 'published') {
							var dummy = {};
							dummy.markers = _getMarkers(resp.itinerary);
							if (dummy.markers.length > 0) {
								dummy.code = resp.code;
								dummy.slug = resp.slug_es;
								for (var j = productCodes.length - 1; j >= 0; j--) {
									if (resp.code == productCodes[j].code) {
										dummy.zoom = productCodes[j].zoom;
									}
								};

								dummy.title_es = resp.title_es;
								dummy.itinerary = [];
								//
								var tempcities = _showCities(resp.itinerary);
								dummy.cities = _cityReducerHome(tempcities, 2);
								//console.log(dummy.cities);
								dummy.days = resp.itinerary.length;
								if (resp.tags.length > 3) {
									dummy.tags = resp.tags.slice(0, 3);
									dummy.tags.push({ label: '...' });
								} else {
									dummy.tags = resp.tags
								}

								var from = new Date();
								var to = new Date(from.getFullYear() + 1, from.getMonth(), from.getDate());
								//console.log("form :"+from+ " to: "+to );
								dummy.price = _calculatePriceMinimumDates(from, to, resp.availability);
								//console.log(dummy.price);
								dummy.markers = _getMarkers(resp.itinerary);
								//
								var position = 0;
								for (var k = 0; k < productCodes.length; k++) {
									if (productCodes[k].code == dummy.code) {
									    position = k;
									    console.log('CODE: ' + dummy.code + '; position: '+ k);
									}
								};
								productsHome[position] = dummy;
								//

							} else {
								console.log("the product : " + resp.code + " has no markers")
							}
						} else {
						    if (resp != null) {
						        console.log("the product : " + resp.code + " is unpublished")
						    }
							
						}
						
						codeCount--;
						//check finished...
						if (codeCount == 0) {
							var finalProducts = [];
							for (var l = 0; l < productsHome.length; l++) {
								if (productsHome[l] != null) {
									finalProducts.push(productsHome[l]);
								}
							}
							_saveCache(finalProducts, function (result) {
								callback(result);
								dbfetch = null;
								cmsfetch = null;
							});
						}

					});
				}
			} else {
				_saveCache([], function (result) {
					callback(result);
				});
			}
		} else {
			_saveCache([], function (result) {
				callback(result);
			});
		}
	});
}

var start = exports.start = buildCachePromotions;
var empty = exports.empty = function () {
	buildCachePromotions = null;
}