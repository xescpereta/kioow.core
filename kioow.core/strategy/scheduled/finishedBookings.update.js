﻿
//utils
var utils = require('../../tools');
var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
var productQueue = new _q.Queue();


//bd mongo...
var _mongodb = require('../../core/dbfetch');
var dbfetch = new _mongodb.MongoFetch();



var updateFinisedBookings = function (callback) { 

    var start = new Date();
    console.log(' Scheduled task that updates bookings finished (status regularok) to regularend.... ' + start);
    var errs = [];
    bookingQueue = new _q.Queue();

    
    // 1) inicializar la query
    //reservas no canceladas y pagadas 100% (con estado regularok) 
    var query = { $and: [{ cancelDate: null  }, { status: 'regularok' }] }; 
    var querystream = dbfetch.getCore().list('Bookings').model.find(query).stream();
    
    //recorrer la respuesta
    querystream.on('data', function (doc) {
        console.log(' * Processin booking id: ',doc.idBooking);
        
        //comprobar los dias que quedan hasta la fecha de entrada        
        var numDays = daysBetweenDates(doc);
              
        // si la fecha de actual es mayor que la de fin de la reserva(es decir al dia siguiente de la fecha de salida)
        if(numDays < 0){
        	 bookingQueue.enqueue(doc);
        }       	
       
    });
    
    querystream.on('error', function (err) {
        console.log(err);
    });
    
    
    // cerrar flujo de query
    querystream.on('close', function () {
  	
    	//inicializo las variables
        var pending = bookingQueue.getLength();     
        var totals = bookingQueue.getLength();
        var updated = 0;
        
        
        var interval = setInterval(function () {
            var booking = bookingQueue.dequeue();
            if (booking != null && booking != '') { 
            	
            	//actualizo el estado de la reserva a finalizado
            	booking.status="regularend";
            	
            	//modifico la fecha de actualizacion
            	booking.lastModifiedDate=new Date();
            	
            	//guardo el historico del cambio
            	var  dummyHistoric = {	  
						"date": new Date(),
						"state" : booking.status,
						"user" :  "CRON-finishedBookings.update" //nombre del cron que cambia el estado						
	           	};	   
            	if(booking.historic == null){
	           		booking.historic=[];
	           	}
	           	booking.historic.push(dummyHistoric);
            	
	           	//guardar la reserva
            	booking.save(function (err, doc) {                   
                  	 if (doc) {	
                  		  console.log('Booking: '+booking.idBooking+' saved as regularend correctly');                                 
                  		  updated++;
                       } else {
                       	throw 'An error has ocurred while saving booking!';
                       }
                   });
                pending--;               
            }
           
            if (pending == 0) { 
                clearInterval(interval);
                callback({
                    Totals: totals,
                    UpdatedCount: updated,
                    Started: start,
                    Finished: new Date(),
                    Errors: errs
                });
            }
        }, 5000);
    });

}


/**
 * funcion qeu devuelve el numero de dias entre la fecha de salida y la actual
 */
var daysBetweenDates = function (booking) { 

    var actual = new Date();
    var endBooking = new Date(booking.end.year, booking.end.month, booking.end.day); 
    var nunDays = (endBooking-actual)/(1000*60*60*24);      
    nunDays = Math.floor( nunDays);    
    return nunDays;
}


var start = exports.start = updateFinisedBookings;