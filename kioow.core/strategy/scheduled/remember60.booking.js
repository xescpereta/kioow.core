﻿
//utils
var utils = require('../../tools');
var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
var productQueue = new _q.Queue();

//mailer & filepath
var _mailer = require('../../mailing/server');
var filepaths = require('../../routes/filepaths').filepaths;

//bd mongo...
var _mongodb = require('../../core/dbfetch');
var dbfetch = new _mongodb.MongoFetch();



var rememberBooking60 = function (callback) { 

    var start = new Date();
    console.log(' Scheduled task that sends mail traveler to remember to pay the rest of booking.... ' + start);
    var errs = [];
    bookingQueue = new _q.Queue();

    
    // 1) inicializar la query
    //reservas no canceladas y con estado regular1-2, han pagado la primera parte
    var query = { $and: [{ cancelDate: null  }, { status: 'regular1-2' }] };
 
    var querystream = dbfetch.getCore().list('Bookings').model.find(query)
    .populate('traveler')        
    .stream();
    
    //recorrer la respuesta
    querystream.on('data', function (doc) {
        console.log(' * Processin booking id: ',doc.idBooking);
        
        //comprobar los dias que quedan hasta la fecha de entrada        
        var numDays = daysBetweenDates(doc);
        console.log("numero de días: ",numDays);
        
        // si esta entre 33, 32 y 31 dia antes de la fecha de entrada, se mandara un mensaje
        if(numDays < 34 && numDays >30){
        	 bookingQueue.enqueue(doc);
        }       	
       
    });
    
    querystream.on('error', function (err) {
        console.log(err);
    });
    
    
    // cerrar flujo de query
    querystream.on('close', function () {
    	
    	//inicializo el mailer
    	var _m = require('../../mailing/server');
    	var connector = new _m.MailServerConnector();    	   	
    	
    	//inicializo las variables
        var pending = bookingQueue.getLength();     
        var totals = bookingQueue.getLength();
        var updated = 0;
        
        
        var interval = setInterval(function () {
            var booking = bookingQueue.dequeue();
            if (booking != null && booking != '') { 
            	
            	// variables para los correos
            	var subject= null;
            	var template ='userremember60';
            	var product = JSON.parse(booking.product);
            	
            	var numDays = daysBetweenDates(booking);
            	if(numDays == 33){
            		subject = '[OMT] Recuerda que te quedan 3 días pagar el resto de la reserva: '+booking.idBooking;
            	}
            	else if(numDays == 32){
            		subject = '[OMT] Recuerda que te quedan 2 días pagar el resto de la reserva: '+booking.idBooking;
            	}
            	else{
            		subject = '[OMT] Recuerda que te quedan 1 día pagar el resto de la reserva: '+booking.idBooking;
            	}
            	
            	var parameterSwig = {
	   	 	            booking : booking,
	   	 	            product : product,
	   	 	            countries : utils._showCountries(product.itinerary),
	   	 	            tags : utils._showTagsArray(product.tags),
	   	 	            hotelcats: utils._showHotelCats(product.itinerary,'_es'),
	   	 	            lang: '_es',
	   	 	            distribution: utils._showDistribution(booking.roomDistribution),
	   	 	            urlBooking: filepaths.rooturl+"/client-booking?idbooking="+booking.idBooking,
	   	 	        	urlRestoPago: filepaths.rooturl+"/booking-pos?bookingId="+booking.idBooking,
	   	 	            mainImageProduct: utils.cloudinary_url(product.productimage.url, 'mainproductimage')
			   	 }; 
             
                
                //mandar mail con swig y contenido correcto
                console.log('mail traveler: ',booking.traveler.email);
                omtcore.Mail.SetupNotifyEmail('reservas@openmarket.travel', booking.traveler.email,
                    subject, template, parameterSwig, function (omtmail){
                    
                        connector.PushMail(omtmail, function () {
                            //the last mail
                            console.log('Email from OMT to '+booking.traveler.email+' pushed...');                                 
                            updated++;
                        });
                }); 
                
                pending--;               
            }
           
            if (pending == 0) { 
                clearInterval(interval);
                callback({
                    Totals: totals,
                    UpdatedCount: updated,
                    Started: start,
                    Finished: new Date(),
                    Errors: errs
                });
            }
        }, 5000);
    });

}


/**
 * funcion qeu devuelve el numero de dias entre la fecha de entrada y la actual
 */
var daysBetweenDates = function (booking) { 
	  //comprobar los dias que quedan hasta la fecha de entrada
    var actual = new Date();
    var startBooking = new Date(booking.start.year, booking.start.month, booking.start.day); 
    var nunDays = (startBooking-actual)/(1000*60*60*24);
//    console.log("fecha entrada: "+booking.start.year+" "+ booking.start.month+" "+ booking.start.day);
//    console.log("fecha entrada en Date: "+startBooking);
//    console.log("fecha actual "+actual);
//    console.log("numero de días: ",nunDays);        
    nunDays = Math.floor( nunDays);
  //  console.log("numero de días: ",nunDays);
    return nunDays;
}


var start = exports.start = rememberBooking60;