﻿
var utils = require('../../tools');
var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
var _fs = require('../../tools/filemgr');

var logourl = 'http://res.cloudinary.com/open-market-travel/image/upload/cs_no_cmyk/w_72,h_72,c_fill,g_face,q_50/';
var selfie = 'http://res.cloudinary.com/open-market-travel/image/upload/cs_no_cmyk/c_fill,h_140,w_250,g_faces,q_70/';

var allresults = [];
var cldimages = {
    productimages: [],
    adminimages: [],
    travelerimages: [],
    dmcsimages: []
};
var idexceptionlist = ['andrescuervo','lolacaballero','joseluisgarcia','davidfuentes'];
var containingexceptions = ['cms/', 'assets/', 'mail/'];

function download(url, imagefile, callback) {
    var fs = require('fs'),
        request = require('request');
    
    var download = function (uri, file, callback) {
        request.head(uri, function (err, res, body) {
            console.log('content-type:', res.headers['content-type']);
            console.log('content-length:', res.headers['content-length']);
            
            request(uri).pipe(fs.createWriteStream(file)).on('close', callback);
        });
    };
    
    download(url, 
    imagefile, function () {
        callback({
            imageurl : url,
            image: imagefile,
            done: true
        });
    });
}

function getCodes(dbfetch, finishHandler) {
    dbfetch.GetAllDMCCodes(function (codes) { 
        finishHandler(codes);
    });
}

function isContainingException(key) {
    var isexc = false;
    for (var i = 0, len = containingexceptions.length; i < len; i++) {
        if (key.indexOf(containingexceptions[i]) > -1) {
            isexc = true;
            break;
        }
    }
    if (isexc == false) { 
        isexc = idexceptionlist.indexOf(key) > -1;
    }
    return isexc;
}
function fetchCloudinaryResources(cloudinary, nextcursor, finishhandler) {
    
    var filt = {
        max_results : 100
    };
    console.log('requesting... ' + nextcursor);
    if (nextcursor != null && nextcursor != '') {
        filt = {
            max_results : 100,
            next_cursor: nextcursor
        };
    }
    cloudinary.api.resources(function (result) {
        //console.log(result.resources[0]);
        //console.log(result.resources.length);
        //console.log(result.next_cursor);
        finishhandler(result);
    }, filt);
}

function fetchAllCloudinaryResources(finishhandler) { 
    
    var nconf = require('nconf');
    
    nconf.env().file({ file: 'C:/node/kioow/kioow.core/settings.json' });
    
    var cloudinary = require('cloudinary');
    var config = nconf.get('cloudinaryconfig');
    cloudinary.config(config);
    
    
    var continuefetching = true;
    var nextcursor = '';

    var interval = setInterval(function () {
        if (continuefetching) {
            fetchCloudinaryResources(cloudinary, nextcursor, function (results) {
                if (results != null && results.resources != null && results.resources.length > 0) {
                    console.log('Fetching results...');
                    allresults = _.union(allresults, results.resources);
                    continuefetching = results.next_cursor != null && results.next_cursor != '';
                    nextcursor = results.next_cursor;
                    console.log('Elements: ' + allresults.length);
                    console.log('next: '+ nextcursor);
                }
            });
        }
        else {
            clearInterval(interval);
            finishhandler(allresults);
        }
    }, 2000);
    
    
}

function getAllImagesFromBBDD(core, finishhandler) {
    //images admin
    var taskstodo = 5;
    //core.list('OMTAdmin').model.find()
    //.distinct('images.photo', function (err, omtadminimages) {
    //    cldimages.adminimages = _.union(cldimages.adminimages, omtadminimages);
    //    taskstodo--;
    //    if (taskstodo == 0) { 
    //        finishhandler(cldimages);
    //    }
    //})
    //.distinct('images.logo', function (err, omtadminimages) {
    //    cldimages.adminimages = _.union(cldimages.adminimages, omtadminimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //})
    //.distinct('images.splash', function (err, omtadminimages) {
    //    cldimages.adminimages = _.union(cldimages.adminimages, omtadminimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //});
    //images dmc
    core.list('DMCs').model.find()
    .distinct('tourEscorts.image', function (err, dmcimages) {
        cldimages.dmcsimages = _.union(cldimages.dmcsimages, dmcimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('additionalinfo.associations.image', function (err, dmcimages) {
        cldimages.dmcsimages = _.union(cldimages.dmcsimages, dmcimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
    //.distinct('images.photo', function (err, dmcimages) {
    //    cldimages.dmcsimages = _.union(cldimages.dmcsimages, dmcimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //})
    //.distinct('images.logo', function (err, dmcimages) {
    //    cldimages.dmcsimages = _.union(cldimages.dmcsimages, dmcimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //})
    //.distinct('images.splash', function (err, dmcimages) {
    //    cldimages.dmcsimages = _.union(cldimages.dmcsimages, dmcimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //});
    //images traveler
    core.list('Travelers').model.find()
    .distinct('images.photo', function (err, travelerimages) {
        cldimages.travelerimages = _.union(cldimages.travelerimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.logo', function (err, travelerimages) {
        cldimages.travelerimages = _.union(cldimages.travelerimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.splash', function (err, travelerimages) {
        cldimages.travelerimages = _.union(cldimages.travelerimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
    ////images dmcproducts
    //core.list('DMCProducts').model.find()
    //.distinct('productimage', function (err, productimages) {
    //    cldimages.productimages = _.union(cldimages.travelerimages, productimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //})
    //.distinct('itinerary.image', function (err, productimages) {
    //    cldimages.productimages = _.union(cldimages.travelerimages, productimages);
    //    taskstodo--;
    //    if (taskstodo == 0) {
    //        finishhandler(cldimages);
    //    }
    //});
}

function buildDictionaries(cloudinaryresources, bbdimages, buildfinishhandler) { 
    //dictionaries...
    var resource_dictionary = new hash.HashTable();
    var image_dictionary = new hash.HashTable();
    var _asnc_tasks_ctrl = {
        resources_done : false,
        resources_count: cloudinaryresources.length,
        images_done: false,
        images_count: bbdimages.length
    };
    function _checkFinished() {
        _asnc_tasks_ctrl.resources_done = _asnc_tasks_ctrl.resources_count == 0;
        _asnc_tasks_ctrl.images_done = _asnc_tasks_ctrl.images_count == 0;
        var done = _asnc_tasks_ctrl.resources_done && _asnc_tasks_ctrl.images_done;
        if (done) { 
            buildfinishhandler(resource_dictionary, image_dictionary);
        }
    }
    _.each(cloudinaryresources, function (resource) {
        resource_dictionary.set(resource.public_id, resource);
        _asnc_tasks_ctrl.resources_count--;
        _checkFinished();
    });
    _.each(bbdimages, function (image) {
        if (image.public_id != null && image.public_id != '') {
            image_dictionary.set(image.public_id, image);
        }
        _asnc_tasks_ctrl.images_count--;
        _checkFinished();
    });

}

function checkImages(clouddictionary, imagesdictionary, checkfinishhandler) {
    var totals = {
        TotalCloudinaryImages: clouddictionary.length,
        TotalBBDDImages: imagesdictionary.length,
        CloudinaryAssociated: 0,
        CloudinaryNotAssociated: 0, 
        CloudinaryNotAllowed: 0,
        RemovableItems: []
    }
    var count = clouddictionary.length;
    var notassociated = '';
    var notallowed = '';
    _.each(clouddictionary.keys(), function (key) {
        var item = imagesdictionary.get(key);
        if (item != null) {
            totals.CloudinaryAssociated++;
        }
        else {
            if (!isContainingException(key)) {
                totals.CloudinaryNotAssociated++;
                notassociated += key + '\r\n';
                totals.RemovableItems.push(key);
            }
            else {
                totals.CloudinaryNotAllowed++;
                notallowed += key + '\r\n';
            }
        }
        count--;
        if (count == 0) {
            var fwrite = new _fs.filemgr();
            fwrite.writeText('c:/temp/clnotassoc.txt', 
                '[NOT ASSOCIATED]\r\n' + 
                notassociated + 
                '\r\n\r\n[NOT ALLOWED]\r\n' +
                notallowed
            );
            checkfinishhandler(totals);
        }
    });

}

function removeCloudinaryImages(cloudids, removefinishhandler) {
    
    var nconf = require('nconf');
    
    nconf.env().file({ file: 'C:/development/node/yourttoo.travel/yourttoo.travel.core/settings.json' });
    
    var cloudinary = require('cloudinary');
    var config = nconf.get('cloudinaryconfig');
    cloudinary.config(config);

    var cloudqueue = new _q.Queue();
    var deletions = [];
    if (cloudids != null && cloudids.length > 0) {
        var maxitems = 10;
        var currentitems = 0;
        var currentArray = [];
        for (var i = 0, len = cloudids.length; i < len; i++) {
            if (currentArray.length == maxitems) {
                var ids = _.shuffle(currentArray);
                cloudqueue.enqueue(ids);
                currentArray = [];
                currentArray.push(cloudids[i]);
            }
            else {
                currentArray.push(cloudids[i]);
            }
        }

        var interval = setInterval(function () {
            if (cloudqueue.getLength() > 0) {
                var idstodelete = cloudqueue.dequeue();
                
                cloudinary.api.delete_resources(idstodelete, function (results) {
                    console.log('Cloudinary IDs Deleted');
                    console.log(results);
                    deletions.push(results);
                });
            } else { 
                //finished
                removefinishhandler({
                    Removed: True,
                    Deletions: deletions
                });
            }
        }, 5000);
    } else {
        removefinishhandler({
            DeletedItems : 0
        });
    }
}

function donwloadall(imagelist, cb) {
    var path = require('path');
    var url = require("url");

    var ct = imagelist.length;
    _.each(imagelist, function (imageurl) {
        //console.log(imageurl);
        if (imageurl != null && imageurl.url != null) {
            var urlpath = url.parse(imageurl.url).pathname;
            var filename = path.basename(urlpath);
            console.log(filename);

            var downlogourl = logourl + filename;
            var downselfieurl = selfie + filename;
            console.log('downloading ' + downlogourl);
            download(downselfieurl, 'c:/temp/' + filename, function (result) {
                console.log(result);
                console.log('downloaded : ' + filename);
                ct--;
                if (ct == 0) {
                    cb({ AllDone: true });
                }
            });
        } else {
            console.log('null reference');
            console.log(imageurl);
            ct--;
            if (ct == 0) {
                cb({ AllDone: true });
            }
        }
        
    });
}

function printresults(dmcs, finishHandler) {
    console.log('Lets build html ...' + dmcs.length);
    var html = '<table>%content%</table>';
    var table = '';
    for (var i = 0, len = dmcs.length; i < len; i++) {
        var dmc = dmcs[i];
        if (dmc != null) {
            var row = '<tr><td>%code%</td><td>%name%</td><td>%legalname%</td><td>%companyname%</td><td>%creation%</td>'+ 
            '<td>%logo%</td><td>%photo%</td><td>%logotr%</td><td>%phototr%</td></tr>';
            row = row.replace('%code%', dmc.code);
            row = row.replace('%name%', dmc.name);
            row = row.replace('%legalname%', dmc.legalname);
            row = row.replace('%companyname%', dmc.companyname);
            row = row.replace('%creation%', dmc.createdDate);
            row = row.replace('%logo%', dmc.images.logo);
            row = row.replace('%photo%', dmc.images.photo);
            row = row.replace('%logotr%', dmc.images.logotransform);
            row = row.replace('%phototr%', dmc.images.phototransform);
            table += '\r\n' + row;
        }
        html = html.replace('%content%', table);
        
        
        var fwrite = new _fs.filemgr();
        fwrite.writeText('c:/temp/dmclost.txt', 
                html
        );


        
    }

    console.log('HEMOS TERMINADO');
    finishHandler({ yujuu: true, html: html });
}

function getDMCSInfo(core, finishHandler) {
    
    var dmcs = [];
    var html = '<html><body><table style=\"font-size:10px;\">' + 
        '<tr><td><b>code</b></td><td><b>name</b></td><td><b>legalname</b></td><td><b>creation</b></td>' + 
                        '<td><b>logo</b></td><td><b>photo</b></td><td><b>logotr</b></td><td><b>phototr</b></td></tr>' + 
    '%content%</table></body></html>';
    var table = '';
    var path = require('path');
    var url = require("url");
    
    
    var listofids = [];
    var listofurls = [];

    core.list('DMCs').model.find()
    .exec(function (err, docs) {
        if (err) {
            console.log(err);
        }
        if (docs != null && docs.length > 0) {
            for (var i = 0, len = docs.length; i < len; i++) {
                var doc = docs[i];
                var DMC = {
                    code: doc.code,
                    images: {
                        logo: '',
                        logotransform: '',
                        photo: '', 
                        phototransform: '',
                    }
                };
                if (doc != null) {
                    DMC.name = doc.name;

                    if (doc.company != null) {
                        DMC.legalname = doc.company.legalname;
                        DMC.companyname = doc.company.name;
                    }
                    if (doc.images != null) {
                        DMC.images.logo = doc.images.logo.url;
                        DMC.images.photo = doc.images.photo.url;
                        if (listofurls.indexOf(doc.images.photo.url) < 0) {
                            listofurls.push(doc.images.photo.url);
                        }
                        if (listofurls.indexOf(doc.images.logo.url) < 0) {
                            listofurls.push(doc.images.logo.url);
                        }
                        //transform selfie
                        var urlpath = url.parse(doc.images.photo.url).pathname;
                        var filename = path.basename(urlpath);
                        var downselfieurl = selfie + filename;
                        DMC.images.phototransform = downselfieurl;
                        var idselfie = filename.split('.')[0];
                        if (listofids.indexOf(idselfie) < 0) { 
                            listofids.push(idselfie);
                        }
                        //transform logo
                        var urlpath = url.parse(doc.images.logo.url).pathname;
                        var filename = path.basename(urlpath);
                        var downlogourl = logourl + filename;
                        DMC.images.logotransform = downlogourl;
                        var idlogo = filename.split('.')[0];
                        if (listofids.indexOf(idlogo) < 0) {
                            listofids.push(idlogo);
                        }
                    }
                    DMC.createdDate = doc.createdOn;
                    dmcs.push(DMC);

                    var row = '<tr><td>%code%</td><td>%name%</td><td>%legalname%</td><td>%creation%</td>' + 
                        '<td><a href=\"%logo%\">%logo%</a></td><td><a href=\"%photo%\">%photo%</a></td>' + 
                        '<td><a href=\"%logotr%\">%logotr%</a></td><td><a href=\"%phototr%\">%phototr%</a></td></tr>';
                    row = row.replace('%code%', DMC.code);
                    row = row.replace('%name%', DMC.name);
                    row = row.replace('%legalname%', DMC.legalname);
                    row = row.replace('%creation%', DMC.createdDate.getDate() + '/' + (DMC.createdDate.getMonth() + 1) + '/' + DMC.createdDate.getFullYear());
                    row = row.replace('%logo%', DMC.images.logo);
                    row = row.replace('%photo%', DMC.images.photo);
                    row = row.replace('%logotr%', DMC.images.logotransform);
                    row = row.replace('%phototr%', DMC.images.phototransform);
                    row = row.replace('%logo%', DMC.images.logo);
                    row = row.replace('%photo%', DMC.images.photo);
                    row = row.replace('%logotr%', DMC.images.logotransform);
                    row = row.replace('%phototr%', DMC.images.phototransform);
                    table += '\r\n' + row;

                }

            }
            html = html.replace('%content%', table);
            
            
            var fwrite = new _fs.filemgr();
            fwrite.writeText('c:/openmarket/logs/dmclost.html', 
                html
            );
            
            fwrite.writeText('c:/openmarket/logs/dmcidslost.txt', 
                listofids.join('\r\n')
            );

            fwrite.writeText('c:/openmarket/logs/dmcurllost.txt', 
                listofurls.join('\r\n')
            );

            finishHandler({ Finished: true, HTML : html });
        }
    });
}

function buildIdList(imagelist,finishhandler) {
    var ids = [];
    var urls = [];
    var path = require('path');
    var url = require("url");
    
    var txtids = '';
    var txturls = '';

    var count = imagelist.length;
    _.each(imagelist, function (image) { 

        if (image != null) {
            if (image.url != null && image.url != '') {
                if (urls.indexOf(image.url) < 0) {
                    urls.push(image.url);
                    var urlpath = url.parse(image.url).pathname;
                    var filename = path.basename(urlpath);
                    var idimage = filename.split('.')[0];
                    if (ids.indexOf(idimage) < 0) { 
                        ids.push(idimage);
                    }
                }
            }
        }
        count--;
        if (count == 0) {
            
            //write files...
            var fwrite = new _fs.filemgr();
            fwrite.writeText('c:/openmarket/logs/idslost.txt', 
                ids.join('\r\n')
            );
            
            fwrite.writeText('c:/openmarket/logs/urlslost.txt', 
                urls.join('\r\n')
            );

            finishhandler({
                ids: ids,
                urls: urls
            });
        }
    });
}

var updateCloudinary = function (callback) {
    
    var start = new Date();
    console.log('Updating cloudinary scheduled task at ... ' + start);
    var _mongodb = require('../../core/dbfetch');
    var dbfetch = new _mongodb.MongoFetch();
    var core = dbfetch.getCore();
    
    var fetchCtrl = {
        cloudinaryfetch: true,
        cloudinaryresults: null,
        bbddfecth : false,
        bbddresults: null
    }
    
    getAllImagesFromBBDD(core, function (results) {
        var allimages = _.union(results.travelerimages,results.dmcsimages);
        buildIdList(allimages, function (totals) { 
            callback(totals);
        });
    });

    //getDMCSInfo(core, callback);
    //fetchAllCloudinaryResources(function (results) {
    //    console.log('Cloudinary resources fetch done...');
    //    console.log(results.length);
    //    fetchCtrl.cloudinaryfetch = true;
    //    fetchCtrl.cloudinaryresults = results;
    //    if (fetchCtrl.bbddfecth && fetchCtrl.cloudinaryfetch) {
    //        buildDictionaries(fetchCtrl.cloudinaryresults, fetchCtrl.bbddresults, function (clouddict, bbdddict) {
    //            checkImages(clouddict, bbdddict, function (checkresults) {
    //                //removeCloudinaryImages(checkresults.RemovableItems, function () { 
    //                    callback(fetchCtrl);
    //                //});
    //            });
    //        });
    //    }
    //});

    //getAllImagesFromBBDD(core, function (results) {
    //    console.log('BBDD images fetch done...');
    //    //console.log(results.length);
    //    fetchCtrl.bbddfecth = true;
    //    fetchCtrl.bbddresults = results;
    //    if (fetchCtrl.bbddfecth && fetchCtrl.cloudinaryfetch) { 
    //        //buildDictionaries(fetchCtrl.cloudinaryresults, fetchCtrl.bbddresults, function (clouddict, bbdddict) {
    //        //    checkImages(clouddict, bbdddict, function () { 
    //        //removeCloudinaryImages(checkresults.RemovableItems, function () {
    //        console.log('vamos a descargar...');
    //        donwloadall(fetchCtrl.bbddresults.dmcsimages, function (dnresult) { 
    //            //console.log(fetchCtrl.bbddresults);
    //            callback(dnresult);
    //        });
                        
    //                //});
    //        //    });
    //        //});
    //    }
    //});

}
var start = exports.start = updateCloudinary;