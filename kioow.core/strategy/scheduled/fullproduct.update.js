﻿var hermesconnector = null;

var utils = require('../../tools');
var _cmsdb = require('../../core/cmsfetch');
var cmsfetch = new _cmsdb.CmsFetch();


var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
var productQueue = new _q.Queue();

//Products...
var _mongodb = require('../../core/dbfetch');
var dbfetch = new _mongodb.MongoFetch();

function initHermesConnection(connectedHandler) {
    var connected = false;
    try {
        hermesconnector = require('../../socketsserver/hermesconnector');
        hermesconnector.connect();
        Hermes = omtcore.hermesconnector;
        connected = true;
        if (connectedHandler != null) {
            connectedHandler(connected);
        }
    }
    catch (err) {
        console.log(err);
        if (connectedHandler != null) {
            connectedHandler(connected);
        }
    }
}

var updateProducts = function (callback) { 
    //intialization ok!
    var start = new Date();
    console.log('Updating products scheduled task at ... ' + start);
    var errs = [];
    productQueue = new _q.Queue();
    var query = null;
    query = { code: { $ne: null } };
    initHermesConnection(function (connected) {
        if (connected) { 
            //let's take all the codes and process everyone...
            var querystream = dbfetch.getCore().list('DMCProducts').model.find(query, { code : 1 })
            .stream();
            
            querystream.on('data', function (doc) {
                console.log(doc.code);
                productQueue.enqueue(doc.code);
            });
            querystream.on('error', function (err) {
                console.log(err);
            });
            querystream.on('close', function () {
                var pending = productQueue.getLength();
                var totals = productQueue.getLength();
                var updated = 0;
                var interval = setInterval(function () {
                    var code = productQueue.dequeue();
                    if (code != null && code != '') { 
                        hermesconnector.trigger('dmcproduct.saved', {
                            code: [code],
                        });
                        updated++;
                    }
                    pending--;
                    if (pending == 0) { 
                        clearInterval(interval);
                        callback({
                            Totals: totals,
                            UpdatedCount: updated,
                            Started: start,
                            Finished: new Date(),
                            Errors: errs
                        });
                    }
                }, 5000);
            });
        } else {
            callback({
                ResultOK: false,
                Message: 'Hermes connection not available!'
            });
        }
    });

}

var start = exports.start = updateProducts;