﻿var generatePopUpContinents = function (callback) {
    var utils = require('../../tools');
    var _ = require('underscore');
    var start = new Date();
    
    console.log('Landing contents scheduled task at ... ' + start);
    
    var _mongodb = require('../../core/dbfetch');
    var dbfetch = new _mongodb.MongoFetch();
    var _cmsdb = require('../../core/cmsfetch');
    var cmsfetch = new _cmsdb.CmsFetch();
    
    
    cmsfetch.GetCountryZonesAndCountries(null, function (zones) {
        var cachezones = [];
        var total = zones.length;
        _.each(zones, function (zone) {
            var zt = zone;
            zt.countries = _.map(zt.countries, function (country) {
                var ct = {
                    slug: country.slug,
                    label_es: country.label_es,
                    label_en: country.label_en,
                    title_es: country.title_es,
                    title_en: country.title_en
                };
                return ct;
            });
            zt.countries = _.sortBy(zt.countries, 'label_es');
            cachezones.push(zt);
            total--;
            if (total == 0) {
                //sorting...
                cachezones = _.sortBy(cachezones, function (item) { return item.zone.sortOrder; });
                var cCache = require('../../omtcache/cachelocalaccess');
                var rqzones = {
                    Key: 'continentAndCountriesListCACHE',
                    Item: cachezones
                };
                cCache.pushStore(rqzones, function (rs) {
                    callback({
                        ResultOK: true,
                        Message: 'PopUP zones and content Cache loaded and finished',
                    });
                    
                });
            }
        });
    });

    
}
var start = exports.start = generatePopUpContinents;