﻿
var utils = require('../../tools');
var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
var _fs = require('../../tools/filemgr');

var allresults = [];
var cldimages = [];
var idexceptionlist = ['andrescuervo','lolacaballero','joseluisgarcia','davidfuentes'];
var containingexceptions = ['cms/', 'assets/', 'mail/'];

function isContainingException(key) {
    var isexc = false;
    for (var i = 0, len = containingexceptions.length; i < len; i++) {
        if (key.indexOf(containingexceptions[i]) > -1) {
            isexc = true;
            break;
        }
    }
    if (isexc == false) { 
        isexc = idexceptionlist.indexOf(key) > -1;
    }
    return isexc;
}
function fetchCloudinaryResources(cloudinary, nextcursor, finishhandler) {
    
    var filt = {
        max_results : 100
    };
    console.log('requesting... ' + nextcursor);
    if (nextcursor != null && nextcursor != '') {
        filt = {
            max_results : 100,
            next_cursor: nextcursor
        };
    }
    cloudinary.api.resources(function (result) {
        //console.log(result.resources[0]);
        //console.log(result.resources.length);
        //console.log(result.next_cursor);
        finishhandler(result);
    }, filt);
}

function fetchAllCloudinaryResources(finishhandler) { 
    
    var nconf = require('nconf');
    
    nconf.env().file({ file: 'C:/development/node/yourttoo.travel/yourttoo.travel.core/settings.json' });
    
    var cloudinary = require('cloudinary');
    var config = nconf.get('cloudinaryconfig');
    cloudinary.config(config);
    
    
    var continuefetching = true;
    var nextcursor = '';

    var interval = setInterval(function () {
        if (continuefetching) {
            fetchCloudinaryResources(cloudinary, nextcursor, function (results) {
                if (results != null && results.resources != null && results.resources.length > 0) {
                    console.log('Fetching results...');
                    allresults = _.union(allresults, results.resources);
                    continuefetching = results.next_cursor != null && results.next_cursor != '';
                    nextcursor = results.next_cursor;
                    console.log('Elements: ' + allresults.length);
                    console.log('next: '+ nextcursor);
                }
            });
        }
        else {
            clearInterval(interval);
            finishhandler(allresults);
        }
    }, 2000);
    
    
}

function getAllImagesFromBBDD(core, finishhandler) {
    //images admin
    var taskstodo = 11;
    core.list('OMTAdmin').model.find()
    .distinct('images.photo', function (err, omtadminimages) {
        cldimages = _.union(cldimages, omtadminimages);
        taskstodo--;
        if (taskstodo == 0) { 
            finishhandler(cldimages);
        }
    })
    .distinct('images.logo', function (err, omtadminimages) {
        cldimages = _.union(cldimages, omtadminimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.splash', function (err, omtadminimages) {
        cldimages = _.union(cldimages, omtadminimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
    //images dmc
    core.list('DMCs').model.find()
    .distinct('images.photo', function (err, dmcimages) {
        cldimages = _.union(cldimages, dmcimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.logo', function (err, dmcimages) {
        cldimages = _.union(cldimages, dmcimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.splash', function (err, dmcimages) {
        cldimages = _.union(cldimages, dmcimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
    //images traveler
    core.list('Travelers').model.find()
    .distinct('images.photo', function (err, travelerimages) {
        cldimages = _.union(cldimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.logo', function (err, travelerimages) {
        cldimages = _.union(cldimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('images.splash', function (err, travelerimages) {
        cldimages = _.union(cldimages, travelerimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
    //images dmcproducts
    core.list('DMCProducts').model.find()
    .distinct('productimage', function (err, productimages) {
        cldimages = _.union(cldimages, productimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    })
    .distinct('itinerary.image', function (err, productimages) {
        cldimages = _.union(cldimages, productimages);
        taskstodo--;
        if (taskstodo == 0) {
            finishhandler(cldimages);
        }
    });
}

function buildDictionaries(cloudinaryresources, bbdimages, buildfinishhandler) { 
    //dictionaries...
    var resource_dictionary = new hash.HashTable();
    var image_dictionary = new hash.HashTable();
    var _asnc_tasks_ctrl = {
        resources_done : false,
        resources_count: cloudinaryresources.length,
        images_done: false,
        images_count: bbdimages.length
    };
    function _checkFinished() {
        _asnc_tasks_ctrl.resources_done = _asnc_tasks_ctrl.resources_count == 0;
        _asnc_tasks_ctrl.images_done = _asnc_tasks_ctrl.images_count == 0;
        var done = _asnc_tasks_ctrl.resources_done && _asnc_tasks_ctrl.images_done;
        if (done) { 
            buildfinishhandler(resource_dictionary, image_dictionary);
        }
    }
    _.each(cloudinaryresources, function (resource) {
        resource_dictionary.set(resource.public_id, resource);
        _asnc_tasks_ctrl.resources_count--;
        _checkFinished();
    });
    _.each(bbdimages, function (image) {
        if (image.public_id != null && image.public_id != '') {
            image_dictionary.set(image.public_id, image);
        }
        _asnc_tasks_ctrl.images_count--;
        _checkFinished();
    });

}

function checkImages(clouddictionary, imagesdictionary, checkfinishhandler) {
    var totals = {
        TotalCloudinaryImages: clouddictionary.length,
        TotalBBDDImages: imagesdictionary.length,
        CloudinaryAssociated: 0,
        CloudinaryNotAssociated: 0, 
        CloudinaryNotAllowed: 0,
        RemovableItems: []
    }
    var count = clouddictionary.length;
    var notassociated = '';
    var notallowed = '';
    _.each(clouddictionary.keys(), function (key) {
        var item = imagesdictionary.get(key);
        if (item != null) {
            totals.CloudinaryAssociated++;
        }
        else {
            if (!isContainingException(key)) {
                totals.CloudinaryNotAssociated++;
                notassociated += key + '\r\n';
                totals.RemovableItems.push(key);
            }
            else {
                totals.CloudinaryNotAllowed++;
                notallowed += key + '\r\n';
            }
        }
        count--;
        if (count == 0) {
            var fwrite = new _fs.filemgr();
            fwrite.writeText('c:/temp/clnotassoc.txt', 
                '[NOT ASSOCIATED]\r\n' + 
                notassociated + 
                '\r\n\r\n[NOT ALLOWED]\r\n' +
                notallowed
            );
            checkfinishhandler(totals);
        }
    });

}

function removeCloudinaryImages(cloudids, removefinishhandler) {
    
    var nconf = require('nconf');
    
    nconf.env().file({ file: 'C:/development/node/yourttoo.travel/yourttoo.travel.core/settings.json' });
    
    var cloudinary = require('cloudinary');
    var config = nconf.get('cloudinaryconfig');
    cloudinary.config(config);

    var cloudqueue = new _q.Queue();
    var deletions = [];
    if (cloudids != null && cloudids.length > 0) {
        var maxitems = 10;
        var currentitems = 0;
        var currentArray = [];
        for (var i = 0, len = cloudids.length; i < len; i++) {
            if (currentArray.length == maxitems) {
                var ids = _.shuffle(currentArray);
                cloudqueue.enqueue(ids);
                currentArray = [];
                currentArray.push(cloudids[i]);
            }
            else {
                currentArray.push(cloudids[i]);
            }
        }

        var interval = setInterval(function () {
            if (cloudqueue.getLength() > 0) {
                var idstodelete = cloudqueue.dequeue();
                
                cloudinary.api.delete_resources(idstodelete, function (results) {
                    console.log('Cloudinary IDs Deleted');
                    console.log(results);
                    deletions.push(results);
                });
            } else { 
                //finished
                removefinishhandler({
                    Removed: True,
                    Deletions: deletions
                });
            }
        }, 5000);
    } else {
        removefinishhandler({
            DeletedItems : 0
        });
    }
}

var updateCloudinary = function (callback) {
    
    var start = new Date();
    console.log('Updating cloudinary scheduled task at ... ' + start);
    var _mongodb = require('../../core/dbfetch');
    var dbfetch = new _mongodb.MongoFetch();
    var core = dbfetch.getCore();
    
    var fetchCtrl = {
        cloudinaryfetch: false,
        cloudinaryresults: null,
        bbddfecth : false,
        bbddresults: null
    }
    
    fetchAllCloudinaryResources(function (results) {
        console.log('Cloudinary resources fetch done...');
        console.log(results.length);
        fetchCtrl.cloudinaryfetch = true;
        fetchCtrl.cloudinaryresults = results;
        if (fetchCtrl.bbddfecth && fetchCtrl.cloudinaryfetch) {
            buildDictionaries(fetchCtrl.cloudinaryresults, fetchCtrl.bbddresults, function (clouddict, bbdddict) {
                checkImages(clouddict, bbdddict, function (checkresults) {
                    removeCloudinaryImages(checkresults.RemovableItems, function () { 
                        callback(checkresults);
                    });
                });
            });
        }
    });

    getAllImagesFromBBDD(core, function (results) {
        console.log('BBDD images fetch done...');
        console.log(results.length);
        fetchCtrl.bbddfecth = true;
        fetchCtrl.bbddresults = results;
        if (fetchCtrl.bbddfecth && fetchCtrl.cloudinaryfetch) { 
            buildDictionaries(fetchCtrl.cloudinaryresults, fetchCtrl.bbddresults, function (clouddict, bbdddict) {
                checkImages(clouddict, bbdddict, function () { 
                    removeCloudinaryImages(checkresults.RemovableItems, function () {
                        callback(checkresults);
                    });
                });
            });
        }
    });

}
var start = exports.start = updateCloudinary;