﻿var _ = require('underscore');
var utils = require('../../tools');

function getDMCsList(core, filter, callback) {
    //cases true-false true-true false-true
    var dmccase = 'all';
    
    var condition = null;
    if ((filter.b2bchannel != null && filter.b2bchannel == true) &&
        (filter.b2cchannel == null || filter.b2cchannel == false)) {
        dmccase = 'onlyb2b';
    }
    if ((filter.b2cchannel != null && filter.b2cchannel == true) &&
        (filter.b2bchannel == null || filter.b2bchannel == false)) {
        dmccase = 'onlyb2c';
    }
    switch (dmccase) {
        case 'all':
            condition = null;
            break;
        case 'onlyb2b':
            condition =
                    { 'membership.b2bchannel': true }
            break;
        case 'onlyb2c':
            condition =
                    { 'membership.b2cchannel': true }
            break;
    }
    
    if (condition != null) {
        core.list('DMCs').model.find(condition).select('id').exec(function (err, docs) {
            var ids = [];
            if (err) {
                console.log(err);
            }
            
            if (docs != null && docs.length > 0) {
                for (var i = 0, len = docs.length; i < len; i++) {
                    ids.push(docs[i].id);
                }
            }
            callback(ids);
        });

    } else {
        callback(null);
    }

}

var buildCacheForProducts = exports.buildCacheForProducts = function (callback) {
    
    var filter = { b2cchannel: true };

    var omtcore = require('../../core');
    var allproducts = [];
    
    getDMCsList(omtcore, filter, function (dmcs) { 
        
        var query = { $and: [{ code: { $ne: null } }, { publishState: 'published' }, { dmc: { $in: dmcs } }] };
        var querystream = omtcore.list('DMCProducts').model.find(query)
        .select('code slug minprice name title title_es createdOn updatedOn publishState tags productimage dmc')
        .populate('dmc', 'name code company.name company.legalname images membership')
        .sort({ 'minprice.value' : 1 })
        .stream();
        
        querystream.on('data', function (doc) {
            console.log(doc.code);
            var t = {
                code: doc.code,
                minprice : doc.minprice,
                name: doc.name,
                title: doc.title,
                title_es: doc.title_es,
                slug: doc.slug,
                createdOn: doc.createdOn,
                updatedOn: doc.updatedOn,
                publishState: doc.publishState,
                tags: doc.tags,
                productimage: doc.productimage,
                dmc: doc.dmc
            }
            allproducts.push(doc);
        });
        
        querystream.on('error', function (err) {
            console.log(err);
        });
        
        querystream.on('close', function () {
            var cCache = require('../../omtcache/cachelocalaccess');
            var rqAll = {
                Key: 'ProductsCACHE',
                Item: allproducts
            }
            cCache.pushStore(rqAll, function (rs) {
                callback({
                    ResultOK: true,
                    Message: 'All products Cache loaded and finished',
                    Total: allproducts.length
                });
            });
        });

    });

    
}

var start = exports.start = buildCacheForProducts;
var empty = exports.empty = function () {
    buildCacheForProducts = null;
}