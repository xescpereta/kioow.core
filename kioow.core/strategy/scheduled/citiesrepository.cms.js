﻿
var cms_countries = null;
var cms_cities = null;
var update_products = null;
var bbdd_cities = null;
var utils = require('../../tools');
var hash = require('../../tools/hashtable');
var _ = require('underscore');
var _q = require('../../tools/queue');
//mongo stuff
var mongo = require('mongodb');
var objectid = require('mongodb').ObjectID;

function getCity(slugsearch) {
    var ct = null;
    if (slugsearch != null && slugsearch != '') {
        ct = cms_cities.get(slugsearch);
    }
    else {
        ct = cms_cities.get(slugsearch);
    }
    return ct;
}

function getCountry(ctcode) {
    var ct = null;
    if (ctcode != null && ctcode != '') {
        ct = cms_countries.get(ctcode.toLowerCase());
    }
    return ct;
}


function CMSBuilding(cmsfetch, callback) {
    var total = 0;
    function _end() {
        total++;
        if (total == 2) {
            callback();
        }
    }
    
    
    cmsfetch.GetAllCountries(false, function (countries) {
        for (var cc = 0; cc < countries.length; cc++) {
            var ct = countries[cc];
            if (ct != null && ct.slug != '') {
                cms_countries.set(ct.slug, ct);
            }
        }
        _end();
    });
    
    cmsfetch.GetCities(function (cities) {
        for (var cc = 0; cc < cities.length; cc++) {
            var ct = cities[cc];
            if (ct != null && ct.label_en != '') {
                cms_cities.set(utils.slug(ct.label_en.toUpperCase() + ' ' + ct.countrycode.toUpperCase()), ct);
            }
        }
        _end();
    });
    //CMS DATA

}

function findAllCities(dbfetch, finishHandler) {
    
    var finding = {
        departure: false,
        sleep: false,
        stop: false
    };
    function _isFinished() { 
        return finding.departure && finding.sleep && finding.stop;
    }
    
    var citylist = []; allcities = [];

    var query = {
        $or: [
            { 'itinerary.departurecity.city' : { $ne: null } },
            { 'itinerary.sleepcity.city' : { $ne: null } },
            { 'itinerary.stopcities.city' : { $ne: null } }]
    }
    dbfetch.getCore().list('DMCProducts').model.find(query)
    .distinct('itinerary.departurecity', function (err, departurecities) {
        for (var i = 0, len = departurecities.length; i < len; i++) {
            var city = departurecities[i];
            if (city.city != null && city.city != '') {
                var ct = '';
                if (city.location != null) {
                    ct = city.location.countrycode;
                }
                var slug = utils.slug(city.city.toUpperCase() + ' ' + ct.toUpperCase());
                city.slug = slug;
                
                if (citylist.indexOf(slug) < 0) {
                    allcities.push(city);
                    citylist.push(slug);
                }
            }
    }
    finding.departure = true;
        if (_isFinished()) { 
            finishHandler(allcities);
        }
    })
    .distinct('itinerary.sleepcity', function (err, sleepcities) {
        for (var i = 0, len = sleepcities.length; i < len; i++) {
            var city = sleepcities[i];
            if (city.city != null && city.city != '') {
                var ct = '';
                if (city.location != null) {
                    ct = city.location.countrycode;
                }
                var slug = utils.slug(city.city.toUpperCase() + ' ' + ct.toUpperCase());
                city.slug = slug;
                
                if (citylist.indexOf(slug) < 0) {
                    allcities.push(city);
                    citylist.push(slug);
                }
            }
        }
    finding.sleep = true;
        if (_isFinished()) {
            finishHandler(allcities);
        }
    })
    .distinct('itinerary.stopcities', function (err, stopcities) {
            for (var i = 0, len = stopcities.length; i < len; i++) {
                var city = stopcities[i];
                if (city.city != null && city.city != '') {
                    var ct = '';
                    if (city.location != null) {
                        ct = city.location.countrycode;
                    }
                    var slug = utils.slug(city.city.toUpperCase() + ' ' + ct.toUpperCase());
                    city.slug = slug;
                
                    if (citylist.indexOf(slug) < 0) {
                        allcities.push(city);
                        citylist.push(slug);
                    }
                }
            }
        finding.stop = true;
        if (_isFinished()) {
            finishHandler(allcities);
        }
        });
}

function checkAllCities(scope, finishHandler) {
    var cmsfetch = scope.cmsfetch;
    var dbfetch = scope.dbfetch;
    var allcities = scope.allcities;
    var results = {
        Total: allcities.length,
        Updated: 0,
        New: 0,
        NotLocationFound: 0,
        Done: 0
    }
    var total = allcities.length;
    var bulks = {
        updates: [],
        inserts: []
    };
    for (var i = 0, len = allcities.length; i < len; i++) {
        var city = allcities[i];
        //manage every city...
        var slug = city.slug;
        var cmscity = getCity(slug);
        
        if (cmscity == null) {
            //a new city to add...
            cmscity = {
                label_en: city.city,
                label_es: '',
                location: { geo: [0, 0] },
                countrycode: '', 
                slug: city.slug,
                country: null
            };
            if (city.location != null) {
                cmscity.location.geo[0] = city.location.longitude;
                cmscity.location.geo[1] = city.location.latitude;
                cmscity.countrycode = city.location.countrycode;
                var cmscountry = getCountry(city.location.countrycode);
                if (cmscountry != null) {
                    cmscity.country = objectid(cmscountry._id.toString());
                }
                bulks.inserts.push(cmscity);

            } else {
                bulks.inserts.push(cmscity);

            }
        }
        else {
            //existing city... 
            cmscity.slug = slug;
            bulks.updates.push({
                query: { _id: cmscity._id }, 
                update: {
                    $set: { slug: slug }
                }
            });

        }
    }

    //batch
    console.log('CMS ' + bulks.updates.length + ' update operations to be done on BBDD');
    console.log('CMS ' + bulks.inserts.length + ' insert operations to be done on BBDD');
    cmsfetch.BulkSaveCities(bulks, function (batchresult) {
        console.log('CMS Bulk update is finished...');
        updateProducts(scope, finishHandler);
    });
    
}

function getCMSCities(cmsfetch, finishHandler) { 
    cmsfetch.GetCities(function (cities) {
        finishHandler(cities);
    });
}

function batchProductUpdate(products, finishHandler) { 
    console.log('BATCH count: ' + products.length);
    var _mongodbbatch = require('../../core/mongobatch');
    var mongobatch = new _mongodbbatch.MongoBatch();
    
    var batchconfig = {
        collectionname : 'DMCProducts',
        inserts : null,
        updates: []
    };
    if (products != null && products.length > 0) {
        for (var i = 0, len = products.length; i < len; i++) {
            
            var bulkupdating = {
                query: { code: products[i].code },
                update: { $set: { itinerary: products[i].itinerary.toObject() } }
            };
            batchconfig.updates.push(bulkupdating);
        }
        console.log('Product Bulk update operations count: ' + batchconfig.updates.length);
        mongobatch.BulkSave(batchconfig, function (results) {
            console.log('Product Bulk update is finished...');
            finishHandler(results);
        });
    }
    else { 
        console.log('Product Bulk update is finished (nothing to update)...');
        finishHandler({
            Total: 0,
            Message: 'Product Bulk update is finished (nothing to update)'
        });
    }
}

function getProducts(scope, finishHandler) {
    var cmsfetch = scope.cmsfetch;
    var dbfetch = scope.dbfetch;
    var core = scope.dbfetch.getCore();
    var query = {
        publishState: 'published',
        'itinerary.0' : { $exists: true }
    }

    var querystream = core.list('DMCProducts').model.find(query, { code: 1, itinerary: 1 })
    .stream();

    querystream.on('data', function (doc) {
        var needtoupdate = false;
        var prd = doc;
        //update_products.set(doc.code, doc);
        for (var i = 0, len = prd.itinerary.length; i < len; i++) { 
            //departure
            if (prd.itinerary[i].departurecity.city != null && 
                prd.itinerary[i].departurecity.city != '' &&
                prd.itinerary[i].departurecity.location != null &&
                prd.itinerary[i].departurecity.location.countrycode != null && 
                prd.itinerary[i].departurecity.location.countrycode != '') {

                var slug = utils.slug(prd.itinerary[i].departurecity.city + ' ' + 
                        prd.itinerary[i].departurecity.location.countrycode);
                var city = cms_cities.get(slug);
                if (city != null) {
                    if (prd.itinerary[i].departurecity.city_es != city.label_es) {
                        prd.itinerary[i].departurecity.city_es = city.label_es;
                        prd.itinerary[i].departurecity.slug = city.slug;
                        needtoupdate = true;
                    }
                }
            }
            //sleep
            if (prd.itinerary[i].sleepcity.city != null && 
                prd.itinerary[i].sleepcity.city != '' &&
                prd.itinerary[i].sleepcity.location != null &&
                prd.itinerary[i].sleepcity.location.countrycode != null && 
                prd.itinerary[i].sleepcity.location.countrycode != '') {
                
                var slug = utils.slug(prd.itinerary[i].sleepcity.city + ' ' + 
                        prd.itinerary[i].sleepcity.location.countrycode);
                var city = cms_cities.get(slug);
                if (city != null) {
                    if (prd.itinerary[i].sleepcity.city_es != city.label_es) {
                        prd.itinerary[i].sleepcity.city_es = city.label_es;
                        prd.itinerary[i].sleepcity.slug = city.slug;
                        needtoupdate = true;
                    }
                }
            }


            //stops
            for (var j = 0, len2 = prd.itinerary[i].stopcities.length; j < len2; j++) {
                if (prd.itinerary[i].stopcities[j].city != null && 
                prd.itinerary[i].stopcities[j].city != '' &&
                prd.itinerary[i].stopcities[j].location != null &&
                prd.itinerary[i].stopcities[j].location.countrycode != null && 
                prd.itinerary[i].stopcities[j].location.countrycode != '') {
                    
                    var slug = utils.slug(prd.itinerary[i].stopcities[j].city + ' ' + 
                        prd.itinerary[i].stopcities[j].location.countrycode);
                    var city = cms_cities.get(slug);
                    if (city != null) {
                        if (prd.itinerary[i].stopcities[j].city_es != city.label_es) {
                            prd.itinerary[i].stopcities[j].city_es = city.label_es;
                            prd.itinerary[i].stopcities[j].slug = city.slug;
                            needtoupdate = true;
                        }
                    }
                }
    
            }

            //All checked!
            if (needtoupdate) {
                update_products.set(prd.code, prd);
            }
        }


    });
    querystream.on('error', function (err) {
        console.log(err);
    });
    
    querystream.on('close', function () {
        //bulk update operations.. return object
        finishHandler();
    });

}


function updateProducts(scope, finishHandler) { 
    var cmsfetch = scope.cmsfetch;
    var dbfetch = scope.dbfetch;
    
    console.log('Product Bulk update is starting...');
    getCMSCities(cmsfetch, function (cities) {
        cms_cities = null;
        cms_cities = new hash.HashTable();

        for (var cc = 0; cc < cities.length; cc++) {
            var ct = cities[cc];
            if (ct != null && ct.label_en != '') {
                cms_cities.set(utils.slug(ct.label_en.toUpperCase() + ' ' + ct.countrycode.toUpperCase()), ct);
            }
        }
        
        getProducts(scope, function () { 
            //we've got everything...
            batchProductUpdate(update_products.values(), finishHandler);
        });

      
    });
}

//for each city...
function checkCityProducts(core, city, finishHandler) {
    var query = {
        $or: []
    }
    var bulkupdating = [];

    query.$or.push({
        'itinerary.departurecity.city' : city.label_en,
        'itinerary.departurecity.location.countrycode' : city.countrycode,
    });
    query.$or.push({
        'itinerary.sleepcity.city' : city.label_en,
        'itinerary.sleepcity.location.countrycode' : city.countrycode,
    });
    query.$or.push({
        'itinerary.stopcities.city' : city.label_en,
        'itinerary.stopcities.location.countrycode' : city.countrycode,
    });

    var querystream = core.list('DMCProducts').model.find(query, { code: 1, itinerary: 1 })
    .stream();
    
    querystream.on('data', function (doc) {
        //console.log('Finded for city ' + city.label_en + '(' + city.label_es + ')' + ', Product code: ' + doc.code);
        var prd = update_products.get(doc.code);
        var needtoupdate = false;
        if (prd != null) { 
            needtoupdate = true;
        } else { 
            prd = doc;
        }
        for (var i = 0, len = prd.itinerary.length; i < len; i++) { 
            //departure
            if (prd.itinerary[i].departurecity.city == city.label_en && prd.itinerary[i].departurecity.location != null &&
                prd.itinerary[i].departurecity.location.countrycode == city.countrycode) {
                if (prd.itinerary[i].departurecity.city_es != city.label_es) { 
                    prd.itinerary[i].departurecity.city_es = city.label_es;
                    prd.itinerary[i].departurecity.slug = city.slug;
                    needtoupdate = true;
                }
            }
            //sleep
            if (prd.itinerary[i].sleepcity.city == city.label_en && prd.itinerary[i].sleepcity.location != null &&
                prd.itinerary[i].sleepcity.location.countrycode == city.countrycode) {
                if (prd.itinerary[i].sleepcity.city_es != city.label_es) { 
                    prd.itinerary[i].sleepcity.city_es = city.label_es;
                    prd.itinerary[i].sleepcity.slug = city.slug;
                    needtoupdate = true;
                }
                
            }
            //stops
            for (var j = 0, len2 = prd.itinerary[i].stopcities.length; j < len2; j++) { 
                if (prd.itinerary[i].stopcities[j].city == city.label_en && prd.itinerary[i].stopcities[j].location != null &&
                    prd.itinerary[i].stopcities[j].location.countrycode == city.countrycode) {
                    if (prd.itinerary[i].stopcities[j].city_es != city.label_es) { 
                        prd.itinerary[i].stopcities[j].city_es = city.label_es;
                        prd.itinerary[i].stopcities[j].slug = city.slug;
                        needtoupdate = true;
                    }    
                }
                    
            }
        }
        if (needtoupdate) {
            update_products.set(prd.code, prd);
        }
    });
    querystream.on('error', function (err) {
        console.log(err);
    });
    
    querystream.on('close', function () {
        //bulk update operations.. return object
        finishHandler();
    });

}


var citiesrepository = function (callback) { 
    
    //init
    var bbddfetch = {
        cms: false,
        bbdd: false
    }
    var scope = {
        dbfetch: null,
        cmsfetch: null,
        allcities: null
    };
    var citiesQueue = new _q.Queue();
    //intialization ok!
    var start = new Date();
    console.log('Updating CMS cities scheduled task at ... ' + start);
    var errs = [];
    //core connectors
    var _mongodb = require('../../core/dbfetch');
    var dbfetch = new _mongodb.MongoFetch();
    var _cmsdb = require('../../core/cmsfetch');
    var cmsfetch = new _cmsdb.CmsFetch();
    scope.dbfetch = dbfetch;
    scope.cmsfetch = cmsfetch;

    //hashes
    cms_countries = new hash.HashTable();
    cms_cities = new hash.HashTable();
    bbdd_cities = new hash.HashTable();
    update_products = new hash.HashTable();
    //Let's Start...
    CMSBuilding(cmsfetch, function () {
        bbddfetch.cms = true;
        console.log('CMS DATA FETCH DONE...');
        if (bbddfetch.cms && bbddfetch.bbdd) { 
            checkAllCities(scope, callback);
        }
    });
    findAllCities(dbfetch, function (cities) {
        bbddfetch.bbdd = true;
        console.log('BBDD CITIES DATA FETCH DONE...');
        scope.allcities = cities;
        if (bbddfetch.cms && bbddfetch.bbdd) { 
            checkAllCities(scope, callback);
        }
    });
}
var start = exports.start = citiesrepository;