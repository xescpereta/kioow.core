﻿

//AUX
function comparelocations(a, b) {

	if (a.countrycode < b.countrycode)
		return -1;
	if (a.countrycode > b.countrycode)
		return 1;
	return 0;
}
function pagination(docs, itemspage) {
	var pg = require('../tools/pager');
	var pager = new pg.Pager(docs, null, null, itemspage);
	return pager;
}
function _indexOfYear(year, availability) {
	var index = -1;
	if (availability != null && availability.length > 0) {
		for (var i = 0; i < availability.length; i++) {
			if (availability[i].year == year) {
				index = i;
				break;
			}
		}
	}
	return index;
}
function _getMonthNameEnglish(monthindex) {
	if (monthindex == 0) { return 'January'; }
	if (monthindex == 1) { return 'February'; }
	if (monthindex == 2) { return 'March'; }
	if (monthindex == 3) { return 'April'; }
	if (monthindex == 4) { return 'May'; }
	if (monthindex == 5) { return 'June'; }
	if (monthindex == 6) { return 'July'; }
	if (monthindex == 7) { return 'August'; }
	if (monthindex == 8) { return 'September'; }
	if (monthindex == 9) { return 'October'; }
	if (monthindex == 10) { return 'November'; }
	if (monthindex == 11) { return 'December'; }
}
function _calculatePriceMinimumDates(from, to, availability) {
	//		console.log("\n*********************");
	//		console.log("calculatePriceMinumun ");
	//		console.log("*********************");
	//		console.log("from: "+from);
	//		console.log("to: "+to);
	var pricemin = {
		value: 0,
		currency: ""
	}

	if (availability == null || availability.length == 0) {
		return pricemin;
	}

	var monthstart = from.getMonth();
	var monthend = to.getMonth();
	var iterate = new Date(from.getFullYear(), from.getMonth(), 1);

	while (iterate <= to) {

		var indexyear = _indexOfYear(iterate.getFullYear(), availability);

		//console.log("*iteracion: "+iterate+" "+to+" indexyear: "+indexyear);
		if (indexyear > -1) {
			var month = _getMonthNameEnglish(iterate.getMonth());
			if (availability[indexyear] != null) {
				var avails = availability[indexyear][month].availability;
				if (avails != null && avails.length > 0) {
					for (var jj = 0; jj < avails.length; jj++) {

						//console.log("@@@@@@@@@@@@@@ jj: "+jj+" precio: "+avails[jj].rooms.double.price+" moneda: "+avails[jj].rooms.currency);
						if (avails[jj].rooms.double.price > 0) {

							if (pricemin.value == 0) {
								pricemin.value = avails[jj].rooms.double.price;
								pricemin.currency = avails[jj].rooms.currency;

							}
							else {
								if (avails[jj].rooms.double.price < pricemin.value) {
									pricemin.value = avails[jj].rooms.double.price;
									pricemin.currency = avails[jj].rooms.currency;
								}
							}
						}
					}
				}
			}
		}
		iterate.setMonth(iterate.getMonth() + 1);
	}
	//console.log("\n\t precio minimo: "+pricemin.value+"\n");

	return pricemin;
}
function _saveCache(countries, tags, callback) {
    var pending = 2;

    var cCache = require('../../omtcache/cachelocalaccess');
    var rqlocs = {
        Key: 'CMSCountriesCACHE',
        Item: countries
    };
    var rqtags = {
        Key: 'CMSTripTagsCACHE',
        Item: tags
    };
    cCache.pushStore(rqlocs, function (rs) {
        pending--;
        if (pending == 0) {
            callback({
                key: 'CMS BUILDING CACHE',
                message: 'Process finished'
            });
        }
    });
    
    cCache.pushStore(rqtags, function (rs) {
        pending--;
        if (pending == 0) {
            callback({
                key: 'CMS BUILDING CACHE',
                message: 'Process finished'
            });
        }
    });
}
//END AUX

var buildCacheCMS = exports.buildCacheCMS = function (callback) {
    
    var utils = require('../../tools');
    
    var hash = require('../../tools/hashtable');
    var _ = require('underscore');
    
    //api de cmsfetch
    var _cmsdb = require('../../core/cmsfetch');
    var cmsfetch = new _cmsdb.CmsFetch();
    //hash 
    var hash = require('../../tools/hashtable');
    
    //intialization ok!
    
    
    if (cmsfetch == null) {
        cmsfetch = new _cmsdb.CmsFetch();
    }
    
    var h_countries = new hash.HashTable();
    var start = new Date();
    console.log('Build CMS Countries Cache Started at ... ' + start);

    cmsfetch.GetAllCountries(false, function (countries) {
        if (countries != null && countries.length > 0) {
            var ctCount = countries.length;
            _.each(countries, function (country) {
                h_countries.set(country.slug, country);
                ctCount--;
                
                if (ctCount == 0) {
                    var h_tags = new hash.HashTable();
                    var start = new Date();
                    console.log('Build CMS Tags Cache Started at ... ' + start);
                    cmsfetch.GetTripTags(function (tags) {
                        if (tags != null && tags.length > 0) {
                            var ttCount = tags.length;
                            _.each(tags, function (tag) {
                                h_tags.set(tag.slug, tag);
                                ttCount--;
                                
                                if (ttCount == 0) {
                                    _saveCache(h_countries, h_tags, function (result) {
                                        callback(result);
                                        cmsfetch = null;
                                        h_countries = null;
                                        h_tags = null;
                                    });

                                }
                            });
                        }
                    });
                    
                }
            });
        }
        else {
            _saveCache([], [], function (result) {
                callback(result);
                cmsfetch = null;
                h_countries = null;
                h_tags = null;
            });
        }
    });
}

var start = exports.start = buildCacheCMS;
var empty = exports.empty = function () {
    buildCacheCMS = null;
}