﻿module.exports = function (options, callback, errorcallback) { 
    
    var core = options.core;
    var started = new Date();
    console.log('Scheduled task started at ' + started);


    core.list('DMCs').model.find({ code: { $ne: null } }).exec(function (err, docs) {
        
        if (err) {
            errorcallback(err);
        }
        else {
            console.log('DMCs: ' + docs.length);
            callback({
                Finded: docs.length,
                ResultOK: true
            });
        }


    });

}