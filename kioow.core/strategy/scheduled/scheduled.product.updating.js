﻿


var updateProducts = function (callback) {
    var utils = require('../../tools');
    var _cmsdb = require('../../core/cmsfetch');
    var cmsfetch = new _cmsdb.CmsFetch();
	

	var hash = require('../../tools/hashtable');
	var _ = require('underscore');
	var _q = require('../../tools/queue');
	var productQueue = new _q.Queue();
	//intialization ok!
	var start = new Date();
	console.log('Updating products scheduled task at ... ' + start);
	var errs = [];
	//Products...
	var _mongodb = require('../../core/dbfetch');
	var dbfetch = new _mongodb.MongoFetch();
	var currentcurrency = null;
	var currencys = require('../../tools/static/data').currencys;

	cmsfetch.GetAllExchangeCurrency(false, function (exchanges) {

        //AUX FUNCTION...
	    function convertValueToCurrency(value, currencyOrig, currencyDest, exchanges) {

	        var fin = false;
	        for (var itExc = 0, len = exchanges.length; itExc < len; itExc++) {

	            // 1) si encuentro la tasa de cambio
	            if (exchanges[itExc].origin == currencyOrig && exchanges[itExc].destination == currencyDest) {
	                //console.log("cambio encontrado: "+exchanges[itExc].change);
	                return Math.round(value * exchanges[itExc].change);
	            }

	                // 2) si encuentro la tasa de cambio inversa
	            else if (exchanges[itExc].destination == currencyOrig && exchanges[itExc].origin == currencyDest) {
	                //console.log("cambio INVERSO encontrado: "+exchanges[itExc].change);
	                return Math.round(value / exchanges[itExc].change);
	            }
	        }
	        return null;
	    }
	    function pad(str, max) {
	        str = str.toString();
	        return str.length < max ? pad("0" + str, max) : str;
	    }
	    //EXCHANGE AUX
	    for (var i = 0, len = currencys.length; i < len; i++) {
	        if (currencys[i].value == 'EUR') {
	            currentcurrency = currencys[i];
	            break;
	        }
	    }

	    var months = ['January', 'February', 'March',
            'April', 'May', 'June', 'July', 'August',
            'September', 'October', 'November', 'December'];
	    var totals = {
	        WithPrice: { count: 0 },
	        WithoutPrice: {
	            count: 0,
	            products: []
	        }
	    };
	    dbfetch.GetAllDMCProductCodes(function (codes) {
	        productQueue.enqueueCollection(codes);
	        console.log(codes);
	        //every 2 sg...
	        var pending = codes.length;
	        var interval = setInterval(function () {
	            var code = productQueue.dequeue();

	            if (code != null && code != '') {
	                dbfetch.GetDMCProductByCode(code, function (product) {
	                    //the updating (Availability and Prices)....
	                    var availability = utils.removeObsoleteAvailability(product);
	                    if (availability != null) {
	                        product.availability = availability;
	                    }
	                    var prices = utils.getMinimumPrices(product);
	                    if (prices != null && prices.length > 0) {
	                        product.prices = prices;
	                        var values = _.map(prices, function (price) {
	                            var today = new Date();
	                            var year = today.getFullYear();
	                            if (
                                    price.year >= year &&
                                    months.indexOf(price.month) >= today.getMonth()
                                    ) {
	                                return price.minprice;
	                            } else {
	                                return 0;
	                            }
	                        });
	                        
	                        values = _.filter(values, function (v) { return v > 0; });
	                        currencies = _.pluck(prices, 'currency');
	                        currencies = _.filter(currencies, function (curr)
	                        { return (curr != null && curr.label != null && curr.label != '') });
	                        var cc = {};
	                        if (currencies != null && currencies.length > 0) {
	                            cc = _.find(currencies, function (c) { return (c.label != null && c.label != ''); });
	                        }
	                        if (cc == null) {
	                            cc.label = '';
	                            cc.symbol = '';
	                            cc.value = '';
	                        }
	                        if (values != null && values.length > 0) {
	                            values.sort(function (a, b) { return a - b; });
	                            var dateprices = _.filter(prices, function (price) {
	                                return price.minprice == values[0];
	                            });
	                            product.minprice.value = values[0];
	                            product.minprice.currency.label = cc.label;
	                            product.minprice.currency.symbol = cc.symbol;
	                            product.minprice.currency.value = cc.value;
	                            product.priceindexing = pad(product.minprice.value, 10) + '.' + product.code;
	                            if (dateprices != null && dateprices.length > 0) {
	                                product.minprice.month = dateprices[0].month;
	                                product.minprice.year = dateprices[0].year;
	                            }
	                            if (product.minprice.currency != null &&
                                                    product.minprice.currency != null &&
                                                    product.minprice.currency.value != 'EUR') {

	                                product.minprice.exchange = {
	                                    value: 0,
	                                    currency: {
	                                        label: '',
	                                        value: '',
                                            symbol: ''
	                                    }
	                                }

	                                product.minprice.exchange.value = convertValueToCurrency(product.minprice.value,
                                                    product.minprice.currency.value, 'EUR', exchanges);
	                                product.minprice.exchange.currency = currentcurrency;
	                                console.log('Exchange detected...');
	                                console.log(product.minprice);
	                            }

	                        } else {
	                            product.minprice.value = 0;
	                            product.minprice.currency.label = '';
	                            product.minprice.currency.symbol = '';
	                            product.minprice.currency.value = '';
	                            product.priceindexing = 'zzzz';
	                        }
	                    }
	                    else {
	                        product.priceindexing = 'zzzz';
	                    }
	                    if (product.itinerary != null) {
	                        product.itinerarylength = product.itinerary.length;
	                        product.itinerarylengthindexing = pad(product.itinerary.length, 5) + '.' + product.code;
	                    } else {
	                        product.itinerarylength = 0;
	                        product.itinerarylengthindexing = pad(0, 5) + '.' + product.code;
	                    }
	                    //saving proces...
	                    product.save(function (err, doc) {
	                        if (err) {
	                            console.log('Error updating: ' + code);
	                            console.log(err);
	                        }
	                        if (doc) {
	                            console.log(
                                    'Product updated! : ' + code +
                                    ' (pending: ' + (pending - 1) + '/' + codes.length + ') ' + ' minprice: ' +
                                    product.minprice.value + ' ' + product.minprice.currency.label +
                                    ' month: ' + product.minprice.month + ' year: ' + +product.minprice.year);
	                            if (product.minprice.value > 0) {
	                                totals.WithPrice.count++;
	                            } else {
	                                totals.WithoutPrice.count++;
	                                totals.WithoutPrice.products.push(doc.code + '#' + doc.publishState);
	                            }
	                        }
	                        pending--;
	                        if (pending == 0) {
	                            clearInterval(interval);
	                            callback({
	                                Totals: totals,
	                                UpdatedCount: codes.length,
	                                Started: start,
	                                Finished: new Date(),
	                                Errors: errs
	                            });
	                        }
	                    })
	                });
	            }
	            else {
	                pending--;
	                if (pending == 0) {
	                    clearInterval(interval);
	                    callback({
	                        UpdatedCount: codes.length,
	                        Started: start,
	                        Finished: new Date(),
	                        Errors: errs
	                    });
	                }
	            }

	        }, 600);

	    });


	});
	
}

var start = exports.start = updateProducts;