﻿
var bindcitycountry = function (callback) { 
    //init
    var _cmsdb = require('../../core/cmsfetch');
    var cmsfetch = new _cmsdb.CmsFetch();
    var utils = require('../../tools');
    var hash = require('../../tools/hashtable');
    var _ = require('underscore');

    var mongo = require('mongodb');
    var objectid = require('mongodb').ObjectID;

    var cms_countries = new hash.HashTable();
    var cms_cities = new hash.HashTable();


    function CMSBuilding(callback) {
        var total = 0;
        function _end() {
            total++;
            if (total == 2) {
                callback();
            }
        }
        
       
        cmsfetch.GetAllCountries(false, function (countries) {
            for (var cc = 0; cc < countries.length; cc++) {
                var ct = countries[cc];
                if (ct != null && ct.slug != '') {
                    cms_countries.set(ct.slug, ct);
                }
            }
            _end();
        });
        
        cmsfetch.GetCities(function (cities) {
        	if(cities != null && cities.length>0){
	            for (var cc = 0; cc < cities.length; cc++) {
	                var ct = cities[cc];
	                if (ct != null && ct.label_en != '') {
	                    cms_cities.set(ct.label_en.toUpperCase() + '_' + ct.countrycode.toUpperCase(), ct);
	                }
	            }
	        }
            _end();
        });
    //CMS DATA

    }


    CMSBuilding(function () {
        var cities = cms_cities.values();

        var count = cities.length;
        _.each(cities, function (city) {
            var ct = cms_countries.get(city.countrycode.toLowerCase());
            if (ct != null) {
                city.country = objectid(ct._id.toString());
                cmsfetch.SaveCity(city, function () {
                    console.log('Updated city ' + city.label_en + ' | ' + city.countrycode);
                    console.log('ID country: ' + ct._id);
                    count--;
                    if (count == 0) {
                        callback({ ResultOK: true, Message: 'All country field for CMS Cities are updated.' });
                    }
                });
            }
            else { 
                console.log('Country for city ' + city.label_en + '[' + city.countrycode + '] not found');
                count--;
                if (count == 0) {
                    callback({ ResultOK: true, Message: 'All country field for CMS Cities are updated.' });
                }
            }
        });

    });
}

var start = exports.start = bindcitycountry;