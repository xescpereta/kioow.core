﻿

//AUX
function comparelocations(a, b) {

	if (a.countrycode < b.countrycode)
		return -1;
	if (a.countrycode > b.countrycode)
		return 1;
	return 0;
}
function pagination(docs, itemspage) {
	var pg = require('../tools/pager');
	var pager = new pg.Pager(docs, null, null, itemspage);
	return pager;
}
function _indexOfYear(year, availability) {
	var index = -1;
	if (availability != null && availability.length > 0) {
		for (var i = 0; i < availability.length; i++) {
			if (availability[i].year == year) {
				index = i;
				break;
			}
		}
	}
	return index;
}
function _getMonthNameEnglish(monthindex) {
	if (monthindex == 0) { return 'January'; }
	if (monthindex == 1) { return 'February'; }
	if (monthindex == 2) { return 'March'; }
	if (monthindex == 3) { return 'April'; }
	if (monthindex == 4) { return 'May'; }
	if (monthindex == 5) { return 'June'; }
	if (monthindex == 6) { return 'July'; }
	if (monthindex == 7) { return 'August'; }
	if (monthindex == 8) { return 'September'; }
	if (monthindex == 9) { return 'October'; }
	if (monthindex == 10) { return 'November'; }
	if (monthindex == 11) { return 'December'; }
}
function _calculatePriceMinimumDates(from, to, availability) {
	//		console.log("\n*********************");
	//		console.log("calculatePriceMinumun ");
	//		console.log("*********************");
	//		console.log("from: "+from);
	//		console.log("to: "+to);
	var pricemin = {
		value: 0,
		currency: ""
	}

	if (availability == null || availability.length == 0) {
		return pricemin;
	}

	var monthstart = from.getMonth();
	var monthend = to.getMonth();
	var iterate = new Date(from.getFullYear(), from.getMonth(), 1);

	while (iterate <= to) {

		var indexyear = _indexOfYear(iterate.getFullYear(), availability);

		//console.log("*iteracion: "+iterate+" "+to+" indexyear: "+indexyear);
		if (indexyear > -1) {
			var month = _getMonthNameEnglish(iterate.getMonth());
			if (availability[indexyear] != null) {
				var avails = availability[indexyear][month].availability;
				if (avails != null && avails.length > 0) {
					for (var jj = 0; jj < avails.length; jj++) {

						//console.log("@@@@@@@@@@@@@@ jj: "+jj+" precio: "+avails[jj].rooms.double.price+" moneda: "+avails[jj].rooms.currency);
						if (avails[jj].rooms.double.price > 0) {

							if (pricemin.value == 0) {
								pricemin.value = avails[jj].rooms.double.price;
								pricemin.currency = avails[jj].rooms.currency;

							}
							else {
								if (avails[jj].rooms.double.price < pricemin.value) {
									pricemin.value = avails[jj].rooms.double.price;
									pricemin.currency = avails[jj].rooms.currency;
								}
							}
						}
					}
				}
			}
		}
		iterate.setMonth(iterate.getMonth() + 1);
	}
	//console.log("\n\t precio minimo: "+pricemin.value+"\n");

	return pricemin;
}
function _saveCache(dmclocations, dmcs, callback) {
    var pending = 2;
    var cCache = require('../../omtcache/cachelocalaccess');
    var rqlocs = {
        Key: 'DMCLocationsCACHE',
        Item: dmclocations
    };
    var rqdmc = {
        Key: 'DMCsCACHE',
        Item: dmcs
    };
    cCache.pushStore(rqlocs, function (rs) {
        pending--;
        if (pending == 0) {
            callback({
                key: 'DMC BUILDING CACHE',
                message: 'Process finished'
            });
        }
    });

    cCache.pushStore(rqdmc, function (rs) {
        pending--;
        if (pending == 0) {
            callback({
                key: 'DMC BUILDING CACHE',
                message: 'Process finished'
            });
        }
    });
}
//END AUX


var buildCacheForDMCs = exports.buildCacheForDMCs = function (callback) {

	var utils = require('../../tools');

	var hash = require('../../tools/hashtable');
	var _ = require('underscore');

	//api de cmsfetch
	var _cmsdb = require('../../core/cmsfetch');
	var cmsfetch = new _cmsdb.CmsFetch();
	//hash 
	var hash = require('../../tools/hashtable');

	//intialization ok!


	if (cmsfetch == null) {
		cmsfetch = new _cmsdb.CmsFetch();
	}
	cmsfetch.GetAllCountries(false, function (countries) {
		var start = new Date();
		console.log('Build DMCs Cache Started at ... ' + start);
		
		var cms_countries = new hash.HashTable();



		if (countries != null && countries.length > 0) {

			//Users.. DMCs.. Travelers...
			var member = require('../../auth/membership');
			var membership = new member.Membership();

			var ctCount = countries.length;
			_.each(countries, function (country) {
				cms_countries.set(country.slug, country);
				ctCount--;

				if (ctCount == 0) {
					if (membership == null) {
						membership = new member.Membership();
					}
					//get all dmcs.. and save it..
					membership.GetAllDMCs(function (dmcs) {
						var h_countries = new hash.HashTable();
						dmcs.sort(utils.comparedmcs);
						var dmcCount = dmcs.length;
						var dmclocations = [];
						_.each(dmcs, function (dmc) {
							var locs = utils.get_all_locations_from_dmc(dmc);
							if (locs != null && locs.length > 0) {
								for (var li = 0; li < locs.length; li++) {
									var theloc = locs[li];
									if (h_countries.hasItem(theloc.country.toLowerCase())) {
										var thloc = h_countries.get(theloc.country.toLowerCase());
										thloc.quantity++;
										var valid = (dmc.membership.registervalid &&
													dmc.membership.publicprofilecomplete &&
													dmc.membership.companyimagescomplete &&
													dmc.membership.companycomplete &&
													dmc.membership.paymentcomplete &&
													dmc.membership.companytaxid &&
													dmc.membership.emergencycomplete);

										if (valid == true) {
											thloc.quantity_valid++;
										}
										h_countries.remove(theloc.country.toLowerCase());
										h_countries.set(theloc.country.toLowerCase(), thloc);
									}
									else {
										var ct_es = '';
										if (cms_countries.hasItem(theloc.countrycode.toLowerCase())) {
											ct_es = cms_countries.get(theloc.countrycode.toLowerCase()).label_es;
										}

										var qtv = 0;
										var valid = (dmc.membership.registervalid &&
													dmc.membership.publicprofilecomplete &&
													dmc.membership.companyimagescomplete &&
													dmc.membership.companycomplete &&
													dmc.membership.paymentcomplete &&
													dmc.membership.companytaxid &&
													dmc.membership.emergencycomplete);

										if (valid == true) {
											qtv++;
										}
										var ct = {
											country: theloc.country,
											country_es: ct_es,
											countrycode: theloc.countrycode,
											quantity: 1,
											quantity_valid: qtv
										};
										h_countries.set(theloc.country.toLowerCase(), ct);
										//dmclocations.push(theloc);
									}
									//if (!utils.exists_in_array(locs[li], dmclocations)) {
									//    dmclocations.push(locs[li]);
									//}
									dmcCount--;
									if (dmcCount == 0) {
										dmclocations = h_countries.values();
										dmclocations.sort(comparelocations);
										_saveCache(dmclocations, dmcs, function (result) {

											callback(result);

											h_countries = null;
											dmclocations = null;
											cms_countries = null;
											cmsfetch = null;
											membership = null;
										});
									}
								}
							} else {
								//nothing to do...
							}
						});

					});

				}
			});
		} else {
			_saveCache([], [], function (result) {

				callback(result);

				h_countries = null;
				dmclocations = null;
				cms_countries = null;
				cmsfetch = null;
				membership = null;
			});	
		}
	});

	
	

}
var start = exports.start = buildCacheForDMCs;
var empty = exports.empty = function () {
	buildCacheForDMCs = null;
}