﻿var common = require('kioow.common');
var _ = require('underscore');

module.exports = function (options, callback, errorcallback) {
    var core = options.core;

    var months = common.staticdata.months_en;
    var currencys = common.staticdata.currencys;
    

    //event complete trigger
    var trigger = function () {
        this.name = 'Completed tasks emiter';
        this.done = function (eventname, assertcondition, data) {
            if (assertcondition) {
                this.emit(eventname, data);
            }
        }
    };
    common.eventtrigger.eventtrigger(trigger);
    var eventlauncher = new trigger();

    eventlauncher.on('cms.done', function (cmsdata) { 
        update(cmsdata);
    });

    function hashcms() {
        var cms_required = {
            cities: false,
            countries: false,
            exchanges: false
        };
        var cms_data = {
            countries : new common.hashtable.HashTable(),
            cities: new common.hashtable.HashTable(),
            exchanges: []
        };
        //get countries in cms
        var ct_querystream = coreobjcms.list('Countries').find().stream();
        ct_querystream.on('data', function (doc) {
            if (doc != null && doc.slug != null && doc.slug != '') {
                cms_data.countries.set(doc.slug, doc);
            }
        });
        ct_querystream.on('error', function (err) {
            console.log(err);
            errorcallback(err);
        });
        ct_querystream.on('close', function (err) {
            cms_required.countries = true;
            eventlauncher.done('cms.done', (cms_required.countries && cms_required.cities && cms_required.exchanges), cms_data);
        });
        //get cities in cms
        var tg_querystream = coreobjcms.list('Cities').find().stream();
        tg_querystream.on('data', function (doc) {
            if (doc != null && doc.slug != null && doc.slug != '') {
                cms_data.cities.set(doc.slug, doc);
            }
        });
        tg_querystream.on('error', function (err) {
            console.log(err);
            errorcallback(err);
        });
        tg_querystream.on('close', function (err) {
            cms_required.cities = true;
            eventlauncher.done('cms.done', (cms_required.countries && cms_required.cities && cms_required.exchanges), cms_data);
        });
        //get exchange for currency
        var ex_querystream = coreobjcms.list('Exchanges').find({ state: 'published' }).stream();
        ex_querystream.on('data', function (exx) { 
            cms_data.exchanges = exx;
        });
        ex_querystream.on('error', function (err) {
            console.log(err);
            errorcallback(err);
        });
        ex_querystream.on('close', function (err) {
            cms_required.exchanges = true;
            eventlauncher.done('cms.done', (cms_required.countries && cms_required.cities && cms_required.exchanges), cms_data);
        });
    }
    function update(cmsdata) {
        var query = { code: options.code };
        var product = null;
        var querystream = core.corebase.list('DMCProducts').model.find(query).stream();

        querystream.on('data', function (doc) {
            product = doc;
        });

        querystream.on('error', function (err) {
            console.log(err);
        });

        querystream.on('close', function () {
            if (product != null) {
                var prices = common.utils.getMinimumPrices(product);
                if (prices != null && prices.length > 0) {
                    product.prices = prices;

                    var values = _.map(prices, function (price) {
                        var today = new Date();
                        var year = today.getFullYear();
                        
                        if (price.year > year) {
                            return price.minprice
                        }
                        else {
                            if (months.indexOf(price.month) >= today.getMonth()) {
                                return price.minprice;
                            } else {
                                return 0;
                            }
                        }
                    });
                    values = _.filter(values, function (v) { return v > 0; });
                    currencies = _.pluck(prices, 'currency');
                    currencies = _.filter(currencies, function (curr) { return (curr != null && curr.label != null && curr.label != '') });
                    var cc = {};
                    if (currencies != null && currencies.length > 0) {
                        cc = _.find(currencies, function (c) { return (c.label != null && c.label != ''); });
                    }
                    if (cc == null) {
                        cc.label = '';
                        cc.symbol = '';
                        cc.value = '';
                    }

                    if (values != null && values.length > 0) {
                        values.sort(function (a, b) { return a - b; });
                        var dateprices = _.filter(prices, function (price) {
                            return price.minprice == values[0];
                        });
                        product.minprice.value = values[0];
                        product.minprice.currency.label = cc.label;
                        product.minprice.currency.symbol = cc.symbol;
                        product.minprice.currency.value = cc.value;
                        product.priceindexing = pad(product.minprice.value, 10) + '.' + product.code;
                        if (dateprices != null && dateprices.length > 0) {
                            product.minprice.month = dateprices[0].month;
                            product.minprice.year = dateprices[0].year;
                        }
                        if (product.minprice.currency != null &&
                                                    product.minprice.currency != null &&
                                                    product.minprice.currency.value != 'EUR') {
                            
                            product.minprice.exchange = {
                                value: 0,
                                currency: {
                                    label: '',
                                    value: '',
                                    symbol: ''
                                }
                            }
                            
                            product.minprice.exchange.value = common.utils.convertValueToCurrency(product.minprice.value,
                                                    product.minprice.currency.value, 'EUR', cmsdata.exchanges);
                            product.minprice.exchange.currency = currentcurrency;
                            console.log('Exchange detected...');
                            console.log(product.minprice);
                        }

                    } else {
                        product.minprice.value = 0;
                        product.minprice.currency.label = '';
                        product.minprice.currency.symbol = '';
                        product.minprice.currency.value = '';
                        product.priceindexing = 'zzzz';
                        product.publishState = 'unpublished';
                    }

                } else {
                    product.priceindexing = 'zzzz';
                    product.publishState = 'unpublished';
                }
                if (product.itinerary != null) {
                    product.itinerarylength = product.itinerary.length;
                    product.itinerarylengthindexing = common.utils.pad(product.itinerary.length, 5) + '.' + product.code;
                } else {
                    product.itinerarylength = 0;
                    product.itinerarylengthindexing = common.utils.pad(0, 5) + '.' + product.code;
                }
                //product = common.utils.updateproductcities(product, cmsdata.cities);

                product.save(function (err, doc) {
                    if (err) {
                        console.log('Error updating: ' + product.code);
                        console.log(err);
                        errorcallback(err);
                    }

                    if (doc) { 
                        console.log(
                            'Product updated! : ' + product.code + ' minprice: ' +
                                    product.minprice.value + ' ' + product.minprice.currency.label +
                                    ' month: ' + product.minprice.month + ' year: ' + +product.minprice.year);
                        callback({
                            product: product,
                            date: new Date(),
                            message: 'Product updated! - Minprice, Cities -  '
                        });
                    }
                });


            }
        });
    }

    hashcms(); 
}