﻿var common = require('kioow.common');
var _ = require('underscore');

module.exports = function (options) {
    var pricemodels = ['DMCProducts', 'MigrationProducts'];

    var doc = options.document;
    var m_currentcurrency = options.currency;
    var exchanges = options.exchanges;
    var currentcurrency = options.currentcurrency;
    var months = common.staticdata.months_en;
    var modelname = (doc != null && doc.list != null && doc.list.model != null) ? doc.list.model.modelName : null;
    console.log('model: ' + modelname);
    if (pricemodels.indexOf(modelname) >= 0) {
        console.log('change currency for price decorator...');
        console.log(doc.minprice.currency);
        if (doc.minprice.currency != null && doc.minprice.currency.value != m_currentcurrency) {
            
            console.log('change currency...');
            console.log('change currency minprice...');
            doc.minprice.value = common.utils.convertValueToCurrency(doc.minprice.value,
                                        doc.minprice.currency.value, m_currentcurrency, exchanges);
            doc.minprice.currency = currentcurrency;
            console.log('change currency prices...');
            if (doc.prices != null && doc.prices.length > 0) {
                for (var i = 0, len = doc.prices.length; i < len; i++) {
                    doc.prices[i].minprice = common.utils.convertValueToCurrency(doc.prices[i].minprice,
                                        doc.prices[i].currency.value, m_currentcurrency, exchanges);
                    doc.prices[i].currency = currentcurrency;
                }
            }
            console.log('change currency availability...');
            if (doc.availability != null && doc.availability.length > 0) {
                _.each(doc.availability, function (availyear) {
                    _.each(months, function (month) {
                        if (availyear[month] != null && 
                    availyear[month].availability != null && 
                    availyear[month].availability.length > 0) {
                            _.each(availyear[month].availability, function (availday) {

                                availday.rooms.triple.price = availday.rooms.triple.price > 0 ? 
                                    common.utils.convertValueToCurrency(availday.rooms.triple.price,
                                        availday.rooms.currency.value, m_currentcurrency, exchanges) : 0

                                availday.rooms.double.price = availday.rooms.double.price > 0 ? 
                                    common.utils.convertValueToCurrency(availday.rooms.double.price,
                                        availday.rooms.currency.value, m_currentcurrency, exchanges) : 0
                                
                                availday.rooms.single.price = availday.rooms.single.price > 0 ? 
                                    common.utils.convertValueToCurrency(availday.rooms.single.price,
                                        availday.rooms.currency.value, m_currentcurrency, exchanges) : 0

                                availday.rooms.currency = currentcurrency;
                            });
                        }

                    });
                });
            }

        } else {
            console.log('currency ok...');
        }
    }

    return doc;
}