﻿module.exports = function (options, callback, errorcallback) {
    var core = options.core;
    
    var mongo = core.mongo;
    var coreobj = core.corebase;

    for (var prop in options.query) {
        var text = options.query[prop];
        options.query[prop] = new RegExp(text, "i");
    }

    mongo.find(options, function (results) {
        if (results.ResultOK == true) {
            callback(results.Data);
        } else {
            errorcallback(results);
        }

    });
}
