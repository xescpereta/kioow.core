﻿//**************** OPEN MARKET HERMES SERVER STARTER ******************//
console.log('HERMES SERVER Starting...');
var utils = require('../tools/index');

var hermesserver = require('../interface/hermes');

console.log(hermesserver);
hermesserver.config = {
    workers: 1,
    name: 'HERMES SERVER'
};
var w3wp = require('kioow.common').workerprocess();
w3wp(hermesserver);
