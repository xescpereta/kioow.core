﻿//**************** OPEN MARKET TRAVEL MEMENTO WORKER SERVER STARTER ******************//
console.log('MEMENTO SERVER Starting...');
var utils = require('../tools/index');

var memento = require('../memento/memento');

memento.config = {
    workers: 4,
    name: 'MEMENTO SERVER'
};
var w3wp = require('kioow.common').workerprocess;
var wp = w3wp(memento);
