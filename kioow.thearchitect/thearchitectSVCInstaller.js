﻿var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
    name: 'The Architect - KIOOW PETS SYSTEM',
    description: 'KIOOW Processes Manager. Manages every process for kioow.com',
    script: 'd:/node/kioow/core/kioow.thearchitect/thearchitect.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', function () {
    svc.start();
});

svc.install();