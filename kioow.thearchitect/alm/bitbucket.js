﻿
var configpath = 'C:/development/node/yourttoo/core/yourttoo.core/configurations/';
var taskspath = 'C:/development/node/yourttoo/core/yourttoo.thearchitect/alm/';
var toolsPath = 'C:/development/node/yourttoo/core/yourttoo.core/tools/';
var forker = require(toolsPath + 'threading');

var BitBucket = function () {
    this.client = null;
    this.config = require(configpath + 'alm.mappings.js');

}

BitBucket.prototype.connect = function () {
    var bt = require('bitbucket-api');
    var credentials = {
        username: bitbucket.config.GITMappings.user, 
        password: bitbucket.config.GITMappings.password,
        version: '2.0'
    };
    
    bitbucket.client = bt.createClient(credentials);
    
}

BitBucket.prototype.getrepository = function (reposlug, repoowner, callback) {
    var conf = { slug: reposlug, owner: repoowner };
    var repository = bitbucket.client.getRepository(conf, function (err, repo) {
        if (err) { console.log(err); if (callback) { callback(err); } }
        if (repo) {
            if (callback) { callback(repo); }
        }
    });
}

BitBucket.prototype.repositories = function (callback) {
    bitbucket.client.repositories(function (rs) {
        callback(rs);
    });
}

BitBucket.prototype.sources = function (repo) { 
    return repo.sources(null, null);
}

BitBucket.prototype.clone = function (repo, callback) {
    var config = {
        parameters: {
            repositoryslug: repo.slug,
            repositoryowner: repo.owner,
            GITMappings: bitbucket.config.GITMappings,
            basepath: bitbucket.config.GITMappings.target
        },
        taskpath: taskspath + 'downloader.js'
    };
    var task = forker.forkprocess(config);
    task.on('TASK.STARTED', function () {
        //task started...
    });
    task.on('TASK.FINISHED', function (result) {
        callback(result);
    });
}

var bitbucket = module.exports = exports = new BitBucket;

//function launchprocess(configuration, callback) {
//    var workerHub = require('child_process');
//    var worker = null;
    
//    try {
//        worker = workerHub.fork(configuration.taskpath);
        
//        worker.on('message', function (result) {
//            console.log(result);
//            if (result.Status == 'WORKER.READY') {
//                worker.send(configuration.paramerters);
//            }
//            if (result.Status == 'WORKER.FINISHED') {
//                callback(result.Data);
//            }
                    
//        });
//        worker.on('exit', function (code) {
//            console.log('worker process exited with exit code ' + code);
//            console.log(configuration.taskpath);
//        });
//        worker.on('close', function (code) {
//            console.log('worker process exited with code ' + code);
//        });

//    }
//    catch (err) {
//        console.log(err);
//        callback({ ResultOK: false, Message: err });
//    }
//}