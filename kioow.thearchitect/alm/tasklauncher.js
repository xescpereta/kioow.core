﻿
//configure path variables...
var toolsPath = 'd:/node/kioow/core/kioow.core/tools/';
var configPath = 'd:/node/kioow/core/kioow.core/configurations/';
var tasksPath = 'd:/node/kioow/core/kioow.thearchitect/alm/';
var configuration = require(configPath + 'alm.mappings.config.js');
var forker = require(toolsPath + 'threading');
//TOKEN Auto Builder
function _buildTOKEN() {
    var d = new Date().getTime();
    var token = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    
    return token;
}

function processready() {
    process.send({ Status: 'WORKER.READY' });
}

function commit(result) {
    process.send({
        Status: 'WORKER.FINISHED',
        Data: result
    });
    
    setTimeout(function () {
        process.exit(0);
    }, 5000);
}

var taskmanager = function () { 
    process.on('message', function (task) {
        var methodname = task.name.replace('.', '');
        //launch task by name...
        taskhub[methodname](task.request, function (results) {
            commit(results);
        });
    });
}

var start = exports.start = new taskmanager;

//we are ready... start
processready();


//task hub launcher and manager
var events = require('events');

var taskhub = {
    deployall: _deployall,
    pullrepository: _pullrepository,
    deployfiles: _deployfiles,
    backupall: _backupall,
    testdummy: _testdummy
};
events.EventEmitter.call(taskhub);
taskhub.super_ = events.EventEmitter;
taskhub.prototype = Object.create(events.EventEmitter.prototype, {
    constructor: {
        value: taskhub,
        enumerable: false
    }
});

//aux time method
function getmilliseconds(date1, date2) {

    var dif = date1.getTime() - date2.getTime()
    
    var Seconds_from_T1_to_T2 = dif / 1000;
    var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
    return Seconds_Between_Dates * 1000;
}
//launch a deploy with configuration...
function launchdeploy(deployconfig, callback) {
    var count = deployconfig.DeployMappings.length;

    _.each(deployconfig.DeployMappings, function (deploymap) { 
        var deploytaskpath = tasksPath + 'deployer';
        var deploytaskconfig = {
            taskid: _buildTOKEN(),
            taskname: deploymap.name + '.TASK',
            taskpath: deploytaskpath,
            request: {
                sourcepath: btconfiguration.GITMappings.source,
                targetpath: btconfiguration.GITMappings.target
            }
        };
        var deploytask = forker.forkprocess(conf);
        deploytask.on('TASK.STARTED', function () {
            console.log(deploytaskconfig.taskname + '[' + deploytaskconfig.id + ']' + ' Started at ' + new Date());
        });
        deploytask.on('TASK.FINISHED', function (result) {
            count--;
            if (count == 0) {
                callback(result);
            }
        });

    });

    

}
//backup, download repo, deploy...
function _deployall(conf, callback) {
    var completedflags = {
        backup: false,
        backupresult: null,
        clone: false,
        cloneresult: null,
        deploy: false,
        deployresult: null
    };
    //launch backup
    var btconfiguration = require(configPath + 'alm.mapping.config');
    var currenttime = new Date();
    var delay = getmilliseconds(conf.datetodeploy, currenttime);

    //for each deploy map...
    var backupcount = btconfiguration.DeployMappings.length;
    _.each(btconfiguration.DeployMappings, function (deploymap) {
        if (deploymap.zippedbackup) { 
            //zip folder...
        } else {
            //copy files...
            var backuptaskpath = tasksPath + 'deployer';
            var date = new Date();
            var backupdirname = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '/';
            var backupconfig = {
                taskid: _buildTOKEN(),
                taskname: deploymap.name + '.TASK',
                taskpath: backuptaskpath,
                request: {
                    sourcepath: btconfiguration.GITMappings.target,
                    targetpath: btconfiguration.GITMappings.backup + backupdirname
                }
            };
            var backuptask = forker.forkprocess(backupconfig);
            backuptask.on('TASK.STARTED', function () { 
                console.log(backupconfig.taskname + '[' + backupconfig.id + ']' + ' Started at ' + new Date());
            });
            backuptask.on('TASK.FINISHED', function (result) {
                backupcount--;
                if (backupcount == 0) {
                    completedflags.backup = true;
                    completedflags.backupresult = result;
                    if (completedflags.backup == true && completedflags.clone == true) {
                        launchdeploy(btconfiguration, function (result) {
                            completedflags.deploy = true;
                            completedflags.deployresult = result;

                            //finishProcess
                            commit(completedflags);
                        });
                    }
                }
            });

        }
    });
    
    
    //launch repo download
    var clonerepotaskpath = tasksPath + 'downloader';
    var cloneconfig = {
        taskid: _buildTOKEN(),
        taskname: 'Clone Repo.TASK',
        taskpath: clonerepotaskpath,
        request: {
            GITMappings: btconfiguration.GITMappings,
            repositoryslug: btconfiguration.GITMappings.slug,
            repositoryowner: btconfiguration.GITMappings.owner,
            basepath: btconfiguration.GITMappings.source
        }
    };
    var clonerepotask = forker.forkprocess(backupconfig);

    clonerepotask.on('TASK.STARTED', function () {
        console.log(cloneconfig.taskname + '[' + cloneconfig.id + ']' + ' Started at ' + new Date());
    });
    clonerepotask.on('TASK.FINISHED', function (result) {
        completedflags.clone = true;
        completedflags.cloneresult = result;
        if (completedflags.backup == true && completedflags.clone == true) {
            launchdeploy(btconfiguration, function (result) { 
                completedflags.deploy = true;
                completedflags.deployresult = result;

                //finishProcess
                commit(completedflags);
            });
        }
    });
}
//download repo
function _pullrepository(conf, callback) {
    //launch repo download
    var btconfiguration = require(configPath + 'alm.mapping.config');
    var clonerepotaskpath = tasksPath + 'downloader';
    var cloneconfig = {
        taskid: _buildTOKEN(),
        taskname: 'Clone Repo.TASK',
        taskpath: clonerepotaskpath,
        request: {
            GITMappings: btconfiguration.GITMappings,
            repositoryslug: btconfiguration.GITMappings.slug,
            repositoryowner: btconfiguration.GITMappings.owner,
            basepath: btconfiguration.GITMappings.source
        }
    };
    var clonerepotask = forker.forkprocess(cloneconfig);
    
    clonerepotask.on('TASK.STARTED', function () {
        console.log(cloneconfig.taskname + '[' + cloneconfig.id + ']' + ' Started at ' + new Date());
    });
    clonerepotask.on('TASK.FINISHED', function (result) {
        //finishProcess
        commit(result);
    });
}

//backup files
function _backupfiles(conf, callback) {
    var btconfiguration = require(configPath + 'alm.mapping.config');
    var dpconf = _.find(btconfiguration.DeployMappings, function (mapping) {
        return conf.component.toLowerCase() == mapping.slug.toLowerCase();
    });

    if (dpconf != null) {
        var backuptaskpath = tasksPath + 'deployer';
        var backupdirname = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '/';
        var bckconfig = {
            taskid: _buildTOKEN(),
            taskname: dpconf.name + '.TASK',
            taskpath: backuptaskpath,
            request: {
                sourcepath: dpconf.target,
                targetpath: dpconf.backup + backupdirname,
                files: conf.files
            }
        };

        var backuptask = forker.forkprocess(backupconfig);
        backuptask.on('TASK.STARTED', function () {
            console.log(backupconfig.taskname + '[' + backupconfig.id + ']' + ' Started at ' + new Date());
        });
        backuptask.on('TASK.FINISHED', function (result) {
            backupcount--;
            if (backupcount == 0) {
                //finishProcess
                callback(result);
            }
        });
    }
}
//backup prd instance
function _backupall(conf, callback) {
    var btconfiguration = require(configPath + 'alm.mapping.config');
    //for each deploy map...
    var backupcount = btconfiguration.DeployMappings.length;
    _.each(btconfiguration.DeployMappings, function (deploymap) {
        if (deploymap.zippedbackup) { 
            //zip folder...
        } else {
            //copy files...
            var backuptaskpath = tasksPath + 'deployer';
            var date = new Date();
            var backupdirname = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '/';
            var backupconfig = {
                taskid: _buildTOKEN(),
                taskname: deploymap.name + '.TASK',
                taskpath: backuptaskpath,
                request: {
                    sourcepath: btconfiguration.GITMappings.target,
                    targetpath: btconfiguration.GITMappings.backup + backupdirname
                }
            };
            var backuptask = forker.forkprocess(backupconfig);
            backuptask.on('TASK.STARTED', function () {
                console.log(backupconfig.taskname + '[' + backupconfig.id + ']' + ' Started at ' + new Date());
            });
            backuptask.on('TASK.FINISHED', function (result) {
                backupcount--;
                if (backupcount == 0) {
                    //finishProcess
                    commit(result);
                }
            });

        }
    });
}
//deploy certain files from repo... (download, deploy...)
function _deployfiles(conf, callback) {
    var btconfiguration = require(configPath + 'alm.mapping.config');
    var tasksok = {
        backup: false,
        pull: false
    };
    _backupfiles(conf, function (result) { 
    
    });

    _pullrepository(null, function (result) {
        if (result.ResultOK) { 
            
        }
    });
}

//testing and dummy
function _testdummy(conf, callback) {
    var emptytaskpath = tasksPath + 'emptytask';
    var emptyconfig = {
        taskid: _buildTOKEN(),
        taskname: 'EMPTYDUMMY.TASK',
        taskpath: emptytaskpath,
        request: { Message: 'This is only for testing.. NOTHING TO DO...'}
    };
    var emptytask = forker.forkprocess(emptyconfig);

    emptytask.on('TASK.STARTED', function () {
        console.log(emptyconfig.taskname + '[' + emptyconfig.id + ']' + ' Started at ' + new Date());
    });
    emptytask.on('TASK.FINISHED', function (result) {
        commit(result);
    });
}

////generic task launcher...
//function launchtask(conf) {
//    var workerHub = require('child_process');
//    var worker = null;
//    //a task generated
//    var atask = {
//        id: conf.taskid,
//        name: conf.taskname
//    };
//    events.EventEmitter.call(atask);
//    atask.super_ = events.EventEmitter;
//    atask.prototype = Object.create(events.EventEmitter.prototype, {
//        constructor: {
//            value: atask,
//            enumerable: false
//        }
//    });
//    //start thread...
//    var worker = workerHub.fork(conf.taskpath);
//    worker.on('message', function (result) {
//        console.log(result);
//        if (result.Status == 'WORKER.READY') {
//            worker.send(conf.request);
//            atask.emit('TASK.STARTED', null);
//        }
//        if (result.Status == 'WORKER.FINISHED') {
//            atask.emit('TASK.FINISHED', result.Data);
//        }
                    
//    });
//    worker.on('exit', function (code) {
//        console.log('worker process exited with exit code ' + code);
//    });
//    worker.on('close', function (code) {
//        console.log('worker process exited with code ' + code);
//    });
    
//    return atask;
//}