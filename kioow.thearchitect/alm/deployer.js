﻿
var ncp = require('ncp').ncp;
ncp.limit = 16;
var configuration = null;

//******** process management
function processready() {
    process.send({ Status: 'WORKER.READY' });
}
//***************************

function copyfile(source, target, cb) {
    var cbCalled = false;
    
    var rd = fs.createReadStream(source);
    rd.on("error", function (err) {
        done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function (err) {
        done(err);
    });
    wr.on("close", function (ex) {
        done();
    });
    rd.pipe(wr);
    
    function done(err) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }
}

function commit(result) {
    process.send({
        Status: 'WORKER.FINISHED',
        Data: result
    });
    
    setTimeout(function () {
        process.exit(0);
    }, 5000);
}

function deploy(options, callback) {
    if (options.files != null && options.files.length > 0) {
        var countfiles = options.files.length;
        var errorreport = null;
        var errors = [];
        _.each(options.files, function (file) {
            var sourcefilepath = options.source + file;
            var targetfilepath = options.target + file;
            copyfile(sourcefilepath, targetfilepath, function (err) {
                if (err) { errors.push(err); console.log(err); if (errorreport == null) { errorreport = errors.join('\r\n'); } }
                countfiles--;
                if (countfiles == 0) { 
                    callback(errorreport);
                }
            });
        });
    } else {
        ncp(source, target, callback);
    }
}

function startprocess() {
    
    var options = {
        source : configuration.sourcepath,
        target : configuration.targetpath,
        files : configuration.files
    }
    deploy(options, function (err) {
        if (err) {
            console.error(err);
            commit({ ResultOK: false, Message: err });
            
        }
        else {
            commit({ ResultOK: true, Data: { Message: 'All directories copied : FROM: ' + source + ' => TO: ' + target } });
        }
        console.log('done!');
    });
}
//**************************


var deployer = exports.deployer = function () {
    
    process.on('message', function (conf) {
        configuration = conf;
        startprocess();
    });
    
}

var start = exports.start = deployer;

//we are ready... start
processready();