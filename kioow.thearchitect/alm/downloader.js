﻿
var btclient = null;
var configuration = null;
var filesdone = [];
var _ = require('underscore');

//******** process management
function processready() { 
    process.send({ Status: 'WORKER.READY' });
}
//***************************

//******** core functions...
function connect() {
    var bt = require('bitbucket-api');
    var credentials = {
        username: configuration.GITMappings.user, 
        password: configuration.GITMappings.password,
        version: '2.0'
    };
    
    btclient = bt.createClient(credentials);
}

function getrepository(callback) {
    var conf = { slug: configuration.repositoryslug, owner: configuration.repositoryowner };
    
    var repository = btclient.getRepository(conf, function (err, repo) {
        if (err) { console.log(err); if (callback) { callback(err); } }
        if (repo) {
            if (callback) { callback(repo); }
        }
    });
}

function downloadsource(repo, source, callback) {
    
    function checkfinished(files, dirs, sources) {
        return (files == 0 && dirs == 0 && sources == 0);
    }
    var results = { dirs: [], files: [], sources: [] };
    
    var fs = require('fs');
    if (source != null) {
        var totaldirs = 0;
        var totalfiles = 0;
        var totalsources = 0;
        //directories & sources
        if (source.directories != null && source.directories.length > 0) {
            totaldirs = source.directories.length;
            totalsources = source.directories.length;
            _.each(source.directories, function (dir) {
                var dresult = {
                    name: dir, 
                    fullpath: configuration.basepath + source.path + dir, 
                    ok: false, 
                    done: false, 
                    error: err
                };
                //build directory...
                fs.mkdir(configuration.basepath + source.path + dir, 0777, function (err) {
                    dresult.ok = true;
                    dresult.done = true;
                    if (err) {
                        console.log(err);
                        dresult.ok = false;
                        dresult.error = err;
                    }
                    totaldirs--;
                    //push results
                    results.dirs.push(dresult);
                    if (checkfinished(totalfiles, totaldirs, totalsources) == true) {
                        callback(results);
                    }
                });
                //download next sources
                var rsname = source.path + dir;
                var rsp = repo.sources(rsname, 'master');
                rsp.info(function (err, data) {
                    downloadsource(repo, data, function (rstresults) {
                        totalsources--;
                        results.sources.push({ name: rsname, results: rstresults });
                        if (checkfinished(totalfiles, totaldirs, totalsources) == true) {
                            callback(results);
                        }
                    });
                });
            });
        }
        
        //files
        if (source.files != null && source.files.length > 0) {
            totalfiles = source.files.length;
            _.each(source.files, function (file) {
                var fsource = {
                    done: false,
                    ok: false,
                    path: source.path + file,
                    name: file,
                    fullpath: configuration.basepath + source.path + file
                };
                //download
                downloadfile(fsource, function (fsresult) {
                    results.files.push(fsresult);
                    totalfiles--;
                    if (checkfinished(totalfiles, totaldirs, totalsources) == true) {
                        callback(results);
                    }
                });
            });
        }
    }
}

function downloadfile(file, repo, callback) {
    
    var fs = require('fs');
    var rs = repo.sources(file.path, 'master');
    
    rs.info(function (err, data) {
        if (err) { console.log(err); }
        if (data) { console.log('file downloaded... ' + file); }
    });
    rs.raw(function (err, data) {
        if (err) { console.log(err); }
        fs.writeFile(file.fullpath, data.raw, null, function (err) {
            file.ok = true;
            if (err) {
                console.log(err);
                file.error = err;
                file.ok = false;
            }
            //file writed.. finished
            file.done = true;
            
            callback(file);
        })
    });
}

function commit(result) { 
    process.send({
        Status: 'WORKER.FINISHED',
        Data: result
    });
    
    setTimeout(function () {
        process.exit(0);
    }, 5000);
}

function startprocess() {
    connect();
    getrepository(function (repo) { 
    
        //get the root source...
        var rsp = repo.sources('/', 'master');
        rsp.info(function (err, data) {
            if (err) {
                console.log(err);
                commit({ ResultOK: false, Message: err });
            }
            else {
                downloadsource(repo, data, function (result) { 
                    commit({ ResultOK: true, Data: result });
                });
            }
        })
    });
}
//**************************


var bitbucketdownloader = exports.bitbucketdownloader = function () { 
    
    process.on('message', function (conf) {
        configuration = conf;
        startprocess();
    });
    
}

var start = exports.start = bitbucketdownloader;

//we are ready... start
processready();