﻿//  {}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}
//  {}                                                                {} 
//  {}            THE ORACLE KIOOW - 2015 Version 4.0.2               {}
//  {}                 A process to to manage ALM                     {}
//  {}                                                                {} 
//  {}               Check /appConfiguration dir                      {} 
//  {}               set your local configuration                     {}
//  {}                                                                {}
//  {}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}

//configure path variables...
var toolsPath = 'd:/node/kioow/core/kioow.core/tools/';
var configPath = 'd:/node/kioow/core/kioow.core/configurations/';
var tasksPath = 'd:/node/kioow/core/kioow.thearchitect/alm/';
var forker = require(toolsPath + 'threading');
//initialization...

var Server = require('socket.io');
var events = require('events');
var hash = require(toolsPath + 'hashtable');
var queue = require(toolsPath + 'queue');
var _ = require('underscore');
//TOKEN Auto Builder
function _buildTOKEN() {
    var d = new Date().getTime();
    var token = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    
    return token;
}

var TheOracle = function () {
    events.EventEmitter.call(this);
    this.tasks = new hash.HashTable();
    this.taskstodo = new queue.Queue();
    this.configuration = require(configPath + 'theoracle.config');
    this.io = { emit: function () { console.log('Listener not initialized...'); } };
    this.maininterval = null;
}
//Hierachy inheritance
TheOracle.super_ = events.EventEmitter;

TheOracle.prototype = Object.create(events.EventEmitter.prototype, {
    constructor: {
        value: TheOracle,
        enumerable: false
    }
});

TheOracle.prototype.startlistener = function (port) {
    if (port == null || port == 0) {
        port = 5656;
    }
    theoracle.io = Server.listen(port);

    theoracle.io.on('connection', function (socket) {
        //Action Methods...
        //deploy all repository release...
        socket.on('deploy.all', function (data) {
            var task = theoracle.enqueuetask({ name: 'deploy.all', request: data, client: socket });
            socket.emit('task.enqueued', task);
        });
        //get and download all repository release...
        socket.on('pull.repository', function (data) { 
            var task = theoracle.enqueuetask({ name: 'pull.repository', request: data, client: socket });
            socket.emit('task.enqueued', task);
        });
        //deploy selected repository files...
        socket.on('deploy.files', function (data) { 
            var task = theoracle.enqueuetask({ name: 'deploy.files', request: data, client: socket });
            socket.emit('task.enqueued', task);
        });
        //backup deployed instances...
        socket.on('backup.all', function (data) { 
            var task = theoracle.enqueuetask({ name: 'backup.all', request: data, client: socket });
            socket.emit('task.enqueued', task);
        });
        
        //dummy and testing
        socket.on('test.dummy', function (data) {
            var task = theoracle.enqueuetask({ name: 'test.dummy', request: data, client: socket });
            socket.emit('task.enqueued', task);
        });
        
        //Admin service events ### methods to control or watch the service...
        //return all the tasks
        socket.on('get.alltasks', function () {
            socket.emit('get.alltasks.response', theoracle.tasks.values());
        });
        //return all the enqueued tasks
        socket.on('get.enqueuedtasks', function () {
            socket.emit('get.enqueuedtasks.response', theoracle.taskstodo.elements());
        });
        //stop the runner
        socket.on('runner.stop', function () {
            var now = new Date();
            theoracle.stoptrigger();
            socket.emit('runner.stoped', { ResultOK: true, Message: 'TheOracle Runner stoped at ' + now});
        });
        //stop the runner
        socket.on('runner.start', function () {
            var now = new Date();
            theoracle.starttrigger();
            socket.emit('runner.started', { ResultOK: true, Message: 'TheOracle Runner started at ' + now });
        });
    }); 
}

//start the queue processing...
TheOracle.prototype.starttrigger = function () {
    theoracle.maininterval = setInterval(function () {
        var task = theoracle.taskstodo.dequeue();
        //change the status of the task
        task.status = 'task.running';
        theoracle.tasks.set(task.taskid, task);

        var dfprocess = forker.forkprocess(task, function (result) { 
            //change the status of the task
            task.status = 'task.done';
            theoracle.tasks.set(task.taskid, task);
        });

        dfprocess.on('TASK.STARTED', function () { 
            console.log(task.name  + ' started');
        });
        dfprocess.on('TASK.FINISHED', function (result) {
            console.log(task.name + ' finished');
            task.client.emit(task.name + '.done', result);
        });

    }, 10000);
    var now = new Date();
    theoracle.io.emit('runner.started', { ResultOK: true, Message: 'TheOracle Runner started at ' + now });
    theoracle.emit('runner.started', { ResultOK: true, Message: 'TheOracle Runner started at ' + now });
}
//stop the queue processing...
TheOracle.prototype.stoptrigger = function () {
    clearInterval(theoracle.maininterval);
    var now = new Date();
    theoracle.io.emit('runner.stoped', { ResultOK: true, Message: 'TheOracle Runner stoped at ' + now });
    theoracle.emit('runner.stoped', { ResultOK: true, Message: 'TheOracle Runner stoped at ' + now });
}


TheOracle.prototype.enqueuetask = function (options) { 
    var task = {
        taskname: options.name,
        taskclientref: options.key,
        taskpath: almtaskspath + 'tasklauncher',
        taskid: _buildTOKEN(),
        request: options.request,
        result: null,
        status: 'task.enqueued',
        client: options.client
    };
    theoracle.tasks.set(task.taskid, task);
    theoracle.taskstodo.enqueue(task);
    return task;
}

var theoracle = null;
theoracle = module.exports = exports = new TheOracle;