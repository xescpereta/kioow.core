﻿
var configuration = null;
var archiver = require('archiver');

//******** process management
function processready() {
    process.send({ Status: 'WORKER.READY' });
}
//***************************

function commit(result) {
    process.send({
        Status: 'WORKER.FINISHED',
        Data: result
    });
    
    setTimeout(function () {
        process.exit(0);
    }, 5000);
}
function startprocess() {
    var archive = archiver.create('zip', {});
    var output = fs.createWriteStream(configuration.ziptargetfile);

    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
        commit({
            ResultOK: true, Data: {
                Message: 'The directory has been zipped : FROM: ' + configuration.sourcedirectory + 
            ' => TO: ' + configuration.ziptargetfile
            }
        });
    });
    
    archive.on('error', function (err) {
        console.log(err);
        commit({ ResultOK: false, Message: err });
    });
    
    //redirect the zip output to the file stream...
    archive.pipe(output);

    archive.directory(configuration.sourcedirectory);
    archive.finalize();
}

var zipper = exports.zipper = function () {
    
    process.on('message', function (conf) {
        configuration = conf;
        startprocess();
    });
    
}

var start = exports.start = deployer;

//we are ready... start
processready();