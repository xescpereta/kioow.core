﻿
var configuration = null;

//******** process management
function processready() {
    process.send({ Status: 'WORKER.READY' });
}
//***************************

function commit(result) {
    process.send({
        Status: 'WORKER.FINISHED',
        Data: result
    });
    
    setTimeout(function () {
        process.exit(0);
    }, 5000);
}



function startprocess() {
    
    console.log(configuration);
    commit({ ResultOK: true, Message: 'Empty task done'});
}
//**************************


var empty = exports.empty = function () {
    
    process.on('message', function (conf) {
        configuration = conf;
        startprocess();
    });
    
}

var start = exports.start = new empty;

//we are ready... start
processready();